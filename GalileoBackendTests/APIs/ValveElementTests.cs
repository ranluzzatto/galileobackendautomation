﻿using GalileoTestInfrastructure.GalileoApis.Alarm;
using GalileoTestInfrastructure.GalileoApis.ValveElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class ValveElementTests
    {
        [Test]
        [Category("Get ValveElements")]
        public void GetValveElements()
        {
            var valveapi = new ValveSetupGetApi();
            valveapi.Run();

            Assert.IsTrue(valveapi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(valveapi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = valveapi.ResponseBody.GetStringValueFromPath("body");
            var items = valveapi.ResponseBody.GetStringValueFromPath("body.items");
            var items2 = valveapi.ResponseBody.GetStringValueFromPath("body.valveElements");

            

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(body, "Body cannot be null");
                Assert.IsNotNull(items, "The valveElements array should exist, even if empty!");
                Assert.IsNotNull(items2, "The valveElements array should exist, even if empty!");
            });
        }




        [Test]
        [Category("ValveElements Post")]
        public void ValveElementsPost() 
        {
            var valve = new ValvePostApi();

            
            var body = ValvePostApi.DefaultPayload.ToJObject();
            var name = "AutomatedName";
            var irrigationValveGroupNumber = 2;
            var multValve = 50;

            body.SetValueToPath("items[0].name", name);
            body.SetValueToPath("items[0].irrigationValveGroupNumber", irrigationValveGroupNumber);
            body.SetValueToPath("items[0].multValve", multValve);


            valve.RequestPayload = body.ToString();
            valve.Run();

            var valveApi = new ValveSetupGetApi();
            valveApi.Run();
            Assert.IsTrue(valveApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(valveApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            Assert.Multiple(() =>
            {
                Assert.IsTrue(valveApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(valveApi.ResponseBody.IsPropertyPathEQValue("body.items[0].irrigationValveGroupNumber", irrigationValveGroupNumber.ToString()));
                Assert.IsTrue(valveApi.ResponseBody.IsPropertyPathEQValue("body.items[0].multValve", multValve.ToString()));
            });

        }

        [Test]
        [Category("Valve RunEvents")]
        public void RunEvents()
        {
            var valve = new RunEventsPostApi();
            var body = @"{""numbers"":[3]}"; // request to run events for item# 3
            
            valve.RequestPayload = body;
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                        ""Type"": 1,
                                          ""CommandCode"": 25,
                                          ""ComponentTypeCode"": 1,
                                          ""CNFCode"": 0,
                                          ""Number"": 0,
                                          ""Numbers"": [
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7,
                                            8,
                                            9,
                                            10
                                          ],
                                          ""StatusCode"": 0,
                                          ""RealTimeValue"": null,
                                          ""FirmwareId"": 0,
                                          ""LogType"": 0,
                                          ""GHNumber"": 0,
                                          ""InstanceNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            valve.AddRedisEventToValidate(eventToValidate);

            valve.Run();

            Assert.IsTrue(valve.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(valve.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(valve.RedisValidationResult, "The expected Redis event was not found");

        }
        [Test]
        [Category("ValveElements SocketField Put")]
        public void ValveElementsSocketFieldt()
        {
            var valve = new SocketFieldPutApi();

            //TODO

        }
    }
}
