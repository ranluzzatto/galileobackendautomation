﻿using GalileoTestInfrastructure.GalileoApis.Setup;
using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class SetupTests
    {
        [Test]
        [TestCase("GALOabcd00000004", true)]
        [TestCase("SomeFictionalSerial", false)]
        [Category("Is allocating pending SN - Get")]
        public void IsAllocatingPendingSNGetApi(string commUnitId, bool trueOrFalse)
        {
            var isAllocatingPendingSNGetApi = new IsAllocatingPendingSNGetApi(new Dictionary<string, string> { { "sn", commUnitId } });
            isAllocatingPendingSNGetApi.Run();

            Assert.IsTrue(isAllocatingPendingSNGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(isAllocatingPendingSNGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            var body = isAllocatingPendingSNGetApi.ResponseBody.GetBoolValueFromPath("body");
            Assert.AreEqual(trueOrFalse, body);
        }

        [Test]
        [TestCase("GALOabcd00000004", false)]
        [TestCase("SomeFictionalSerial", true)]
        [Category("Is SN unique - Get")]
        public void IsSNUniqueGet(string commUnitId, bool trueOrFalse)
        {
            var isSNUniqueGetApi = new IsSNUniqueGetApi(new Dictionary<string, string> { { "sn", commUnitId } });
            isSNUniqueGetApi.Run();

            Assert.IsTrue(isSNUniqueGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(isSNUniqueGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            var body = isSNUniqueGetApi.ResponseBody.GetBoolValueFromPath("body");
            Assert.AreEqual(trueOrFalse, body);
        }

        [Test]
        [Category("Get controller setup view - Get")]
        public void GetControllerSetupViewGet()
        {
            var getControllerSetupViewGetApi = new GetControllerSetupViewGetApi(new Dictionary<string, string> {
                                                { "commUnitID",  ConfigurationManagerWrapper.GetParameterValue("CommUnitID")} });
            getControllerSetupViewGetApi.Run();

            var body = getControllerSetupViewGetApi.ResponseBody["body"];
            Assert.IsTrue(getControllerSetupViewGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getControllerSetupViewGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.Multiple(() =>
            {
                var controllerSetupView = body["controllerSetupView"];
                Assert.IsNotNull(controllerSetupView, "controllerSetupView");
                Assert.IsNotNull(controllerSetupView["greenHouseSettingsView"], "greenHouseSettingsView");
                Assert.IsNotNull(controllerSetupView["openFieldSettingsView"], "openFieldSettingsView");

                var timeZoneViews = body["timeZoneViews"];
                if(timeZoneViews != null)
                {
                    foreach(var timeZoneView in timeZoneViews)
                    {
                        Assert.IsNotNull(timeZoneView, "timeZoneViews");
                    }
                }
            });
        }

       // [Test] Needs fixing
     //   [Category("Update controller setup - Post")]
        public void UpdateControllerSetupPost()
        {
            var updateControllerSetupPostApi = new UpdateControllerSetupPostApi();
            updateControllerSetupPostApi.RequestPayload = updateControllerSetupPostApi.DefaultPayload;
            updateControllerSetupPostApi.Run();

            var response = updateControllerSetupPostApi.ResponseBody["body"];
            Assert.IsTrue(updateControllerSetupPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error: {0}",updateControllerSetupPostApi.ResponseStatusCode.ToString());
            Assert.IsTrue(updateControllerSetupPostApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            // Compare requestPayload with response
            var payloadJson = updateControllerSetupPostApi.DefaultPayload.ToJObject();
            payloadJson["commUnitID"] = payloadJson.GetStringValueFromPath("commUnitID");
            payloadJson["configID"] = payloadJson.GetStringValueFromPath("configID");
            Assert.IsTrue(JToken.DeepEquals(response, payloadJson), "post and get compare--\n\nResponse:\n----------\n{0}\n payloadJson:\n------\n{1}", response, payloadJson);
        }

        [Test]
        [Category("Get controller open field messages view - Get")]
        public void GetControllerOpenFieldMessagesViewGet()
        {
            var getControllerOpenFieldMessagesViewGetApi = new GetControllerOpenFieldMessagesViewGetApi(new Dictionary<string, string> {
                                                { "commUnitID",  ConfigurationManagerWrapper.GetParameterValue("CommUnitID")} });
            getControllerOpenFieldMessagesViewGetApi.Run();

            var response = getControllerOpenFieldMessagesViewGetApi.ResponseBody["body"];
            Assert.IsTrue(getControllerOpenFieldMessagesViewGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getControllerOpenFieldMessagesViewGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsNotNull(response, "body");
        }

        [Test]
        [Category("Get cards setup view - Get")]
        public void GetCardsSetupViewGet()
        {
            var getCardsSetupViewGetApi = new GetCardsSetupViewGetApi(new Dictionary<string, string> {
                                                { "commUnitID",  ConfigurationManagerWrapper.GetParameterValue("CommUnitID")} });
            getCardsSetupViewGetApi.Run();

            var body = getCardsSetupViewGetApi.ResponseBody["body"];
            Assert.IsTrue(getCardsSetupViewGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getCardsSetupViewGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.Multiple(() =>
            {
                var cardList = body["cardList"];
                foreach (var cardInList in cardList) {
                    Assert.IsNotNull(cardInList, "cardList");
                    var cards = cardInList["cards"];
                    foreach(var card in cards)
                    {
                        Assert.IsNotNull(card, "cards");
                        Assert.IsNotNull(card["controlPanelSubCard"], "controlPanelSubCard");
                    }
                }

                var cardTypeList = body["cardTypeList"];
                foreach (var cardInList in cardTypeList)
                {
                    Assert.IsNotNull(cardInList, "cardTypeList");
                    var cardTypes = cardInList["cardTypes"];
                    foreach (var cardType in cardTypes)
                    {
                        Assert.IsNotNull(cardType, "cardTypes");
                    }
                }
            });
        }

        [Test]
        [Category("Download all setup to controller - Put")]
        public void DownloadAllSetupToControllerPutApi()
        {
            var downloadAllSetupToControllerPutApi = new DownloadAllSetupToControllerPutApi();
            downloadAllSetupToControllerPutApi.Run();

            Assert.IsTrue(downloadAllSetupToControllerPutApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(downloadAllSetupToControllerPutApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsTrue(downloadAllSetupToControllerPutApi.ResponseBody.GetBoolValueFromPath("body"));
        }

        [Test]
        [Category("Clear - Put")]
        public void ClearPut()
        {
            var clearPutApi = new ClearPutApi();
            clearPutApi.Run();

            Assert.IsTrue(clearPutApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(clearPutApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsTrue(clearPutApi.ResponseBody.GetBoolValueFromPath("body"));
        }
    }
}