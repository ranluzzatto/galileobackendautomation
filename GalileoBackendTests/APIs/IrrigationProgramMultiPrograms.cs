﻿using GalileoTestInfrastructure.GalileoApis.IrrigationProgramMultiPrograms;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class IrrigationProgramMultiPrograms
    {
        [Test]
        [Category("Irrigation program multiPrograms - Get")]
        public void IrrigationProgramMultiProgramsGet()
        {
            var irrigationProgramMultiProgramsGet = new IrrigationProgramMultiProgramsGetApi();
            irrigationProgramMultiProgramsGet.Run();

            Assert.IsTrue(irrigationProgramMultiProgramsGet.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramMultiProgramsGet.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var valveElements = irrigationProgramMultiProgramsGet.ResponseBody["body"]["valveElements"];
            var fertilizerCenterProgramItems = irrigationProgramMultiProgramsGet.ResponseBody["body"]["fertilizerCenterProgramItems"];
            var valveGroupItems = irrigationProgramMultiProgramsGet.ResponseBody["body"]["valveGroupItems"];
            var fertilizerPumpGroupItems = irrigationProgramMultiProgramsGet.ResponseBody["body"]["fertilizerPumpGroupItems"];
            var fertilizerCenterGroupItems = irrigationProgramMultiProgramsGet.ResponseBody["body"]["fertilizerCenterGroupItems"];
            var items = irrigationProgramMultiProgramsGet.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                var res = valveElements.GetChildrenCount() == 0;
                if (valveElements != null || !res)
                {
                    Assert.IsNotNull(valveElements, "valveElements");
                }

                res = fertilizerCenterProgramItems.GetChildrenCount() == 0;
                if (fertilizerCenterProgramItems != null || !res)
                {
                    Assert.IsNotNull(fertilizerCenterProgramItems, "fertilizerCenterProgramItems");
                }

                res = valveGroupItems.GetChildrenCount() == 0; 
                if (valveGroupItems != null || !res)
                {
                    Assert.IsNotNull(valveGroupItems, "valveGroupItems");
                }

                res = fertilizerPumpGroupItems.GetChildrenCount() == 0;
                if (fertilizerPumpGroupItems != null || !res)
                {
                    Assert.IsNotNull(fertilizerPumpGroupItems, "fertilizerPumpGroupItems");
                }

                res = fertilizerCenterGroupItems.GetChildrenCount() == 0;
                if (fertilizerCenterGroupItems != null || !res)
                {
                    Assert.IsNotNull(fertilizerCenterGroupItems, "fertilizerCenterGroupItems");
                }

                res = items.GetChildrenCount() == 0;
                if (items != null || !res)
                {
                    Assert.IsNotNull(items, "items");
                }
            });
        }

        [Test]
        [Category("Irrigation program multiPrograms - Post")]
        public void IrrigationProgramMultiProgramsPost()
        {
            var irrigationProgramMultiProgramsPost = new IrrigationProgramMultiProgramsPostApi();
            irrigationProgramMultiProgramsPost.RequestPayload = irrigationProgramMultiProgramsPost.DefaultPayload;
            irrigationProgramMultiProgramsPost.Run();

            Assert.IsTrue(irrigationProgramMultiProgramsPost.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramMultiProgramsPost.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            // Get - check of post
            var irrigationProgramMultiProgramsGet = new IrrigationProgramMultiProgramsGetApi();
            irrigationProgramMultiProgramsGet.Run();

            Assert.IsTrue(irrigationProgramMultiProgramsGet.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramMultiProgramsGet.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var items = irrigationProgramMultiProgramsGet.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                for(var i=0; i<3; i++)
                {
                    var iterNumber = i + 1;
                    Assert.IsTrue(items[i].GetStringValueFromPath("name") == "Irrigation Program " + iterNumber, "item name");
                    Assert.IsTrue(items[i].GetIntValueFromPath("valveA") == iterNumber, "item number");
                }
            });
        }

        //[Test] Do not work because of directory
        //[Category("Export CSV - Post")]
        //public void ExportCSVPost()
        //{
        //    var exportCSVPost = new ExportCSVPostApi();
        //    exportCSVPost.RequestPayload = exportCSVPost.DefaultPayload;
        //    exportCSVPost.Run();

        //    Assert.IsTrue(exportCSVPost.ResponseStatusCode == HttpStatusCode.OK, "Expected Response was be OK, but got {0}",exportCSVPost.ResponseStatusCode);
        //}

        //[Test]
        //[Category("Export PDF - Post")]
        //public void ExportPDFPost()
        //{
        //    var exportPDFPost = new ExportPDFPostApi();
        //    exportPDFPost.RequestPayload = exportPDFPost.DefaultPayload;
        //    exportPDFPost.Run();

        //    Assert.IsTrue(exportPDFPost.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
        //}

        [Test]
        [TestCase(@"{""type"":3,""value"":0,""fieldName"":""Restart"",""numbers"":[1]}")]
        [TestCase(@"{""type"":3,""value"":1,""fieldName"":""Restart"",""numbers"":[1]}")]
        [TestCase(@"{""type"":3,""value"":2,""fieldName"":""Restart"",""numbers"":[1]}")]
        [TestCase(@"{""type"":1,""value"":1,""fieldName"":""UntilPause"",""numbers"":[1]}")]
        [TestCase(@"{""type"":1,""value"":1,""fieldName"":""PauseTime"",""numbers"":[1]}")]
        [TestCase(@"{""type"":1,""value"":1,""fieldName"":""WaterMultiplyForCurrIrrig"",""numbers"":[1]}")]
        [TestCase(@"{""type"":6,""value"":1,""fieldName"":""WaterRemaining"",""numbers"":[1],""fractionalDigitsCount"":2}")]
        [Category("Socket field - Put")]
        public void SocketFieldPut(string payloadParam)
        {
            var socketFieldPut = new SocketFieldPutApi();
            socketFieldPut.RequestPayload = payloadParam;
            socketFieldPut.Run();

            Assert.IsTrue(socketFieldPut.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(socketFieldPut.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");
        }

        [Test]
        [Category("Information - Get")]
        public void InformationGet()
        {
            var informationGetApi = new InformationGetApi();
            informationGetApi.Run();

            Assert.IsTrue(informationGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(informationGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = informationGetApi.ResponseBody["body"];
            Assert.Multiple(() =>
            {
                foreach (var item in body)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("number"), "number");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("name"), "name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("fertCenterNumber"), "fertCenterNumber");
                }
            });
        }

        [Test]
        [Category("Filters - Get")]
        public void FiltersGet()
        {
            var filtersGetApi = new FiltersGetApi();
            filtersGetApi.Run();

            Assert.IsTrue(filtersGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(filtersGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = filtersGetApi.ResponseBody["body"];
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(body.GetStringValueFromPath("programNumberFrom"), "programNumberFrom");
                Assert.IsNotEmpty(body.GetStringValueFromPath("programNumberTo"), "programNumberTo");
                Assert.IsNotEmpty(body.GetStringValueFromPath("statuses"), "statuses");
                Assert.IsNotEmpty(body.GetStringValueFromPath("activity"), "activity");
                Assert.IsNotEmpty(body.GetStringValueFromPath("localFertPumpNumber"), "localFertPumpNumber");
                Assert.IsNotEmpty(body.GetStringValueFromPath("fertCenterNumber"), "fertCenterNumber");
                Assert.IsNotEmpty(body.GetStringValueFromPath("fertCenterProgNumber"), "fertCenterProgNumber");
            });
        }

        [Test]
        [Category("Filters - Put")]
        public void FiltersPut()
        {
            var filtersPostApi = new FiltersPutApi();
            filtersPostApi.RequestPayload = filtersPostApi.DefaultPayload;
            filtersPostApi.Run();

            Assert.IsTrue(filtersPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(filtersPostApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            // Get - check post
            var filtersGetApi = new FiltersGetApi();
            filtersGetApi.Run();

            Assert.IsTrue(filtersGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(filtersGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            // Values of payload to compare the get response
            var payload = filtersPostApi.DefaultPayload.ToJObject();
            var programNumberFrom = payload.GetIntValueFromPath("programNumberFrom");
            var programNumberTo = payload.GetIntValueFromPath("programNumberTo");
            var localFertPumpNumber = payload.GetIntValueFromPath("localFertPumpNumber");
            var fertCenterNumber = payload.GetIntValueFromPath("fertCenterNumber");
            var fertCenterProgNumber = payload.GetIntValueFromPath("fertCenterProgNumber");

            var body = filtersGetApi.ResponseBody["body"];
            Assert.Multiple(() =>
            {
                Assert.AreEqual(programNumberFrom, body.GetIntValueFromPath("programNumberFrom"), "programNumberFrom");
                Assert.AreEqual(programNumberTo, body.GetIntValueFromPath("programNumberTo"), "programNumberTo");
                Assert.IsNull(body.GetStringValueFromPath("statuses"), "statuses");
                Assert.IsNull(body.GetStringValueFromPath("activity"), "activity");
                Assert.AreEqual(localFertPumpNumber, body.GetIntValueFromPath("localFertPumpNumber"), "localFertPumpNumber");
                Assert.AreEqual(fertCenterNumber, body.GetIntValueFromPath("fertCenterNumber"), "fertCenterNumber");
                Assert.AreEqual(fertCenterProgNumber, body.GetIntValueFromPath("fertCenterProgNumber"), "fertCenterProgNumber");
            });
        }
    }
}
