﻿using GalileoTestInfrastructure.GalileoApis.Alarm;
using GalileoTestInfrastructure.GalileoApis.FertilizerPumpElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class FertilizerPumpElementGroupTests
    {

        [Test]
        [Category("Get FertilizerPumpElementGroup")]
        public void GetFertilizerPumpElementGroup()
        {
            var FertPump = new FertilizerPumpElementGroupGetApi();

            FertPump.Run();

            Assert.IsTrue(FertPump.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(FertPump.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");


            Assert.Multiple(() =>
            {
                
                Assert.IsNotNull(FertPump.ResponseBody["body"]["analogGeneralSetup"], "Response should include analogGeneralSetup");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["analogSetupItems"], "Response should include analogSetupItems");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["analogTypeGeneralSetup"], "Response should include analogTypeGeneralSetup");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["autoGeneralSetup"], "Response should include autoGeneralSetup");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["commonSensors"], "Response should include commonSensors");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["fertilizerPumpItems"], "Response should include fertilizerPumpItems");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["fertillzerCenterItems"], "Response should include fertillzerCenterItems");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["generalDerivativeSetup"], "Response should include generalDerivativeSetup");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["irrigationPrograms"], "Response should include irrigationPrograms");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["pidSetupItems"], "Response should include pidSetupItems");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["pipeLineGroups"], "Response should include pipeLineGroups");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["privateWaterMeterElement"], "Response should include privateWaterMeterElement");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["sensorElements"], "Response should include sensorElements");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["sensorGroupNumbers"], "Response should include sensorGroupNumbers");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["waterMeterElements"], "Response should include waterMeterElements");
                Assert.IsNotNull(FertPump.ResponseBody["body"]["waterMeterGroupNumbers"], "Response should include waterMeterGroupNumbers");
                Assert.IsTrue(FertPump.ResponseBody["body"]["irrigationPrograms"].Count() == 200, "There should be 200 irrigation programs!");






            });
        }

        [Test]
        [Category("Post FertilizerPumpElementGroup")]

        public void FertilizerPumpElementGroupPost()
        {
            var FertPump = new FertilizerPumpElementGroupPostApi();
            var bodyToEdit = FertilizerPumpElementGroupPostApi.DefaultPayload.ToJObject();

            //Edit an item
            var name = "AutomationFert!!!";
            var fertPumpType = 1;
            var fertilizerTypeID = 0;
            var mainFertilizerPumpElement = true;
            var openFertilizerPumpElement = true;
            var closeFertilizerPumpElement = false;
            var flowElementType = 1;
            var sensorType = 0;
            var fertilizerPumpElement = true;
            var pulse = 6;
            var fertFlowRate = 56;
            var pulseDuration = 56;
            var minimumAutoPulse = 5.2;
            var recommendedCycle = 52;
            var electronicStroke = 6;
            var continueOperation = true;
            var waterMeterType = 1;
            var waterMeter = 402815;
            var pipeLineNumber = 1;
            var pipeFillDelay = 160;
            var noFertPulse = 6180;
            var uncontrolFert = 62;
            var autoCancellAlarm = true;
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].fertPumpType", fertPumpType);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].name", name);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].fertilizerTypeID", fertilizerTypeID);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].mainFertilizerPumpElement", mainFertilizerPumpElement);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].openFertilizerPumpElement", openFertilizerPumpElement);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].closeFertilizerPumpElement", closeFertilizerPumpElement);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].flowElementType", flowElementType);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].sensorType", sensorType);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].fertilizerPumpElement", fertilizerPumpElement);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].pulse", pulse);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].fertFlowRate", fertFlowRate);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].pulseDuration", pulseDuration);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].minimumAutoPulse", minimumAutoPulse);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].recommendedCycle", recommendedCycle);
            if(fertPumpType == 1)
                bodyToEdit.SetValueToPath("fertilizerPumpItems[0].electronicStroke", electronicStroke);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].continueOperation", continueOperation);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].waterMeterType", waterMeterType);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].waterMeter", waterMeter);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].pipeLineNumber", pipeLineNumber);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].pipeFillDelay", pipeFillDelay);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].noFertPulse", noFertPulse);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].uncontrolFert", uncontrolFert);
            bodyToEdit.SetValueToPath("fertilizerPumpItems[0].autoCancellAlarm", autoCancellAlarm);



            FertPump.RequestPayload = bodyToEdit.ToString();
            FertPump.Run();


            var FertPumpGet = new FertilizerPumpElementGroupGetApi();
            FertPumpGet.Run();
            Assert.IsTrue(FertPumpGet.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK, but received {0} instead", FertPumpGet.ResponseStatusCode);
            Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");




            Assert.Multiple(() =>
            {
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].fertPumpType", fertPumpType.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].name", name));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].fertilizerTypeID", fertilizerTypeID.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].mainFertilizerPumpElement", mainFertilizerPumpElement.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].openFertilizerPumpElement", openFertilizerPumpElement.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].closeFertilizerPumpElement", closeFertilizerPumpElement.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].flowElementType", flowElementType.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].sensorType", sensorType.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].fertilizerPumpElement", fertilizerPumpElement.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].pulse", pulse.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].fertFlowRate", fertFlowRate.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].pulseDuration", pulseDuration.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].minimumAutoPulse", minimumAutoPulse.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].recommendedCycle", recommendedCycle.ToString()));
                if (fertPumpType == 1) //if fertPumpType == Electronic --> electronicStroke is a MUST, disabled for other fertPump types
                    Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].electronicStroke", electronicStroke.ToString()));

                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].continueOperation", continueOperation.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].waterMeterType", waterMeterType.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].waterMeter", waterMeter.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].pipeLineNumber", pipeLineNumber.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].pipeFillDelay", pipeFillDelay.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].noFertPulse", noFertPulse.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].uncontrolFert", uncontrolFert.ToString()));
                Assert.IsTrue(FertPumpGet.ResponseBody.IsPropertyPathEQValue("body.fertilizerPumpItems[0].autoCancellAlarm", autoCancellAlarm.ToString()));
            });
        }

        [Test]
        [Category("RunEvents")]
        public void RunEvents()
        {
            var Fert = new FertilizerPumpElementGroupRunEvents();
            var body = @"{""numbers"":[1]}"; 

            Fert.RequestPayload = body;
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 9,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [
                                        1
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0,
                                      ""GHNumber"": 0,
                                      ""InstanceNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            Fert.AddRedisEventToValidate(eventToValidate);

            Fert.Run();

            Assert.IsTrue(Fert.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(Fert.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(Fert.RedisValidationResult, "The expected Redis event was not found ");
        }

        [Test]
        [Category("SocketField")]
        public void SocketField()
        {
            var Fert = new FertilizerPumpElementGroupSocketField();
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 4,
                                      ""CommandCode"": 9,
                                      ""ComponentTypeCode"": 0,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": null,
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": {
                                        ""Numbers"": [
                                          1
                                        ],
                                        ""ParameterNumber"": 60,
                                        ""Value"": 1
                                      },
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0,
                                      ""GHNumber"": 0,
                                      ""InstanceNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            Fert.AddRedisEventToValidate(eventToValidate);
            Fert.RequestPayload = @"{""type"":5,""value"":true,""fieldName"":""Start"",""numbers"":[1]}";
            Fert.Run();

            Assert.IsTrue(Fert.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(Fert.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(Fert.RedisValidationResult, "The expected Redis event was not found ");
        }
        [Test]
        [Category("Get FertilizerPumpElementGroupInfo")]
        public void GetFertilizerPumpElementGroupInfo()
        {
            var FertPump = new FertilizerPumpElementGroupInfoGetApi();

            FertPump.Run();

            Assert.IsTrue(FertPump.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(FertPump.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsNotNull(FertPump.ResponseBody["body"]["items"],"Response should include list of elements connected, even if empty");

        }

        }



    }

