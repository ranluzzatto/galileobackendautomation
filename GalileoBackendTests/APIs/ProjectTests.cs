﻿using GalileoTestInfrastructure.GalileoApis.Project;
using GalileoTestInfrastructure.GalileoApis.PlotElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class ProjectTests
    {
        [Test]
        [Category("Get Project")]
        public void GetProject()
        {
            var projectApi = new ProjectGetApi();
            projectApi.Run();

            Assert.IsTrue(projectApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(projectApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = projectApi.ResponseBody.GetStringValueFromPath("body");
            var project = projectApi.ResponseBody.GetStringValueFromPath("body.projects");

            

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(body, "Body cannot be null");
                Assert.IsNotNull(project, "The projects array should exist, even if empty!");

            });
        }


        [Test]
        [Category("Get Active")]
        public void GetActive()
        {
            var projectApi = new ActiveGetApi();
            projectApi.Run();

            Assert.IsTrue(projectApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(projectApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = projectApi.ResponseBody.GetStringValueFromPath("body");
            var project = projectApi.ResponseBody.GetStringValueFromPath("body.projectID");
            var controller = projectApi.ResponseBody.GetStringValueFromPath("body.controllerID");
            var gh = projectApi.ResponseBody.GetStringValueFromPath("body.greenHouseID");


            Assert.Multiple(() =>
            {
                Assert.IsNotNull(body, "Body cannot be null");
                Assert.IsNotNull(project, "A controller must be in a project");
                Assert.IsEmpty(gh, "This controller is OF, hence GH ID Must be empty!");
                Assert.IsNotNull(controller, "The projects array should exist, even if empty!");
            });
        }


        [Test]
        [Category("ProjectActive Post")]
        public void ProjectActivePostApi() //Incomplete
        {
            var active = new ActivePostApi();

            var body = active.DefaultPayload.ToJObject();
            active.RequestPayload = body.ToString();
            active.Run();

        }

        [Test]
        [Category("ElementTypesGetApi")]
        public void GetElementTypes()
        {
            var elementsApi = new ElementsTypesGetApi();
            elementsApi.Run();

            Assert.IsTrue(elementsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(elementsApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = elementsApi.ResponseBody.GetStringValueFromPath("body");
            Assert.IsNotNull(body, "Body cannot be null");

        }

        [Test]
        [Category("OnlineStatus")]
        public void OnlineStatus()
        {
            var onlineStatus = new ProjectConfigOnlineStatusApi();
            onlineStatus.Run();
            Assert.IsTrue(onlineStatus.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(onlineStatus.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = onlineStatus.ResponseBody.GetStringValueFromPath("body");
            Assert.IsNotNull(body, "Body cannot be null");
        }

        [Test]
        [Category("Reset")]
        public void ResetGetApi()
        {
            var resetApi = new ResetGetApi();
            resetApi.Run();
            Assert.IsTrue(resetApi.ResponseStatusCode == HttpStatusCode.OK, "Status code should be OK, received {0} instead", resetApi.ResponseStatusCode);
            Assert.IsTrue(resetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in body must be true");
        }

        [Test]
        [Category("UpdateStatus")]
        public void UpdateStatusApi()
        {
            var updateStatusApi = new UpdateStatusPutApi();

            updateStatusApi.Run();
        }
    }
}
