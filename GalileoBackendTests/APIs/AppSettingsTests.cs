﻿using GalileoTestInfrastructure.GalileoApis.AppSettings;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class AppSettingsTests
    {
        [TestCase(UserRoles.Payment)]
        [TestCase(UserRoles.God)]
        [Category("Get AppSettings")]
        public void AppSettingsGet(UserRoles role)
        {
            var appSettingsGetApi = new AppSettingsGetApi(new Dictionary<string, string> { { "key", "1" } });

            appSettingsGetApi.AsUser(role).Run();

            if(role == UserRoles.Payment)
            {
                Assert.IsTrue(appSettingsGetApi.ResponseStatusCode == HttpStatusCode.Unauthorized
                    || appSettingsGetApi.ResponseStatusCode == HttpStatusCode.Forbidden, "status code in the response should be Forbidden");
            }
            else
            {
                var key = appSettingsGetApi.ResponseBody.GetIntValueFromPath("body.key");
                var value = appSettingsGetApi.ResponseBody.GetStringValueFromPath("body.value");

                Assert.Multiple(() =>
                {
                    Assert.IsTrue(key == 1, "key value should be 1 as it is sent in uri parameters");
                    Assert.IsTrue(!string.IsNullOrEmpty(value), "value (Sum difference allowed in %) has to hold a number");
                    Assert.IsTrue(appSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
                    Assert.IsTrue(appSettingsGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");
                });
            }
        }

        [Test]
        [Category("Put AppSettings")]
        public void AppSettingsPut()
        {
            var appSettingsPutApi = new AppSettingsPutApi();
            var rnd = new Random();
            var randomPercent = rnd.Next(0, 100);  // Sum difference allowed in %

            appSettingsPutApi.RequestPayload = @"{""key"":1,""value"":""RandomPercent""}"
                                               .Replace("RandomPercent", randomPercent.ToString());
            appSettingsPutApi.Run();

            var appSettingsGetApi = new AppSettingsGetApi(new Dictionary<string, string> { { "key", "1" } });
            appSettingsGetApi.Run();

            var actualPercent = appSettingsGetApi.ResponseBody.GetIntValueFromPath("body.value");

            Assert.Multiple(() =>
            {
                Assert.IsTrue(appSettingsPutApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
                Assert.IsTrue(appSettingsPutApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");
                Assert.AreEqual(randomPercent, actualPercent,
                    string.Format("Expected Percent is :{0}, but was:{1}", randomPercent, actualPercent));
            });
        }
    }
}
