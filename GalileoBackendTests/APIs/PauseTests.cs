﻿using GalileoTestInfrastructure.GalileoApis.Pause;
using GalileoTestInfrastructure.GalileoApis.PlotElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class PauseTests
    {
        [Test]
        [Category("Get Pause")]
        public void GetPause()
        {
            var pauseApi = new PauseControllerGetApi();
            pauseApi.Run();

            Assert.IsTrue(pauseApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pauseApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = pauseApi.ResponseBody.GetStringValueFromPath("body");
            var item = pauseApi.ResponseBody.GetStringValueFromPath("body.item");
            

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(body, "Body cannot be null");
                Assert.IsNotNull(item, "There should be an item!");
            });
        }





        [Test]
        [Category("PauseController Post")]
        public void PauseControllerPost()
        {
            var pauseController = new PauseControllerPostApi();

            var body = PauseControllerPostApi.DefaultPayload.ToJObject();
            body["permanentPauseController"] = 0;
            pauseController.RequestPayload = body.ToString();
            pauseController.Run();

            var pauseApi = new PauseControllerGetApi();
            pauseApi.Run();
            Assert.IsTrue(pauseApi.ResponseStatusCode == HttpStatusCode.OK, "Http returned {0}", pauseApi.ResponseStatusCode);
            Assert.IsTrue(pauseApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");
            Assert.IsTrue(pauseApi.ResponseBody.GetIntValueFromPath("body.item.permanentPauseController") == 0, "permanentPauseController");
        }

        
    }
}
