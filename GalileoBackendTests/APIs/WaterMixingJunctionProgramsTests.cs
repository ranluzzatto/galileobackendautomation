﻿using GalileoTestInfrastructure.GalileoApis.WaterMixingJunction;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class WaterMixingJunctionProgramsTests
    {
        [Test]
        [Category("Get WaterMixingJunction")]
        public void GetWaterMixingJunction()
        {
            var wmjGetApi = new WaterMixingJunctionGetApi();
            wmjGetApi.Run();

            Assert.IsTrue(wmjGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(wmjGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var waterMixingJunctionItems = wmjGetApi.ResponseBody["body"]["items"];


            Assert.Multiple(() =>
            {
                Assert.IsNotNull(waterMixingJunctionItems, "response should include items");
                Assert.IsTrue(waterMixingJunctionItems.Count() == 24, "there should be a 24 items in water mixing junction");

            });


        }

        [Test]
        [Category("Post WaterMixingJunction")]
        public void PostWaterMixingJunction()
        {
            var wmjPostApi = new WaterMixingJunctionPostApi();
            var bodyToEdit = WaterMixingJunctionPostApi.DefaultPayload.ToJObject();

            var name = "new name";
            var waterMixingGroupNumber = 1;
            var waterMixingMethod = 1;
            var salineWaterRequired = 55;
            var freshWaterRequired = 8;
            var ecRequired = 3;

            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].waterMixingGroupNumber", waterMixingGroupNumber);
            bodyToEdit.SetValueToPath("items[0].waterMixingMethod", waterMixingMethod);
            bodyToEdit.SetValueToPath("items[0].salineWaterRequired", salineWaterRequired);
            bodyToEdit.SetValueToPath("items[0].freshWaterRequired", freshWaterRequired);
            bodyToEdit.SetValueToPath("items[0].ecRequired", ecRequired);

            wmjPostApi.RequestPayload = bodyToEdit.ToString();
            wmjPostApi.Run();


            Assert.IsTrue(wmjPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(wmjPostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var wmjGetApi = new WaterMixingJunctionGetApi();

            wmjGetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(wmjGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(wmjGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].waterMixingGroupNumber", waterMixingGroupNumber.ToString()));
                Assert.IsTrue(wmjGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].waterMixingMethod", waterMixingMethod.ToString()));
                Assert.IsTrue(wmjGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].salineWaterRequired", salineWaterRequired.ToString()));
                Assert.IsTrue(wmjGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].freshWaterRequired", freshWaterRequired.ToString()));
                Assert.IsTrue(wmjGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].ecRequired", ecRequired.ToString()));
            });
        }


    }




}
