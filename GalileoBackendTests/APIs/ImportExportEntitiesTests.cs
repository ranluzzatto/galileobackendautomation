﻿using GalileoTestInfrastructure.GalileoServices;
using NUnit.Framework;

namespace GalileoBackendTests.APIs
{
    public class ImportExportEntitiesTests
    {
        [Test]
        [Category("ImportEntities")]
        public void ImportEntities()
        {
            var galileoImportService = new GalileoImportService();

            galileoImportService.Import("APIs\\ImportExportEntitiesTestsData\\GalileoDataControllerForInportService.json");
        }


        [Test]
        [Category("ExportEntities")]
        public void ExportEntities()
        {
            var gelileoExportService = new GalileoExportService();
            var exportData = gelileoExportService.GetControlerExportAsJObject();
            //example of parsing the exported data
            var xxx = exportData["DataCollectionSensor"][0]["LowValueForAlarm"];
            
        }
    }
}
