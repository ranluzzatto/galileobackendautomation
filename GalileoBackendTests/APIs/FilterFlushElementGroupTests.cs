﻿using GalileoTestInfrastructure.GalileoApis.Alarm;
using GalileoTestInfrastructure.GalileoApis.FilterFlushElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class FilterFlushElementGroupTests
    {

        [Test]
        [Category("Get FilterFlush")]
        public void GetFilterFlush()
        {
            var FilterFlushGetApi = new FilterFlushGetApi();

            FilterFlushGetApi.Run();

            Assert.IsTrue(FilterFlushGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(FilterFlushGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");


            Assert.Multiple(() =>
            {
                Assert.IsNotNull(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.conditionInputElements"), "response should include conditionInputElements");
                Assert.IsNotNull(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.connectionLineNumbers"), "response should include connectionLineNumbers");
                Assert.IsNotNull(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.privateWaterMeterElements"), "response should include privateWaterMeterElements");
                Assert.IsNotNull(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.units"), "response should include units");
                Assert.IsNotNull(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.waterMeterElements"), "response should include waterMeterElements");
                Assert.IsNotNull(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.waterMeterGroupNumbers"), "response should include waterMeterGroupNumbers");
                Assert.IsTrue(FilterFlushGetApi.ResponseBody["body"]["units"].Count() == 50, "there should be a 100 items in data colection sensor");






            });
        }

        [Test]
        [Category("Post FilterFlush")]

        public void PostFilterFlush()
        {
            var FilterFlushPostApi = new FilterFlushPostApi();
            var bodyToEdit = FilterFlushPostApi.DefaultPayload.ToJObject();

            //Edit an item
            var name = "new name";
            var delayBetweenFilters = 8;
            var delayForFlushMin = 5;
            var faultReaction = 0;
            var firstFilterNumber = 45;
            var flushingTime = 5;
            var irrigWhileFlushing = 0;
            var lastFilterNumber = 9;
            var maxContinousFlushes = 10;
            var startSustainBeforeFlush = 8;

            bodyToEdit.SetValueToPath("units[0].name", name);
            bodyToEdit.SetValueToPath("units[0].delayBetweenFilters", delayBetweenFilters);
            bodyToEdit.SetValueToPath("units[0].delayForFlushMin", delayForFlushMin);
            bodyToEdit.SetValueToPath("units[0].faultReaction", faultReaction);
            bodyToEdit.SetValueToPath("units[0].firstFilterNumber", firstFilterNumber);
            bodyToEdit.SetValueToPath("units[0].flushingTime", flushingTime);
            bodyToEdit.SetValueToPath("units[0].irrigWhileFlushing", irrigWhileFlushing);
            bodyToEdit.SetValueToPath("units[0].lastFilterNumber", lastFilterNumber);
            bodyToEdit.SetValueToPath("units[0].maxContinousFlushes", maxContinousFlushes);
            bodyToEdit.SetValueToPath("units[0].startSustainBeforeFlush", startSustainBeforeFlush);

            FilterFlushPostApi.RequestPayload = bodyToEdit.ToString();

            FilterFlushPostApi.Run();

            Assert.IsTrue(FilterFlushPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(FilterFlushPostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var FilterFlushGetApi = new FilterFlushGetApi();

            FilterFlushGetApi.Run();


            Assert.Multiple(() =>
            {
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.units[0].name") == name);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].delayBetweenFilters") == delayBetweenFilters);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].delayForFlushMin") == delayForFlushMin);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].faultReaction") == faultReaction);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].firstFilterNumber") == firstFilterNumber);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].flushingTime") == flushingTime);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].irrigWhileFlushing") == irrigWhileFlushing);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].lastFilterNumber") == lastFilterNumber);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].maxContinousFlushes") == maxContinousFlushes);
                Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].startSustainBeforeFlush") == startSustainBeforeFlush);
            });
        }

        [Test]
        [Category("RunEvents")]
        public void RunEvents()
        {
            var FilterFlushRunEventsApi = new FilterFlushRunEventsApi();
            var body = @"{""numbers"":[3]}"; // request to run events for item# 3

            FilterFlushRunEventsApi.RequestPayload = body;
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 5,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [
                                        3
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            FilterFlushRunEventsApi.AddRedisEventToValidate(eventToValidate);

            FilterFlushRunEventsApi.Run();

            Assert.IsTrue(FilterFlushRunEventsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(FilterFlushRunEventsApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(FilterFlushRunEventsApi.RedisValidationResult, "The expected Redis event was not found ");
        }

        [Test]
        [Category("SocketField")]
        public void SocketField()
        {
            var FilterFlush = new FilterFlushSocketFieldApi();
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 4,
                                      ""CommandCode"": 5,
                                      ""ComponentTypeCode"": 0,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": null,
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": {
                                        ""Numbers"": [
                                          1
                                        ],
                                        ""ParameterNumber"": 15,
                                        ""Value"": 1
                                      },
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0,
                                      ""GHNumber"": 0,
                                      ""InstanceNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            FilterFlush.AddRedisEventToValidate(eventToValidate);
            FilterFlush.RequestPayload = @"{""type"":3,""value"":1,""fieldName"":""ManualOveride"",""numbers"":[1]}";
            FilterFlush.Run();

            Assert.IsTrue(FilterFlush.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(FilterFlush.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(FilterFlush.RedisValidationResult, "The expected Redis event was not found ");
        }


    }



}

