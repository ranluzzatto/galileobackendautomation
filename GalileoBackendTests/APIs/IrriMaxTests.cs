﻿using GalileoTestInfrastructure.GalileoApis.IrriMax;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class IrriMaxTests
    {
        // TODO: check class tests after implementation
        [Test]
        [Category("Irrimax key - Get")]
        public void IrrimaxKeyGet()
        {
            var irrimaxKeyGetApi = new IrrimaxKeyGetApi();
            irrimaxKeyGetApi.Run();

            var result = irrimaxKeyGetApi.ResponseBody["body"];
            Assert.IsTrue(irrimaxKeyGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(irrimaxKeyGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsNotEmpty(result.GetStringValueFromPath("apiKey"));
        }

        [Test]
        [Category("Irrimax key - Put")]
        public void IrrimaxApiKeyPut()
        {
            var savePostApi = new IrrimaxKeyPutApi();
            var changedPayload = savePostApi.DefaultPayload.ToJObject();
            var newKey = "KeyCheck";
            changedPayload["ApiKey"] = newKey;
            savePostApi.RequestPayload = changedPayload.ToString();
            savePostApi.Run();

            Assert.IsTrue(savePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(savePostApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");
            Assert.IsTrue(savePostApi.ResponseBody.GetBoolValueFromPath("body"), "Put method body");

            // Get check put method
            var irrimaxApiKeyGetApi = new IrrimaxKeyGetApi();
            irrimaxApiKeyGetApi.Run();

            var result = irrimaxApiKeyGetApi.ResponseBody["body"];
            Assert.AreEqual(newKey, result.GetStringValueFromPath("apiKey"), "apiKey");
        }

        [Test]
        [Category("Exist - Get")]
        public void ExistGet()
        {
            // Get api key to check if exist
            var irrimaxKeyGetApi = new IrrimaxKeyGetApi();
            irrimaxKeyGetApi.Run();

            var apiKey = irrimaxKeyGetApi.ResponseBody["body"].GetStringValueFromPath("apiKey");
            Assert.IsTrue(irrimaxKeyGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(irrimaxKeyGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsNotEmpty(apiKey);

            // Check if exist
            var existGetApi = new ExistGetApi(new Dictionary<string, string> { { "apiKey", apiKey } });
            existGetApi.Run();

            Assert.IsTrue(existGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(existGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");
        }

        [Test]
        [Category("Irrimax")]
        [Category("Encrypted token")]
        public void EncryptedToken()
        {
            var encryptedtokenGetApi = new EncryptedTokenGetApi();
            encryptedtokenGetApi.Run();

            var result = encryptedtokenGetApi.ResponseBody["body"];
            Assert.IsTrue(encryptedtokenGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(encryptedtokenGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsNotEmpty(result.GetStringValueFromPath("encryptedToken"));
        }
    }
}
