using GalileoTestInfrastructure.GalileoApis.Auth;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace GalileoBackendTests.APIs
{
    public class AuthTests
    {
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// POST /auth/login
        /// </summary>
        [Test]
        [Category("AuthLogin")]
        public void SuccessfulLogin()
        {
            var authApi = new AuthLoginApi();

            authApi.RequestPayload = @" {
                                    ""userName"":""test"",
                                    ""password"": ""1234"",
                                    ""isMobile"": false
                                 }";
            authApi.Run();

            Assert.IsTrue(authApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(authApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");
            Assert.IsTrue(!string.IsNullOrEmpty(authApi.ResponseBody.GetStringValueFromPath("body.accountToken")), "Account Token must hold a value");
        }
        public void Session()
        {
            var authApi = new AuthLoginApi();

            authApi.RequestPayload = @" {
                                    ""userName"":""test"",
                                    ""password"": ""1234"",
                                    ""isMobile"": false
                                 }";
            authApi.Run();

            Assert.IsTrue(authApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(authApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");
            Assert.IsTrue(!string.IsNullOrEmpty(authApi.ResponseBody.GetStringValueFromPath("body.accountToken")), "Account Token must hold a value");
            var key = authApi.ResponseBody.GetStringValueFromPath("body.accountToken");
        }
        [Test]
        [Category("AuthLogout")]
        public void SuccessfulLogout()
        {
            var authApi = new AuthLogoutApi();

            authApi.RequestPayload = @" {
                                    ""isMobile"": false
                                 }";
            authApi.Run();

            Assert.IsTrue(authApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(authApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");
        }
        /// <summary>
        /// GET /user/exists
        /// </summary>
        [Test]
        [Category("UserExists")]
        public void UserExists()
        {
            var authUserExistsApi = new AuthUserExistsApi(new Dictionary<string, string> { { "userName", "test" } });

            authUserExistsApi.Run();

            Assert.IsTrue(authUserExistsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(authUserExistsApi.ResponseBody.GetBoolValueFromPath("result"), "galileo user 'test' was not found!");
        }

        /// <summary>
        /// POST /auth/token/refresh
        /// </summary>
        [Test]
        [Category("TokenRefresh")]
        public void TokenRefresh()
        {
            var authTokenRefreshApi = new AuthTokenRefreshApi();
            authTokenRefreshApi.RequestPayload = @"{ ""isMobile"":false}";

            authTokenRefreshApi.Run();

            var token = authTokenRefreshApi.ResponseBody.GetStringValueFromPath("body.accountToken");

            Assert.IsTrue(authTokenRefreshApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(authTokenRefreshApi.ResponseBody.GetBoolValueFromPath("result"), "result should be true");

            Assert.IsTrue(!string.IsNullOrEmpty(token), "Refreshed token cannot be empty");
            Assert.IsTrue(authTokenRefreshApi.ResponseBody.GetStringValueFromPath("body.accountToken") != AuthTokenRefreshApi.Token
                , "New token cannot be the same as a refreshed token");

            Thread.Sleep(1000);//since the token is generated based on DateTime.UtcNow,
                               //need to verify there is at least 1 second differance between token refresh

            authTokenRefreshApi.Run();//run again and validate a different token

            //second response token should differ from the first one
            var secondToken = authTokenRefreshApi.ResponseBody.GetStringValueFromPath("body.accountToken");

            Assert.IsTrue(token != secondToken
                , string.Format("New token cannot be the same as a post refresh token, \nToken1:{0}\n\n, Token2:{1}", token, secondToken));
        }
    }
}