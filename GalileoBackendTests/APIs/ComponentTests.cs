﻿using GalileoTestInfrastructure.GalileoServices;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;

namespace GalileoBackendTests.APIs
{
    public class ComponentTests
    {
        [Test]
        [Category("Get Components test")]
        public void GetComponents()
        {
            var importService = new GalileoImportService();
            //data file holds 2 sensors, number1 and number6 
            importService.Import(@"APIs\ComponentTestsData\SensorBaseData.json");

            var galileoComponentsService = new GalileoComponentsService();
            var components = galileoComponentsService.GetComponents(SetupElementType.OpenFieldSensor);

            Assert.IsTrue(components.Count == 2);

            var sensor1 = components.ElementAt(0);
            var sensor2 = components.ElementAt(1);

            Assert.Multiple(() =>
            {
                Assert.IsTrue(sensor1.Number == 1);
                Assert.IsTrue(sensor2.Number == 6);
                Assert.IsTrue(sensor1.Name == "Sensor 1");
                Assert.IsTrue(sensor2.Name == "Sensor 6");
                Assert.IsTrue(sensor1.Properties["MinValue"] == 1.ToString());
                Assert.IsTrue(sensor1.Properties["MaxValue"] == 3.ToString());
                Assert.IsTrue(sensor2.Properties["MinValue"] == 2.ToString());
                Assert.IsTrue(sensor2.Properties["MaxValue"] == 22.ToString());
            });
        }
    }
}
