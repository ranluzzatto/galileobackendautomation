﻿using GalileoTestInfrastructure.GalileoApis.Firmware;
using NUnit.Framework;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class FirmwareTests
    {
        [Test]
        [Category("Firmwares")]
        public void GetFirmwares()      
        {
            var firmwaresApi = new FirmwaresApi();

            firmwaresApi.Run();

            Assert.IsTrue(firmwaresApi.ResponseStatusCode == HttpStatusCode.OK,"status code in the response should be OK");
            Assert.IsTrue(firmwaresApi.ResponseBody != null);
        }
    }
}
