﻿using GalileoTestInfrastructure.GalileoApis.WaterMixingJunctionElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class WaterMixingJunctionElementGroupTests
    {
        [Test]
        [Category("Get WaterMixingJunctionElementGroup")]

        public void GetWaterMixingJunctionElementGroup()
        {
            var waterMixingJunctionElementGetApi = new WaterMixingJunctionElementGroupGetApi();
            waterMixingJunctionElementGetApi.Run();

            Assert.IsTrue(waterMixingJunctionElementGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(waterMixingJunctionElementGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

           
            var waterSourceItems = waterMixingJunctionElementGetApi.ResponseBody["body"]["waterSourceItems"];


            Assert.Multiple(() =>
            {
                
                Assert.IsNotNull(waterSourceItems, "response should include source items");
                Assert.IsNotNull(waterMixingJunctionElementGetApi.ResponseBody.GetStringValueFromPath("body.pipeLineNumbers"), "response should include pipeLineNumbers");
                Assert.IsNotNull(waterMixingJunctionElementGetApi.ResponseBody.GetStringValueFromPath("body.sensorElements"), "response should include sensorElements");
                Assert.IsNotNull(waterMixingJunctionElementGetApi.ResponseBody.GetStringValueFromPath("body.waterMixingJunctionPrograms"), "response should include waterMixingJunctionPrograms");
                Assert.IsNotNull(waterMixingJunctionElementGetApi.ResponseBody.GetStringValueFromPath("body.waterSourceSetup"), "response should include waterSourceSetup");
                Assert.IsTrue(waterSourceItems.Count() == 4, "there should be a 4 items in water source");
            });
        }





    }

    
}
