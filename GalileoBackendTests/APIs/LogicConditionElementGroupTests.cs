﻿using GalileoTestInfrastructure.GalileoApis.LogicConditionElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class LogicConditionElementGroupTests
    {
        [Test]
        [Category("Get LogicCondition")]
        public void GetLogicCondition()
        {
            var l_c_GetApi = new LogicConditionGetApi();
            l_c_GetApi.Run();

            Assert.IsTrue(l_c_GetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var logicConditionItems = l_c_GetApi.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(logicConditionItems, "response should include items");
                Assert.IsNotNull(l_c_GetApi.ResponseBody.GetStringValueFromPath("body.fertCenterNumbers"), "response should include fertCenterNumbers");
                Assert.IsNotNull(l_c_GetApi.ResponseBody.GetStringValueFromPath("body.irrigationPrograms"), "response should include irrigationPrograms");
                Assert.IsNotNull(l_c_GetApi.ResponseBody.GetStringValueFromPath("body.saleTableItems"), "response should include saleTableItems");
                Assert.IsNotNull(l_c_GetApi.ResponseBody.GetStringValueFromPath("body.waterPumpNumbers"), "response should include waterPumpNumbers");
                Assert.IsTrue(logicConditionItems.Count() == 60, "there should be a 60 items in logic condition");
            });

        }

        [Test]
        [Category("Post LogicCondition")]
        public void PostLogicCondition() 
        
        {
            var l_c_PostApi = new LogicConditionPostApi();
            var bodyToEdit = LogicConditionPostApi.DefaultPayload.ToJObject();

            var name = "new name";
            var condSetup = 1;
            var elementType = 1;
            var condType = 5;
            var linkType = 1;
            var hour = 2;
            var minute = 57;

            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].condSetup", condSetup);
            bodyToEdit.SetValueToPath("items[0].elementType", elementType);
            bodyToEdit.SetValueToPath("items[0].condType", condType);
            bodyToEdit.SetValueToPath("items[0].linkType", linkType);
            bodyToEdit.SetValueToPath("items[0].startTime.hour", hour);
            bodyToEdit.SetValueToPath("items[0].startTime.minute", minute);

            l_c_PostApi.RequestPayload = bodyToEdit.ToString();
            l_c_PostApi.Run();

            Assert.IsTrue(l_c_PostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(l_c_PostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var l_c_GetApi = new LogicConditionGetApi();

            l_c_GetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].condSetup", condSetup.ToString()));
                Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].elementType", elementType.ToString()));
                Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].condType", condType.ToString()));
                Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].linkType", linkType.ToString()));
                Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].startTime.hour", hour.ToString()));
                Assert.IsTrue(l_c_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].startTime.minute", minute.ToString()));
            });





        }





    }
}
