﻿using GalileoTestInfrastructure.GalileoApis.License;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class LicenseTests
    {
        [Test]
        [Category("Licenses - Get")]
        public void LicensesGet()
        {
            var licensesGetApi = new LicensesGetApi(new Dictionary<string, string> { { "page", "1" }, { "step", "100" } });
            licensesGetApi.Run();

            var licenses = licensesGetApi.ResponseBody["body"];
            Assert.IsTrue(licensesGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(licensesGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var license in licenses)
                {
                    Assert.IsNotNull(license, "License");
                }
            });
        }

        [Test]
        [Category("Count - Get")]
        public void CountGet()
        {
            var countGetApi = new CountGetApi();
            countGetApi.Run();

            var count = countGetApi.ResponseBody["body"].GetStringValueFromPath("count");
            Assert.IsTrue(countGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(countGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsNotEmpty(count, "count");
        }

        [Test]
        [Category("Users - Get")]
        public void UsersGet()
        {
            var usersGetApi = new UsersGetApi();
            usersGetApi.Run();

            var users = usersGetApi.ResponseBody["body"];
            Assert.IsTrue(usersGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(usersGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.Multiple(() =>
            {
                if(users != null)
                {
                    foreach (var user in users)
                    {
                        Assert.IsNotNull(user, "User");
                    }
                }            
            });
        }

        [Test]
        [Category("By license code - Get")]
        public void ByLicenseCodeGet()
        {
            var byLicenseCodeGetApi = new ByLicenseCodeGetApi(new Dictionary<string, string> { { "licenseCode", "4C3AD7044CC14E41" } });
            byLicenseCodeGetApi.Run();

            var licenses = byLicenseCodeGetApi.ResponseBody["body"];
            Assert.IsTrue(byLicenseCodeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(byLicenseCodeGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.Multiple(() =>
            {
                if (licenses != null)
                {
                    foreach (var license in licenses)
                    {
                        Assert.IsNotNull(license, "License");
                    }
                }
            });
        }

        public void getActiveLicense()
        {
            //TODO
        }
        [Test]
        [TestCase("0A11AAB0CD814466", true)]
        [TestCase("E2593C4D376E46A6", false)]
        [Category("Is active - Get")]
        public void IsActiveGet(string licenseCode, bool trueOrFalse)
        {
            var isActiveGetApi = new IsActiveGetApi(new Dictionary<string, string> { { "licenseCode", licenseCode } });
            isActiveGetApi.Run();

            Assert.IsTrue(isActiveGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(trueOrFalse == isActiveGetApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");
        }

        [Test]
        [TestCase(@"{""payer"":""Shlomi"",""receivedByID"":1}")]
        [TestCase(@"{""payer"":"""",""receivedByID"":null}")]
        [Category("Received by settings - Put")]
        public void ReceivedBySettingsPut(string payload)
        {
            var receivedBySettingsPutApi = new ReceivedBySettingsPutApi();
            receivedBySettingsPutApi.RequestPayload = payload;
            receivedBySettingsPutApi.Run();

            Assert.IsTrue(receivedBySettingsPutApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(receivedBySettingsPutApi.ResponseBody.GetBoolValueFromPath("result"), "Service result error");

            Assert.IsTrue(receivedBySettingsPutApi.ResponseBody.GetBoolValueFromPath("body"), "body");
        }
    }
}
