﻿using GalileoTestInfrastructure.GalileoApis.SensorElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class SensorGroupTests
    {
        [Test]
        [Category("Get SensorGroup")]

        public void GetSensorGroup()
        {
            var sensor_group_get_api = new SensorGetApi();
            sensor_group_get_api.Run();

            Assert.IsTrue(sensor_group_get_api.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(sensor_group_get_api.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var SensorsGroupItems = sensor_group_get_api.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(SensorsGroupItems, "response should include items");
                Assert.IsNotNull(sensor_group_get_api.ResponseBody.GetStringValueFromPath("body.sensorElements"), "response should include sensorElements");
                Assert.IsTrue(SensorsGroupItems.Count() == 20, "there should be a 20 items in sensor group");
            });

        }

        [Test]
        [Category("Post SensorGroup")]
        public void PostSensorGroup() 
        {
            var sensor_group_post_api = new SensorPostApi();
            var bodyToEdit = SensorPostApi.DefaultPayload.ToJObject();

            var name = "new name";
            var groupType = 2;
            var maxAlowedSensorsDiffer = 88;

            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].groupType", groupType);
            bodyToEdit.SetValueToPath("items[0].maxAlowedSensorsDiffer", maxAlowedSensorsDiffer);

            sensor_group_post_api.RequestPayload = bodyToEdit.ToString();
            sensor_group_post_api.Run();

            Assert.IsTrue(sensor_group_post_api.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(sensor_group_post_api.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var sensor_group_get_api = new SensorGetApi();
            sensor_group_get_api.Run();


            Assert.Multiple(() =>
            {
                Assert.IsTrue(sensor_group_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(sensor_group_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].groupType", groupType.ToString()));
                Assert.IsTrue(sensor_group_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].maxAlowedSensorsDiffer", maxAlowedSensorsDiffer.ToString()));
           
            });

        }

        [Test]
        [Category("RunEvents")]
        public void RunEvents() 
        {
            var sensorGroupRunEventsApi = new RunEventsApi();
            var body = @"{""numbers"":[1]}"; // request to run events for item# 3

            sensorGroupRunEventsApi.RequestPayload = body;
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 65,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [
                                        1
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0,
                                      ""GHNumber"": 0,
                                      ""InstanceNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
            sensorGroupRunEventsApi.AddRedisEventToValidate(eventToValidate);

            sensorGroupRunEventsApi.Run();

            Assert.IsTrue(sensorGroupRunEventsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(sensorGroupRunEventsApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(sensorGroupRunEventsApi.RedisValidationResult, "The expected Redis event was not found ");




        }


    }
}
