﻿using GalileoTestInfrastructure.GalileoApis.PauseDeviceElementGroup;
using GalileoTestInfrastructure.GalileoServices;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class PauseDeviceElementGroupTests
    {
        [Test]
        [Category("Get PauseDevice")]

        public void GetPauseDevice()
        {
            var pause_device_GetApi = new PauseDeviceGetApi();
            pause_device_GetApi.Run();

            Assert.IsTrue(pause_device_GetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
         
            var pauseDeviceItems = pause_device_GetApi.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(pauseDeviceItems, "response should include items");
                Assert.IsNotNull(pause_device_GetApi.ResponseBody.GetStringValueFromPath("body.conditionInputElementList"), "response should include conditionInputElementList");
                Assert.IsNotNull(pause_device_GetApi.ResponseBody.GetStringValueFromPath("body.pipeLineList"), "response should include pipeLineList");
                Assert.IsNotNull(pause_device_GetApi.ResponseBody.GetStringValueFromPath("body.sensorElementList"), "response should include sensorElementList");
                Assert.IsTrue(pauseDeviceItems.Count() == 20, "there should be a 20 items in data colection sensor");
            });


        }

        [Test]
        [Category("Post PauseDevice")]
        public void PostPauseDevice() 
        {
            var pause_device_PostApi = new PauseDevicePostApi();
            var bodyToEdit = PauseDevicePostApi.DefaultPayload.ToJObject();

            var name = "new name";
            var typeOfOper = 1;
            var operSetPointBelow = 6;
            var operSetPointAbove = 8;
            var operDelay = 9;
            var stopDelay = 7;
            var alarmDelay = 12;

            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].typeOfOper", typeOfOper);
            bodyToEdit.SetValueToPath("items[0].operSetPointBelow", operSetPointBelow);
            bodyToEdit.SetValueToPath("items[0].operSetPointAbove", operSetPointAbove);
            bodyToEdit.SetValueToPath("items[0].operDelay", operDelay);
            bodyToEdit.SetValueToPath("items[0].stopDelay", stopDelay);
            bodyToEdit.SetValueToPath("items[0].alarmDelay", alarmDelay);

            pause_device_PostApi.RequestPayload = bodyToEdit.ToString();
            pause_device_PostApi.Run();

            Assert.IsTrue(pause_device_PostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pause_device_PostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var pause_device_GetApi = new PauseDeviceGetApi();
            pause_device_GetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].typeOfOper", typeOfOper.ToString()));
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].operSetPointBelow", operSetPointBelow.ToString()));
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].operSetPointAbove", operSetPointAbove.ToString()));
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].operDelay", operDelay.ToString()));
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].stopDelay", stopDelay.ToString()));
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].alarmDelay", alarmDelay.ToString()));
            });


        }

        [Test]
        [Category("Assign Sensor Element,Condition Input To PauseDevice item")]

        public void AssignElementsToPauseDevice()
        {
            var exportService = new GalileoExportService();
            var e = exportService.GetControlerExportAsJObject();

          /*  var importService = new GalileoImportService();
            importService.Import(@"APIs\PauseDeviceTestsData\GalileoPauseDeviceBaseData.txt");

            var pause_device_GetApi = new PauseDeviceGetApi();
            pause_device_GetApi.Run();

            //get the related elements lists 
            var sensortElements = pause_device_GetApi.ResponseBody["body"]["sensorElementList"];
            var conditionInputElements = pause_device_GetApi.ResponseBody["body"]["conditionInputElementList"];

            //these values where asigned in the export file
            var sensorElementNumberToAssign = 1;
            var conditionInputToAssign = 1;

            //start editing the default(empty payload)
            var bodyToEdit = PauseDevicePostApi.DefaultPayload.ToJObject();

            var sensorElement = sensortElements.FirstOrDefault(s => s.GetIntValueFromPath("number") == sensorElementNumberToAssign);
            bodyToEdit.SetValueToPath("items[0].sensorElementID", sensorElement.GetIntValueFromPath("id"));

            var condInputElement = conditionInputElements.FirstOrDefault(a => a.GetIntValueFromPath("number") == conditionInputToAssign);
            bodyToEdit.SetValueToPath("items[0].conditionInputElementID", condInputElement.GetIntValueFromPath("id"));

            var pause_device_PostApi = new PauseDevicePostApi();

            pause_device_PostApi.RequestPayload = bodyToEdit.ToString();

            pause_device_PostApi.Run();
            pause_device_GetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].sensorElementID", sensorElement.GetIntValueFromPath("id").ToString()), "sensor element id mismatch");
                Assert.IsTrue(pause_device_GetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].conditionInputElementID", condInputElement.GetIntValueFromPath("id").ToString()), "cond input id mismatch");
            });*/


        }

        [Test]
        [Category("RunEvents")]
        public void RunEvents() 
        {
            var pause_device__run_events_api = new PauseDeviceRunEventsApi();
            var body = @"{""numbers"":[3]}"; // request to run events for item# 3
            pause_device__run_events_api.RequestPayload = body;

            //prepare redis expected data request
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 35,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [
                                      3
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0,
                                      ""GHNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
            pause_device__run_events_api.AddRedisEventToValidate(eventToValidate);
            pause_device__run_events_api.Run();

            Assert.IsTrue(pause_device__run_events_api.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pause_device__run_events_api.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(pause_device__run_events_api.RedisValidationResult, "The expected Redis event was not found ");





        }










    }
}
