﻿using GalileoTestInfrastructure.GalileoApis.PipeLineElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class PipeLineElementGroupTests 
    {
        [Test]
        [Category("Get PipeLine")]
        public void GetPipeLine()
        {
            var pipe_line_get_api = new PipeLineGetApi();
            pipe_line_get_api.Run();

            Assert.IsTrue(pipe_line_get_api.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var pipeLineItems = pipe_line_get_api.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(pipeLineItems, "response should include items");
                Assert.IsNotNull(pipe_line_get_api.ResponseBody.GetStringValueFromPath("body.conditionInputElements"), "response should include conditionInputElements");
                Assert.IsNotNull(pipe_line_get_api.ResponseBody.GetStringValueFromPath("body.irrigationPrograms"), "response should include irrigationPrograms");
                Assert.IsNotNull(pipe_line_get_api.ResponseBody.GetStringValueFromPath("body.plotNumbers"), "response should include plotNumbers");
                Assert.IsTrue(pipeLineItems.Count() == 50, "there should be a 100 items in pipe line");
            });


        }

        [Test]
        [Category("Post PipeLine")]

        public void PostPipeLine()
        {
            var pipe_line_post_api = new PipeLinePostApi();
            var bodyToEdit = PipeLinePostApi.DefaultPayload.ToJObject();

            var name = "AUTOMATION 123";
            var manualOverride = 1;
            var openLineVersFieldValves = 1;
            var delayForOpen = 20;
            var delayForClose = 30;
            var highFlowFaultstoPause = 5;
            var lineStatusInUncontWaterID = 1;
            var waterFlowLimit = 88;
            var hour = 9;
            var minute = 13;

            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].manualOverride", manualOverride);
            bodyToEdit.SetValueToPath("items[0].openLineVersFieldValves", openLineVersFieldValves);
            bodyToEdit.SetValueToPath("items[0].delayForOpen", delayForOpen);
            bodyToEdit.SetValueToPath("items[0].delayForClose", delayForClose);
            bodyToEdit.SetValueToPath("items[0].highFlowFaultstoPause", highFlowFaultstoPause);
            bodyToEdit.SetValueToPath("items[0].lineStatusInUncontWaterID", lineStatusInUncontWaterID);
            bodyToEdit.SetValueToPath("items[0].waterFlowLimit", waterFlowLimit);
            bodyToEdit.SetValueToPath("items[0].openTimeEndTime.hour", hour);
            bodyToEdit.SetValueToPath("items[0].openTimeEndTime.minute", minute);

            pipe_line_post_api.RequestPayload = bodyToEdit.ToString();

            pipe_line_post_api.Run();

            Assert.IsTrue(pipe_line_post_api.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pipe_line_post_api.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var pipe_line_get_api = new PipeLineGetApi();

            pipe_line_get_api.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].manualOverride", manualOverride.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].openLineVersFieldValves", openLineVersFieldValves.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].delayForOpen", delayForOpen.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].delayForClose", delayForClose.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].highFlowFaultstoPause", highFlowFaultstoPause.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].lineStatusInUncontWaterID", lineStatusInUncontWaterID.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].waterFlowLimit", waterFlowLimit.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].openTimeEndTime.hour", hour.ToString()));
                Assert.IsTrue(pipe_line_get_api.ResponseBody.IsPropertyPathEQValue("body.items[0].openTimeEndTime.minute", minute.ToString()));


            });


        }
        [Test]
        [Category("RunEvents")]
        public void RunEvents() 
        {
            var pipe_line_run_events_api = new PipeLineRunEventsApi();
            var body = @"{""numbers"":[3]}"; // request to run events for item# 3

            pipe_line_run_events_api.RequestPayload = body;
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 7,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [
                                        3
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0,
                                      ""GHNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
            pipe_line_run_events_api.AddRedisEventToValidate(eventToValidate);
            pipe_line_run_events_api.Run();

            Assert.IsTrue(pipe_line_run_events_api.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pipe_line_run_events_api.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(pipe_line_run_events_api.RedisValidationResult, "The expected Redis event was not found ");


        }

        [Test]
        [Category("SocketField - Reset Data")]
        public void SocketField_ResetData() 
        {
            var pipeLineSocketFieldApi = new PipeLineSocketFiledApi();

            //reset data for item group #2
            pipeLineSocketFieldApi.RequestPayload = @"{""type"":5,""value"":true,""fieldName"":""ClickForPermanentPause"",""numbers"":[2]}";
            var expectedRealTimeCommand = @"{
                                              ""SN"": ""SerialNumberPlaceHolder"",
                                              ""Type"": 4,
                                              ""CommandCode"": 7,
                                              ""ComponentTypeCode"": 0,
                                              ""CNFCode"": 0,
                                              ""Number"": 0,
                                              ""Numbers"": null,
                                              ""StatusCode"": 0,
                                              ""RealTimeValue"": {
                                                ""Numbers"": [
                                                  2
                                                ],
                                                ""ParameterNumber"": 0,
                                                ""Value"": 1
                                              },
                                              ""FirmwareId"": 0,
                                              ""LogType"": 0,
                                              ""GHNumber"": 0
                                            }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));



            pipeLineSocketFieldApi.AddRedisEventToValidate(expectedRealTimeCommand);
            pipeLineSocketFieldApi.Run();

            Assert.IsTrue(pipeLineSocketFieldApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(pipeLineSocketFieldApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(pipeLineSocketFieldApi.RedisValidationResult, "expected Redis event was not found ");


        }



    }
}
