﻿using GalileoTestInfrastructure.GalileoApis.BurstProtectionElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class BurstProtectionElementGroupTest
    {
        [Test]
        [Category("Get BurstProtectionElementGroup")]

        public void GetBurstProtectionElementGroup()
        {
            var BurstProtectionGetApi = new BurstProtectionElementGroupGetApi();

            BurstProtectionGetApi.Run();
            Assert.IsTrue(BurstProtectionGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var BurstProtectionElementGroupsItems = BurstProtectionGetApi.ResponseBody["body"]["items"];
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(BurstProtectionElementGroupsItems, "response should include items");
                Assert.IsNotNull(BurstProtectionGetApi.ResponseBody.GetStringValueFromPath("body.pipeLineList"), "response should include pipeLineList");
                Assert.IsNotNull(BurstProtectionGetApi.ResponseBody.GetStringValueFromPath("body.privateWaterMeterElements"), "response should include privateWaterMeterElements");
                Assert.IsNotNull(BurstProtectionGetApi.ResponseBody.GetStringValueFromPath("body.saleTableItems"), "response should include saleTableItems");
                Assert.IsNotNull(BurstProtectionGetApi.ResponseBody.GetStringValueFromPath("body.waterMeterElements"), "response should include waterMeterElements");
                Assert.IsNotNull(BurstProtectionGetApi.ResponseBody.GetStringValueFromPath("body.waterMeterGroupNumbers"), "response should include waterMeterGroupNumbers");
                Assert.IsTrue(BurstProtectionElementGroupsItems.Count() == 5, "there should be a 5 items in burst protection");


            });







        }


        [Test]
        [Category("Post BurstProtectionElementGroup")]

        public void BurstProtectionElementGroup()
        {
            var BurstProtectionPostApi = new BurstProtectionElementGroupPostApi();


            //defult payload
            BurstProtectionPostApi.RequestPayload = BurstProtectionPostApi.DefaultPayload;
            BurstProtectionPostApi.Run();

            //us default "empty" body
            var bodyToEdit = JObject.Parse(BurstProtectionPostApi.DefaultPayload);

            //Edit an item
            var name = "new name7";
            var autoCancelAlarm = false;
            var delayNegFlowMin = 11;
            var delayPosFlowMin = 12;
            var maxNegativeFlow = 5.5;
            var maxPositiveFlow = 5;
            var moreFlowFromCommTable = true;
            var negativeFaultAction = 2;





            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].autoCancelAlarm", autoCancelAlarm);
            bodyToEdit.SetValueToPath("items[0].delayNegFlowMin", delayNegFlowMin);
            bodyToEdit.SetValueToPath("items[0].delayPosFlowMin", delayPosFlowMin);
            bodyToEdit.SetValueToPath("items[0].maxNegativeFlow", maxNegativeFlow);
            bodyToEdit.SetValueToPath("items[0].maxPositiveFlow", maxPositiveFlow);
            bodyToEdit.SetValueToPath("items[0].moreFlowFromCommTable", moreFlowFromCommTable);
            bodyToEdit.SetValueToPath("items[0].negativeFaultAction", negativeFaultAction);




            BurstProtectionPostApi.RequestPayload = bodyToEdit.ToString();

            BurstProtectionPostApi.ValidateReadCnfRedisEvent = true;

            BurstProtectionPostApi.Run();



            Assert.IsTrue(BurstProtectionPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(BurstProtectionPostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(BurstProtectionPostApi.ReadCnfRedisEventValidationResult);
            var BurstProtectionGetApi = new BurstProtectionElementGroupGetApi();
            BurstProtectionGetApi.Run();




            Assert.Multiple(() =>
            {
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].autoCancelAlarm", autoCancelAlarm.ToString()));
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].delayNegFlowMin", delayNegFlowMin.ToString()));
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].delayPosFlowMin", delayPosFlowMin.ToString()));
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].maxNegativeFlow", maxNegativeFlow.ToString()));
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].maxPositiveFlow", maxPositiveFlow.ToString()));
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].moreFlowFromCommTable", moreFlowFromCommTable.ToString()));
                Assert.IsTrue(BurstProtectionGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].negativeFaultAction", negativeFaultAction.ToString()));



            });



        }
        [Test]
        [Category("RunEvents")]
        public void RunEvents()
        {
            var BurstProtectionRunEventsApi = new BurstProtectionElementGroupRunEventsApi();
            var body = @"{""numbers"":[4]}"; // request to run events for item# 4
            BurstProtectionRunEventsApi.RequestPayload = body;
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                        ""Type"": 1,
                                          ""CommandCode"": 34,
                                          ""ComponentTypeCode"": 1,
                                          ""CNFCode"": 0,
                                          ""Number"": 0,
                                          ""Numbers"": [
                                           4
                                          ],
                                          ""StatusCode"": 0,
                                          ""RealTimeValue"": null,
                                          ""FirmwareId"": 0,
                                          ""LogType"": 0,
                                          ""GHNumber"": 0,
                                          ""InstanceNumber"": 0
                                      
        }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
            BurstProtectionRunEventsApi.AddRedisEventToValidate(eventToValidate);
            BurstProtectionRunEventsApi.Run();

            Assert.IsTrue(BurstProtectionRunEventsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(BurstProtectionRunEventsApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(BurstProtectionRunEventsApi.RedisValidationResult, "The expected Redis event was not found ");

        }

        [Test]
        [Category("SocketField - Cancel Fault ")]
        public void SocketField_CancelFault()
        {
            var dcsSocketFieldApi = new BurstProtectionElementGroupSocketFiledApi();
            dcsSocketFieldApi.RequestPayload = @"{""type"":3,""value"":1,""fieldName"":""CancelFault"",""numbers"":[1]}";
            var expectedRealTimeCommand = @"{
                                              ""SN"": ""SerialNumberPlaceHolder"",
                                              ""Type"": 4,
                                              ""CommandCode"": 34,
                                              ""ComponentTypeCode"": 0,
                                              ""CNFCode"": 0,
                                              ""Number"": 0,
                                              ""Numbers"": null,
                                              ""StatusCode"": 0,
                                              ""RealTimeValue"": {
                                              ""Numbers"": [
                                                1
                                                ],
                                              ""ParameterNumber"": 5,
                                              ""Value"": 1
                                               },
                                               ""FirmwareId"": 0,
                                               ""LogType"": 0
                                             }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

          
            dcsSocketFieldApi.AddRedisEventToValidate(expectedRealTimeCommand);
            dcsSocketFieldApi.Run();

            Assert.IsTrue(dcsSocketFieldApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(dcsSocketFieldApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(dcsSocketFieldApi.RedisValidationResult, "expected Redis event was not found");





        }





    }
}