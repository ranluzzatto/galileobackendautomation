﻿using GalileoTestInfrastructure.GalileoApis.DataCollectionSensorElementGroup;
using GalileoTestInfrastructure.GalileoServices;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class DataCollectionSensorElementGroupTests
    {
        /// <summary>
        /// GET /controller/{controllerId}/element-group/data-collection-sensor
        /// </summary>
        [Test]
        [Category("Get DataCollectionSensor")]
        public void GetDataCollectionSensor()
        {
            var dcsGetApi = new DataCollectionSensorGetApi();

            dcsGetApi.Run();

            Assert.IsTrue(dcsGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var dataCollectionSensorsItems = dcsGetApi.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(dataCollectionSensorsItems, "response should include items");
                Assert.IsNotNull(dcsGetApi.ResponseBody.GetStringValueFromPath("body.sensorElements"), "response should include sensorElements");
                Assert.IsNotNull(dcsGetApi.ResponseBody.GetStringValueFromPath("body.auxilaryOutputElements"), "response should include auxilaryOutputElements");
                Assert.IsNotNull(dcsGetApi.ResponseBody.GetStringValueFromPath("body.sensorGroupNumbers"), "response should include sensorGroupNumbers");
                Assert.IsTrue(dataCollectionSensorsItems.Count() == 100, "there should be a 100 items in data colection sensor");
            });
        }

        [Test]
        [Category("Post DataCollectionSensor")]
        public void PostDataCollectionSensor()
        {
            var dcsPostApi = new DataCollectionSensorPostApi();

            //us default "empty" body
            var bodyToEdit = DataCollectionSensorPostApi.DefaultPayload.ToJObject();

            //Edit an item
            var name = "new name";
            var lowValueForAlarm = 3;
            var highValueForAlarm = 5;
            var alarmDelay = 12;
            var sensorType = 2;
            var sensor = 19;

            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].lowValueForAlarm", lowValueForAlarm);
            bodyToEdit.SetValueToPath("items[0].highValueForAlarm", highValueForAlarm);
            bodyToEdit.SetValueToPath("items[0].alarmDelay", alarmDelay);
            bodyToEdit.SetValueToPath("items[0].sensorType", sensorType);//sensor type = sensor group
            bodyToEdit.SetValueToPath("items[0].sensor", sensor);

            dcsPostApi.RequestPayload = bodyToEdit.ToString();

            dcsPostApi.Run();

            Assert.IsTrue(dcsPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(dcsPostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            //get the edited item and validate it
            var dcsGetApi = new DataCollectionSensorGetApi();

            dcsGetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].lowValueForAlarm", lowValueForAlarm.ToString()));
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].highValueForAlarm", highValueForAlarm.ToString()));
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].alarmDelay", alarmDelay.ToString()));
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].sensorType", sensorType.ToString()));
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].sensor", sensor.ToString()));
            });
        }

        [Test]
        [Category("RunEvents")]
        public void RunEvents()
        {
            var dcsRunEventsApi = new DataCollectionSensorRunEventsApi();
            var body = @"{""numbers"":[3]}"; // request to run events for item# 3

            dcsRunEventsApi.RequestPayload = body;

            //prepare redis expected data request
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 32,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [
                                        3
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            dcsRunEventsApi.AddRedisEventToValidate(eventToValidate);

            dcsRunEventsApi.Run();

            Assert.IsTrue(dcsRunEventsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(dcsRunEventsApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(dcsRunEventsApi.RedisValidationResult, "The expected Redis event was not found ");

        }

        [Test]
        [Category("SocketField - Reset Data")]
        public void SocketField_ResetData()
        {
            var dcsSocketFieldApi = new DataCollectionSensorSocketFiledApi();

            //reset data for item group #2
            dcsSocketFieldApi.RequestPayload = @"{""type"":5,""value"":true,""fieldName"":""ResetData"",""numbers"":[2]}";
            var expectedRealTimeCommand = @"{
  ""SN"": ""SerialNumberPlaceHolder"",
  ""Type"": 4,
  ""CommandCode"": 32,
  ""ComponentTypeCode"": 0,
  ""CNFCode"": 0,
  ""Number"": 0,
  ""Numbers"": null,
  ""StatusCode"": 0,
  ""RealTimeValue"": {
    ""Numbers"": [
      2
    ],
    ""ParameterNumber"": 7,
    ""Value"": 1
  },
  ""FirmwareId"": 0,
  ""LogType"": 0
}".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            dcsSocketFieldApi.AddRedisEventToValidate(expectedRealTimeCommand);

            dcsSocketFieldApi.Run();

            Assert.IsTrue(dcsSocketFieldApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(dcsSocketFieldApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(dcsSocketFieldApi.RedisValidationResult, "expected Redis event was not found ");
        }

        [Test]
        [Category("SocketField - Sensor Initiation")]
        public void SocketField_SensorInitiation()
        {
            var dcsSocketFieldApi = new DataCollectionSensorSocketFiledApi();

            //reset data for item group #2
            dcsSocketFieldApi.RequestPayload = @"{""type"":5,""value"":true,""fieldName"":""SensorInitiation"",""numbers"":[1]}";
            var expectedRealTimeCommand = @"{
  ""SN"": ""SerialNumberPlaceHolder"",
  ""Type"": 4,
  ""CommandCode"": 32,
  ""ComponentTypeCode"": 0,
  ""CNFCode"": 0,
  ""Number"": 0,
  ""Numbers"": null,
  ""StatusCode"": 0,
  ""RealTimeValue"": {
    ""Numbers"": [
      1
    ],
    ""ParameterNumber"": 8,
    ""Value"": 1
  },
  ""FirmwareId"": 0,
  ""LogType"": 0
}".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            dcsSocketFieldApi.AddRedisEventToValidate(expectedRealTimeCommand);

            dcsSocketFieldApi.Run();

            Assert.IsTrue(dcsSocketFieldApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(dcsSocketFieldApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(dcsSocketFieldApi.RedisValidationResult, "expected Redis event was not found ");
        }

        [Test]
        [Category("List")]
        public void List()
        {
            var dcsListApi = new DataCollectionSensorListApi();

            dcsListApi.Run();

            var items = dcsListApi.ResponseBody["body"];

            Assert.IsTrue(dcsListApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(dcsListApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            Assert.Multiple(() =>
            {
                Assert.IsTrue(items.Count() == 100, "there should be a 100 items in data colection sensor");
                foreach (var item in items)
                    Assert.IsTrue(item["id"].ToString() == item["number"].ToString(), "item ID and item Number did not match");
            });
        }

        [Test]
        [Category("Assign Sensor Element,Group, Auxilery output To DataCollectionSensor item")]
        public void AssignElementsToDataCollectionSensor()
        {
            var importService = new GalileoImportService();
            importService.Import(@"APIs\DataCollectionSensorTestsData\DataCollectionSensorBaseData.txt");

            var dcsGetApi = new DataCollectionSensorGetApi();
            dcsGetApi.Run();

            //get the related elements lists 
            var sensotElementsList = dcsGetApi.ResponseBody["body"]["sensorElements"];
            var auxilaryOutputElementsList = dcsGetApi.ResponseBody["body"]["auxilaryOutputElements"];

            //these values where asigned in the export file
            var sensorElementNumberToAssign = 6;
            var sensorGroupNumberToAssign = 1;
            var auxilaryOutputElementNumberToAssign = 4;

            //start editing the default(empty payload)
            var bodyToEdit = DataCollectionSensorPostApi.DefaultPayload.ToJObject();

            bodyToEdit.SetValueToPath("items[1].name", "selectedSensor6");
            bodyToEdit.SetValueToPath("items[1].sensorType", 1);//sensor type = sensor element

            bodyToEdit.SetValueToPath("items[2].name", "selectedGroup1");
            bodyToEdit.SetValueToPath("items[2].sensorType", 2);//sensor type = sensor group
            bodyToEdit.SetValueToPath("items[2].sensor", sensorGroupNumberToAssign);

            var sensorElement = sensotElementsList.FirstOrDefault(s => s.GetIntValueFromPath("number") == sensorElementNumberToAssign);
            bodyToEdit.SetValueToPath("items[1].sensor", sensorElement.GetIntValueFromPath("id"));

            var auxOutputElement = auxilaryOutputElementsList.FirstOrDefault(a => a.GetIntValueFromPath("number") == auxilaryOutputElementNumberToAssign);
            bodyToEdit.SetValueToPath("items[2].auxOutputElementID", auxOutputElement.GetIntValueFromPath("id"));

            var dcsPostApi = new DataCollectionSensorPostApi();

            dcsPostApi.RequestPayload = bodyToEdit.ToString();

            dcsPostApi.Run();
            dcsGetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[1].sensorType", 1.ToString()), "sensor type should be 1 = selesor element");
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[1].sensor", sensorElement.GetIntValueFromPath("id").ToString()), "sensor element id mismatch");
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[2].sensorType", 2.ToString()), "sensor type should be 2 = selesor group");
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[2].sensor", sensorGroupNumberToAssign.ToString()), "sensor type should be 1 = selesor element");
                Assert.IsTrue(dcsGetApi.ResponseBody.IsPropertyPathEQValue("body.items[2].auxOutputElementID", auxOutputElement.GetIntValueFromPath("id").ToString()), "sensor auxOutputElementID id mismatch");
            });
        }
    }
}
