﻿using GalileoTestInfrastructure.GalileoApis.IrrigationProgramOperationTable;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class IrrigationProgramOperationTableTests
    {
        [Test]
        [Category("Irrigation program operation table - Get")]
        public void IrrigationProgramOperationTableGet()
        {
            var irrigationProgramOperationTableGet = new IrrigationProgramOperationTableGetApi();
            irrigationProgramOperationTableGet.Run();

            Assert.IsTrue(irrigationProgramOperationTableGet.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramOperationTableGet.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var valveElements = irrigationProgramOperationTableGet.ResponseBody["body"]["valveElements"];
            var valveGroupItems = irrigationProgramOperationTableGet.ResponseBody["body"]["valveGroupItems"];
            var fertilizerCenterProgramItems = irrigationProgramOperationTableGet.ResponseBody["body"]["fertilizerCenterProgramItems"];
            var fertilizerCenterGroupItems = irrigationProgramOperationTableGet.ResponseBody["body"]["fertilizerCenterGroupItems"];
            var priorityGroupItems = irrigationProgramOperationTableGet.ResponseBody["body"]["priorityGroupItems"];
            var fertilizerPumpGroupNumbers = irrigationProgramOperationTableGet.ResponseBody["body"]["fertilizerPumpGroupNumbers"];
            var items = irrigationProgramOperationTableGet.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                var res = valveElements.GetChildrenCount() == 0;
                if (valveElements != null || !res)
                {
                    Assert.IsNotNull(valveElements, "valveElements");
                }

                res = valveGroupItems.GetChildrenCount() == 0;
                if (valveGroupItems != null || !res)
                {
                    Assert.IsNotNull(valveGroupItems, "valveGroupItems");
                }

                res = fertilizerCenterProgramItems.GetChildrenCount() == 0; 
                if (fertilizerCenterProgramItems != null || !res)
                {
                    Assert.IsNotNull(fertilizerCenterProgramItems, "fertilizerCenterProgramItems");
                }

                res = fertilizerCenterGroupItems.GetChildrenCount() == 0;
                if (fertilizerCenterGroupItems != null || !res)
                {
                    Assert.IsNotNull(fertilizerCenterGroupItems, "fertilizerCenterGroupItems");
                }

                res = priorityGroupItems.GetChildrenCount() == 0;
                if (priorityGroupItems != null || !res)
                {
                    Assert.IsNotNull(priorityGroupItems, "priorityGroupItems");
                }

                res = fertilizerPumpGroupNumbers.GetChildrenCount() == 0;
                if (fertilizerPumpGroupNumbers != null || !res)
                {
                    Assert.IsNotNull(fertilizerPumpGroupNumbers, "fertilizerPumpGroupNumbers");
                }

                res = items.GetChildrenCount() == 0;
                if (items != null || !res)
                {
                    Assert.IsNotNull(items, "items");
                }
            });
        }

        [Test]
        [Category("Irrigation program operation table - Post")]
        public void IrrigationProgramOperationTablePost()
        {
            var irrigationProgramOperationTablePost = new IrrigationProgramOperationTablePostApi();
            var changedPayload = irrigationProgramOperationTablePost.DefaultPayload.ToJObject();

            var data = 1;
            foreach(var item in changedPayload["items"])
            {
                item["activity"] = "Active";
                item["valveAType"] = data;
            }

            irrigationProgramOperationTablePost.RequestPayload = changedPayload.ToString();
            irrigationProgramOperationTablePost.Run();

            Assert.IsTrue(irrigationProgramOperationTablePost.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK, received {0} instead", irrigationProgramOperationTablePost.ResponseStatusCode);
            Assert.IsTrue(irrigationProgramOperationTablePost.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            // Get for post check
            var irrigationProgramOperationTableGet = new IrrigationProgramOperationTableGetApi();
            irrigationProgramOperationTableGet.Run();

            Assert.IsTrue(irrigationProgramOperationTableGet.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramOperationTableGet.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var items = irrigationProgramOperationTableGet.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                var res = items.GetChildrenCount() == 0;
                if (items != null || !res)
                {
                    for(var i=0; i<3; i++)
                    {
                        Assert.AreEqual(data, items[i].GetIntValueFromPath("activity"), "item.activity");
                        Assert.AreEqual(data, items[i].GetIntValueFromPath("valveAType"), "item.valveAType");
                    }
                }
            });
        }

        [Test]
        [Category("Socket field - Put")]
        public void SocketFieldPut()
        {
            var socketFieldPutApi = new SocketFieldPutApi();
            socketFieldPutApi.RequestPayload = socketFieldPutApi.DefaultPayload;
            socketFieldPutApi.Run();

            Assert.IsTrue(socketFieldPutApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(socketFieldPutApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            Assert.IsTrue(socketFieldPutApi.ResponseBody.GetBoolValueFromPath("body"), "body");
        }

        [Test]
        [Category("Filters - Get")]
        public void FiltersGet()
        {
            var filtersGetApi = new FiltersGetApi();
            filtersGetApi.Run();

            Assert.IsTrue(filtersGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(filtersGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = filtersGetApi.ResponseBody["body"];

            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(body.GetStringValueFromPath("programNumberFrom"), "programNumberFrom");
                Assert.IsNotEmpty(body.GetStringValueFromPath("programNumberTo"), "programNumberTo");
                Assert.IsNotEmpty(body["statuses"],"statuses");
                Assert.IsNotEmpty(body.GetStringValueFromPath("activity"), "activity");
                Assert.IsNotEmpty(body.GetStringValueFromPath("localFertPumpNumber"), "localFertPumpNumber");
                Assert.IsNotEmpty(body.GetStringValueFromPath("fertCenterNumber"), "fertCenterNumber");
                Assert.IsNotEmpty(body.GetStringValueFromPath("fertCenterProgNumber"), "fertCenterProgNumber");
            });
        }

        [Test]
        [Category("Filters - Put")]
        public void FiltersPut()
        {
            var filtersPostApi = new FiltersPutApi();
            filtersPostApi.RequestPayload = filtersPostApi.DefaultPayload;
            filtersPostApi.Run();

            Assert.IsTrue(filtersPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(filtersPostApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            // Get - check post
            var filtersGetApi = new FiltersGetApi();
            filtersGetApi.Run();

            Assert.IsTrue(filtersGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(filtersGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            // Values of payload to compare the get response
            var payload = filtersPostApi.DefaultPayload.ToJObject();
            var programNumberFrom = payload.GetStringValueFromPath("programNumberFrom");
            var programNumberTo = payload.GetStringValueFromPath("programNumberTo");
            var localFertPumpNumber = payload.GetStringValueFromPath("localFertPumpNumber");
            var fertCenterNumber = payload.GetStringValueFromPath("fertCenterNumber");
            var fertCenterProgNumber = payload.GetStringValueFromPath("fertCenterProgNumber");
            var activity = payload["activity"];

            var body = filtersGetApi.ResponseBody["body"];
            Assert.Multiple(() =>
            {
                Assert.AreEqual(programNumberFrom, body.GetStringValueFromPath("programNumberFrom"), "programNumberFrom");
                Assert.AreEqual(programNumberTo, body.GetStringValueFromPath("programNumberTo"), "programNumberTo");
                Assert.IsNotNull(body["statuses"], "statuses");
                Assert.AreEqual(body["activity"],activity, "activity");
                Assert.AreEqual(localFertPumpNumber, body["localFertPumpNumber"], "localFertPumpNumber");
                Assert.AreEqual(fertCenterNumber, body["fertCenterNumber"], "fertCenterNumber");
                Assert.AreEqual(fertCenterProgNumber, body["fertCenterProgNumber"], "fertCenterProgNumber");
            });
        }

        [Test]
        [Category("Run events - Post")]
        public void RunEventsPost()
        {
            var runEventsPostApi = new RunEventsPostApi();
            runEventsPostApi.RequestPayload = runEventsPostApi.DefaultPayload;
            runEventsPostApi.Run();

            Assert.IsTrue(runEventsPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(runEventsPostApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            Assert.IsTrue(runEventsPostApi.ResponseBody.GetBoolValueFromPath("body"), "body");
        }
    }
}
