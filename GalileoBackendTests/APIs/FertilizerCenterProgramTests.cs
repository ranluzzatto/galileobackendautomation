﻿using GalileoTestInfrastructure.GalileoApis.FertilizerCenterProgram;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class FertilizerCenterProgramTests
    {

        [Test]
        [Category("Get FertilizerCenterProgram")]
        public void GetFertilizerCenterProgram() 
        {
            var fcpGetApi = new FertilizerCenterProgramGetApi();
            fcpGetApi.Run();

            Assert.IsTrue(fcpGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(fcpGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var fertilizerCenterProgramsItems = fcpGetApi.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(fertilizerCenterProgramsItems, "response should include items");
                Assert.IsNotNull(fcpGetApi.ResponseBody.GetStringValueFromPath("body.fertPumpGroups"), "response should include fertPumpGroups");
                Assert.IsNotNull(fcpGetApi.ResponseBody.GetStringValueFromPath("body.fertSelectorElements"), "response should include fertSelectorElements");
                Assert.IsNotNull(fcpGetApi.ResponseBody.GetStringValueFromPath("body.fertilizerCenterItems"), "response should include fertilizerCenterItems");
                Assert.IsTrue(fertilizerCenterProgramsItems.Count() == 160, "there should be a 160 items in fertilizer program center");
            });


        }

        [Test]
        [Category("Post FertilizerCenterProgram")]
        public void PostFertilizerCenterProgram() 
        {
            var fcpPostApi = new FertilizerCenterProgramPostApi();
            var bodyToEdit = FertilizerCenterProgramPostApi.DefaultPayload.ToJObject();

            var name = "new name";
            var ecRequire = 2;
            var phRequire = 5;

            bodyToEdit.SetValueToPath("items[0].name", name);
            bodyToEdit.SetValueToPath("items[0].ecRequire", ecRequire);
            bodyToEdit.SetValueToPath("items[0].phRequire", phRequire);

            fcpPostApi.RequestPayload = bodyToEdit.ToString();
            fcpPostApi.Run();

            Assert.IsTrue(fcpPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(fcpPostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var fcpGetApi = new FertilizerCenterProgramGetApi();
            fcpGetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(fcpGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
                Assert.IsTrue(fcpGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].ecRequire", ecRequire.ToString()));
                Assert.IsTrue(fcpGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].phRequire", phRequire.ToString()));
               
            });



        }

    }



}

