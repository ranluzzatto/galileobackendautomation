﻿using GalileoTestInfrastructure.GalileoApis.AuxilaryOutputElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class AuxilaryOutputElementGroupTests
    {
        [Test]
        [Category("Get AuxilaryOutput")]
        public void AuxilaryOutputGet()
        {
            var auxilaryOutputGetApi = new AuxilaryOutputGetApi();

            auxilaryOutputGetApi.Run();

            var analogSensorElements = auxilaryOutputGetApi.ResponseBody["body"]["analogSensorElements"];
            var conditionInputElements = auxilaryOutputGetApi.ResponseBody["body"]["conditionInputElements"];
            var logicConditionElements = auxilaryOutputGetApi.ResponseBody["body"]["logicConditionElements"];
            var programItems = auxilaryOutputGetApi.ResponseBody["body"]["programItems"];
            var setup = auxilaryOutputGetApi.ResponseBody["body"]["setup"];

            Assert.Multiple(() =>
            {
                Assert.IsTrue(auxilaryOutputGetApi.ResponseStatusCode == HttpStatusCode.OK,
                    "status code in the response should be OK");

                Assert.IsTrue(auxilaryOutputGetApi.ResponseBody.GetBoolValueFromPath("result"),
                    "Result in the body must be true");

                Assert.IsNotNull(analogSensorElements);
                Assert.IsNotNull(conditionInputElements);
                Assert.IsNotNull(logicConditionElements);
                Assert.IsNotNull(programItems);
                Assert.IsNotNull(setup);

                //Auxilary output group size should be 20
                Assert.AreEqual(programItems.Count(), 20, "Auxilary output group size should be 20");
            });
        }

        [TestCase(@"[1,2,3,4,5,6,7,8,9,10]")]
        [TestCase(@"[19]")]
        [Category("RunEvents")]
        public void RunEvents(string auxilaryOutputNumbers)
        {
            var auxilaryOutputRunEventsApi = new AuxilaryOutputRunEventsApi();

            auxilaryOutputRunEventsApi.RequestPayload = @"{""numbers"":AuxNumbers}".Replace("AuxNumbers", auxilaryOutputNumbers);

            var expectedEvent = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 19,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": AuxNumbers,
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0
}".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"))
                                   .Replace("AuxNumbers", auxilaryOutputNumbers);

            auxilaryOutputRunEventsApi.AddRedisEventToValidate(expectedEvent);
            auxilaryOutputRunEventsApi.Run();

            Assert.IsTrue(auxilaryOutputRunEventsApi.RedisValidationResult, "expected redis mseeage was not found");
        }

        [TestCase(@"[6]")]
        [TestCase(@"[14]")]
        [Category("AuxilaryOutput socket-field CancelFaultStatus")]
        public void SocketField_CancelFaultStatus(string auxNumber)
        {
            var auxilaryOutputSocketFieldApi = new AuxilaryOutputSocketFieldApi();

            auxilaryOutputSocketFieldApi.RequestPayload = @"{""type"":5,""value"":true,""fieldName"":""CancelFaultStatus"",""numbers"":auxNumberPlaceHolder}".Replace("auxNumberPlaceHolder", auxNumber);

            var expectedEvent = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 4,
                                      ""CommandCode"": 19,
                                      ""ComponentTypeCode"": 0,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": null,
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": {
                                        ""Numbers"": auxNumberPlaceHolder,
                                        ""ParameterNumber"": 17,
                                        ""Value"": 1
                                      },
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0
                                    }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"))
                                    .Replace("auxNumberPlaceHolder", auxNumber);

            auxilaryOutputSocketFieldApi.AddRedisEventToValidate(expectedEvent);

            auxilaryOutputSocketFieldApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(auxilaryOutputSocketFieldApi.RedisValidationResult, "redis validation event was not ");

                Assert.IsTrue(auxilaryOutputSocketFieldApi.ResponseStatusCode == HttpStatusCode.OK,
                        "status code in the response should be OK");
                Assert.IsTrue(auxilaryOutputSocketFieldApi.ResponseBody.GetBoolValueFromPath("result"),
                    "Result in the body must be true");
            });
        }

        [Test]
        [Category("PostAuxiliaryOutputs")] //Post
        public void PostAuxiliaryOutput()
        {
            var PostAuxiliaryOutput = new AuxilaryOutputElementGroupPostApi();
            var bodyToEdit = AuxilaryOutputElementGroupPostApi.DefaultPayload.ToJObject();

            var analogSensorElementID = 403376;
            var analogSensorType = 1;
            var condInputToPauseElementID = 403378;
            var delayTime = 0;
            var differentialToStop = 90;
            var logicConditionNumber1 = 1;
            var logicConditionNumber2 = 2;
            var name = "Alisaaa";
            var operTime = 10;
            var operationMode = 1;
            var sequencialOperation = true;
            var startSetPointAbove = 100;
            var startSetPointBelow = 10;
            var startTime = 5;
            var stopTime = 10;
            
            bodyToEdit.SetValueToPath("programItems[0].name", name);
            bodyToEdit.SetValueToPath("programItems[0].analogSensorElementID", analogSensorElementID);
            bodyToEdit.SetValueToPath("programItems[0].analogSensorType", analogSensorType);
            bodyToEdit.SetValueToPath("programItems[0].condInputToPauseElementID", condInputToPauseElementID);
            bodyToEdit.SetValueToPath("programItems[0].delayTime", delayTime);
            bodyToEdit.SetValueToPath("programItems[0].differentialToStop", differentialToStop);
            bodyToEdit.SetValueToPath("programItems[0].logicConditionNumber1", logicConditionNumber1);
            bodyToEdit.SetValueToPath("programItems[0].logicConditionNumber2", logicConditionNumber2);
            bodyToEdit.SetValueToPath("programItems[0].operTime", operTime);
            bodyToEdit.SetValueToPath("programItems[0].operationMode", operationMode);
            bodyToEdit.SetValueToPath("programItems[0].sequencialOperation", sequencialOperation);
            bodyToEdit.SetValueToPath("programItems[0].startSetPointAbove", startSetPointAbove);
            bodyToEdit.SetValueToPath("programItems[0].startSetPointBelow", startSetPointBelow);
            bodyToEdit.SetValueToPath("programItems[0].startTime.hour", startTime);
            bodyToEdit.SetValueToPath("programItems[0].stopTime.hour", stopTime);

            PostAuxiliaryOutput.RequestPayload = bodyToEdit.ToString();

            PostAuxiliaryOutput.Run();
            Assert.IsTrue(PostAuxiliaryOutput.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(PostAuxiliaryOutput.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");


            var GetAuxiliaryOutput = new AuxilaryOutputGetApi();
            GetAuxiliaryOutput.Run();
            Assert.IsTrue(GetAuxiliaryOutput.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(GetAuxiliaryOutput.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");




            Assert.Multiple(() =>
            {
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetStringValueFromPath("body.units[0].name") == name);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].delayBetweenFilters") == delayBetweenFilters);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].delayForFlushMin") == delayForFlushMin);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].faultReaction") == faultReaction);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].firstFilterNumber") == firstFilterNumber);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].flushingTime") == flushingTime);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].irrigWhileFlushing") == irrigWhileFlushing);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].lastFilterNumber") == lastFilterNumber);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].maxContinousFlushes") == maxContinousFlushes);
                //Assert.IsTrue(FilterFlushGetApi.ResponseBody.GetIntValueFromPath("body.units[0].startSustainBeforeFlush") == startSustainBeforeFlush);
            });
        }
    }
}
