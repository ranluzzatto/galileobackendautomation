﻿using GalileoTestInfrastructure.GalileoApis.WaterPumpElementGroup;
using GalileoTestInfrastructure.GalileoServices;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;


namespace GalileoBackendTests
{
    public class WaterPumpElementGroupTests
    {
        [Test]
        [Category("Get WaterPump")]
        public void GetWaterPump()
        {
            var waterPumpGetApi = new ListGetApi();
            waterPumpGetApi.Run();

            Assert.IsTrue(waterPumpGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(waterPumpGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var waterPumpsItems = waterPumpGetApi.ResponseBody["body"]["items"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(waterPumpsItems, "response should include items");
                Assert.IsNotNull(waterPumpGetApi.ResponseBody.GetStringValueFromPath("body.conditionInputElements"), "response should include conditionInputElements");
                Assert.IsNotNull(waterPumpGetApi.ResponseBody.GetStringValueFromPath("body.pipeLineNumbers"), "response should include pipeLineNumbers");
                Assert.IsTrue(waterPumpsItems.Count() == 20, "there should be a 20 items in water pump");
            });


        }

        [Test]
        [Category("Post WaterPump")]
        public void PostWaterPump() 
        {
            var waterPumpPostApi = new WaterPumpHousePostApi();
            var bodyToEdit = WaterPumpHousePostApi.DefaultPayload.ToJObject();

            var name = "new name";

            bodyToEdit.SetValueToPath("items[0].name", name);
            waterPumpPostApi.RequestPayload = bodyToEdit.ToString();

            waterPumpPostApi.Run();

            Assert.IsTrue(waterPumpPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(waterPumpPostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var waterPumpGetApi = new ListGetApi();
            waterPumpGetApi.Run();

            Assert.IsTrue(waterPumpGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].name", name));
        }

        [Test]
        [Category("RunEvents")]
        public void RunEvents() 
        {
            var waterPumpRunEventsApi = new RunEventsApi();
            var body = @"{""numbers"":[3]}";
            waterPumpRunEventsApi.RequestPayload = body;
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 21,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [                                      
                                        3                                    
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0,
                                      ""GHNumber"": 0
                                    }";
            eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
            waterPumpRunEventsApi.AddRedisEventToValidate(eventToValidate);
            waterPumpRunEventsApi.Run();

            Assert.IsTrue(waterPumpRunEventsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(waterPumpRunEventsApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
            Assert.IsTrue(waterPumpRunEventsApi.RedisValidationResult, "The expected Redis event was not found ");


        }

        [Test]
        [Category("Assign Pipe line Group, Condition input To Water pump item")]
        public void AssignElementsToWaterPump() 
        {
            //   var exportService = new GalileoExportService();
            //   var e = exportService.GetControlerExportAsJObject();

            var importService = new GalileoImportService();
            importService.Import(@"APIs\WaterPumpTestsData\WaterPumpBaseData.txt");

            var waterPumpGetApi = new ListGetApi();
            waterPumpGetApi.Run();

            var pipeLineElementsList = waterPumpGetApi.ResponseBody["body"]["pipeLineNumbers"];
            var conditionInputElementsList = waterPumpGetApi.ResponseBody["body"]["conditionInputElements"];

            //these values where asigned in the export file
            var pipeLineElementNumberToAssign = 1;
            var conditionOperElementNumberToAssign = 1;
            var conditionFaultElementNumberToAssign = 2;

            //start editing the default(empty payload)
            var bodyToEdit = WaterPumpHousePostApi.DefaultPayload.ToJObject();

            bodyToEdit.SetValueToPath("items[0].pipeLineNumber", pipeLineElementNumberToAssign);

            var conElementOper = conditionInputElementsList.FirstOrDefault(s => s.GetIntValueFromPath("number") == conditionOperElementNumberToAssign);
            bodyToEdit.SetValueToPath("items[0].operConditionInputElementID", conElementOper.GetIntValueFromPath("id"));

            var conElementfault = conditionInputElementsList.FirstOrDefault(a => a.GetIntValueFromPath("number") == conditionFaultElementNumberToAssign);
            bodyToEdit.SetValueToPath("items[0].faultConditionInputElementID", conElementfault.GetIntValueFromPath("id"));

            var waterPumpPostApi = new RunEventsApi();
            waterPumpPostApi.RequestPayload = bodyToEdit.ToString();

            waterPumpPostApi.Run();
            waterPumpGetApi.Run();

            Assert.Multiple(() =>
            {
               
                Assert.IsTrue(waterPumpGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].operConditionInputElementID", conElementOper.GetIntValueFromPath("id").ToString()), "oper Condition Input element id mismatch");
                Assert.IsTrue(waterPumpGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].faultConditionInputElementID", conElementfault.GetIntValueFromPath("id").ToString()), "con Element fault id mismatch");
                Assert.IsTrue(waterPumpGetApi.ResponseBody.IsPropertyPathEQValue("body.items[0].pipeLineNumber", pipeLineElementNumberToAssign.ToString()), "pipe line type should be 1 = pipe line element");
            });



        }







    }
}
