﻿using GalileoTestInfrastructure.GalileoApis.IrrigationProgramSetup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class IrrigationProgramSetupTests
    {
        [Test]
        [Category("Irrigation program setup - Get")]
        public void IrrigationProgramSetupGet()
        {
            var irrigationProgramSetupGetApi = new IrrigationProgramSetupGetApi();
            irrigationProgramSetupGetApi.Run();

            Assert.IsTrue(irrigationProgramSetupGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramSetupGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var generalCounterElements = irrigationProgramSetupGetApi.ResponseBody["body"]["generalCounterElements"];
            var sensorElements = irrigationProgramSetupGetApi.ResponseBody["body"]["sensorElements"];
            var sensorGroupNumbers = irrigationProgramSetupGetApi.ResponseBody["body"]["sensorGroupNumbers"];
            var commonSensors = irrigationProgramSetupGetApi.ResponseBody["body"]["commonSensors"];
            var conditionInputElements = irrigationProgramSetupGetApi.ResponseBody["body"]["conditionInputElements"];
            var logicConditionItems = irrigationProgramSetupGetApi.ResponseBody["body"]["logicConditionItems"];
            var priorityGroupItems = irrigationProgramSetupGetApi.ResponseBody["body"]["priorityGroupItems"];
            var pipelineGroupItems = irrigationProgramSetupGetApi.ResponseBody["body"]["pipelineGroupItems"];
            var irrigationMethodItems = irrigationProgramSetupGetApi.ResponseBody["body"]["irrigationMethodItems"];
            var evaporationCounterNumbers = irrigationProgramSetupGetApi.ResponseBody["body"]["evaporationCounterNumbers"];
            var irrigationProgramSetupItems = irrigationProgramSetupGetApi.ResponseBody["body"]["irrigationProgramSetupItems"];
            var irrigationProgramGeneralSetupItems = irrigationProgramSetupGetApi.ResponseBody["body"]["irrigationProgramGeneralSetupItems"];

            Assert.Multiple(() =>
            {
                var res = generalCounterElements.GetChildrenCount() == 0;
                if (generalCounterElements != null || !res)
                {
                    Assert.IsNotNull(generalCounterElements, "generalCounterElements");
                }

                res = sensorElements.GetChildrenCount() == 0;
                if (sensorElements != null || !res)
                {
                    Assert.IsNotNull(sensorElements, "sensorElements");
                }

                res = sensorGroupNumbers.GetChildrenCount() == 0;
                if (sensorGroupNumbers != null || !res)
                {
                    Assert.IsNotNull(sensorGroupNumbers, "sensorGroupNumbers");
                }

                res = commonSensors.GetChildrenCount() == 0;
                if (commonSensors != null || !res)
                {
                    Assert.IsNotNull(commonSensors, "commonSensors");
                }

                res = conditionInputElements.GetChildrenCount() == 0;
                if (conditionInputElements != null || !res)
                {
                    Assert.IsNotNull(conditionInputElements, "conditionInputElements");
                }

                res = logicConditionItems.GetChildrenCount() == 0;
                if (logicConditionItems != null || !res)
                {
                    Assert.IsNotNull(logicConditionItems, "logicConditionItems");
                }

                res = priorityGroupItems.GetChildrenCount() == 0;
                if (priorityGroupItems != null || !res)
                {
                    Assert.IsNotNull(priorityGroupItems, "priorityGroupItems");
                }

                res = pipelineGroupItems.GetChildrenCount() == 0;
                if (pipelineGroupItems != null || !res)
                {
                    Assert.IsNotNull(pipelineGroupItems, "pipelineGroupItems");
                }

                res = irrigationMethodItems.GetChildrenCount() == 0;
                if (irrigationMethodItems != null || !res)
                {
                    Assert.IsNotNull(irrigationMethodItems, "irrigationMethodItems");
                }

                res = evaporationCounterNumbers.GetChildrenCount() == 0;
                if (evaporationCounterNumbers != null || !res)
                {
                    Assert.IsNotNull(evaporationCounterNumbers, "evaporationCounterNumbers");
                }

                res = irrigationProgramSetupItems.GetChildrenCount() == 0;
                if (irrigationProgramSetupItems != null || !res)
                {
                    Assert.IsNotNull(irrigationProgramSetupItems, "irrigationProgramSetupItems");
                }

                res = irrigationProgramGeneralSetupItems.GetChildrenCount() == 0;
                if (irrigationProgramGeneralSetupItems != null || !res)
                {
                    Assert.IsNotNull(irrigationProgramGeneralSetupItems, "irrigationProgramGeneralSetupItems");
                }
            });
        }

        [Test]
        [Category("irrigation program setup - Post")]
        public void irrigationProgramSetupPost()
        {
            var irrigationProgramSetupPostApi = new IrrigationProgramSetupPostApi();
            var changedPayload = irrigationProgramSetupPostApi.DefaulyPayload.ToJObject();

            var waterUnderFlowDifferData = 10;
            var lineFillDelayMinData = 1;
            foreach (var payload in changedPayload["irrigationProgramSetupItems"])
            {
                payload["waterUnderFlowDiffer"] = waterUnderFlowDifferData;
                payload["lineFillDelayMin"] = lineFillDelayMinData;
            }

            var priorityGroupNumberData = 1;
            foreach (var payload in changedPayload["irrigationProgramGeneralSetupItems"])
            {
                payload["priorityGroupNumber"] = priorityGroupNumberData;
            }

            irrigationProgramSetupPostApi.RequestPayload = changedPayload.ToString();
            irrigationProgramSetupPostApi.Run();

            Assert.IsTrue(irrigationProgramSetupPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramSetupPostApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            // Get check post method
            var irrigationProgramSetupGetApi = new IrrigationProgramSetupGetApi();
            irrigationProgramSetupGetApi.Run();

            Assert.IsTrue(irrigationProgramSetupGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(irrigationProgramSetupGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var irrigationProgramSetupItems = irrigationProgramSetupGetApi.ResponseBody["body"]["irrigationProgramSetupItems"];
            var irrigationProgramGeneralSetupItems = irrigationProgramSetupGetApi.ResponseBody["body"]["irrigationProgramGeneralSetupItems"];

            Assert.Multiple(() =>
            {
                for(var i=0; i<3; i++)
                {
                    Assert.AreEqual(waterUnderFlowDifferData, irrigationProgramSetupItems[i].GetIntValueFromPath("waterUnderFlowDiffer"), "waterUnderFlowDiffer");
                    Assert.AreEqual(lineFillDelayMinData, irrigationProgramSetupItems[i].GetIntValueFromPath("lineFillDelayMin"), "lineFillDelayMin");
                    Assert.AreEqual(priorityGroupNumberData, irrigationProgramGeneralSetupItems[i].GetIntValueFromPath("priorityGroupNumber"), "irrigationProgramGeneralSetupItems");
                }
            });
        }

        [Test]
        [Category("List - Get")]
        public void ListGet()
        {
            var listGetApi = new ListGetApi();
            listGetApi.Run();

            Assert.IsTrue(listGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(listGetApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var body = listGetApi.ResponseBody["body"];

            Assert.Multiple(() =>
            {
                foreach(var program in body)
                {
                    Assert.IsNotNull(program.GetStringValueFromPath("id"), "ID");
                    Assert.IsNotNull(program.GetStringValueFromPath("number"), "Number");
                    Assert.IsNotNull(program.GetStringValueFromPath("name"), "Name");
                }
            });
        }

        [Test]
        [Category("Socket field - Put")]
        public void SocketFieldPut()
        {
            var socketFieldPutApi = new SocketFieldPutApi();
            socketFieldPutApi.RequestPayload = socketFieldPutApi.DefaulyPayload;
            socketFieldPutApi.Run();

            Assert.IsTrue(socketFieldPutApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(socketFieldPutApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            Assert.IsTrue(socketFieldPutApi.ResponseBody.GetBoolValueFromPath("body"), "body");
        }
    }
}
