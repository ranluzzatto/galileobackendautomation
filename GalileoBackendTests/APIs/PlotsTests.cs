﻿using GalileoTestInfrastructure.GalileoApis.PlotElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class PlotElementGroup
    {
        [Test]
        [Category("Get Plot")]
        public void GetPlot()
        {
            var plotGetApi = new PlotGetApi();
            plotGetApi.Run();

            Assert.IsTrue(plotGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(plotGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var plotBody = plotGetApi.ResponseBody["body"];

            Assert.Multiple(() =>
            {
               Assert.IsTrue(plotBody.Count() == 50, "there should be a 50 items in plot");

            });
        }

    
    }




}
