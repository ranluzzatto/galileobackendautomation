﻿using GalileoTestInfrastructure.GalileoApis.BaseSettings;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class BaseSettingsTest
    {
        [Test]
        [Category("Get BaseSettings")]

        public void GetBaseSettings()
        {
            var BaseSettingsGetApi = new BaseSettingsGetApi();

            BaseSettingsGetApi.Run();
            Assert.IsTrue(BaseSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(BaseSettingsGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            Assert.Multiple(() =>
            {
             
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetIntValueFromPath("body.item.baudRate"), "response should include baudRate");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetIntValueFromPath("body.item.controllerID"), "response should include controllerID");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetIntValueFromPath("body.item.keepAlive"), "response should include keepAlive");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetBoolValueFromPath("body.item.modemActive"), "response should include modemActive");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetStringValueFromPath("body.item.modemApn"), "response should include modemApn");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetIntValueFromPath("body.item.modemDelayConnect"), "response should include modemDelayConnect");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetStringValueFromPath("body.item.modemPassword"), "response should include modemPassword");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetStringValueFromPath("body.item.modemUser"), "response should include modemUser");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetIntValueFromPath("body.item.port"), "response should include port");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetBoolValueFromPath("body.item.serialActive"), "response should include serialActive");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetStringValueFromPath("body.item.serverAddress"), "response should include serverAddress");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetBoolValueFromPath("body.item.tcpActive"), "response should include tcpActive");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetBoolValueFromPath("body.item.wifiActive"), "response should include wifiActive");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetStringValueFromPath("body.item.wifiNetwork"), "response should include wifiNetwork");
                Assert.IsNotNull(BaseSettingsGetApi.ResponseBody.GetStringValueFromPath("body.item.wifiPassword"), "response should include wifiPassword");

            });

        
        
        }


        [Test]
        [Category("Post BaseSettings")]
        public void PostBaseSettings()
        {
            var BaseSettingsPostApi = new BaseSettingsPostApi();

            BaseSettingsPostApi.RequestPayload = BaseSettingsPostApi.DefaultPayload;
            BaseSettingsPostApi.Run();

            //us default "empty" body
            var bodyToEdit = BaseSettingsPostApi.DefaultPayload.ToJObject();

            //Edit an item
            var baudRate = 3;
            var controllerID = 3;
            var wifiPassword = "blabla";


            bodyToEdit.SetValueToPath("item.baudRate", baudRate);
            bodyToEdit.SetValueToPath("item.controllerID", controllerID);
            bodyToEdit.SetValueToPath("item.wifiPassword", wifiPassword);

            BaseSettingsPostApi.RequestPayload = bodyToEdit.ToString();

            

            var expectedEvent = @"{
                                     ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 7,
                                      ""CommandCode"": 0,
                                      ""ComponentTypeCode"": 0,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": null,
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0
                                    }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));



            BaseSettingsPostApi.AddRedisEventToValidate(expectedEvent);
            BaseSettingsPostApi.Run();

            Assert.IsTrue(BaseSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(BaseSettingsPostApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
           Assert.IsTrue(BaseSettingsPostApi.RedisValidationResult,"The expected Redis event was not found ");

            //get the edited item and validate it
            var BaseSettingsGetApi = new BaseSettingsGetApi();

            BaseSettingsGetApi.Run();

            Assert.Multiple(() =>
            {
                Assert.IsTrue(BaseSettingsGetApi.ResponseBody.IsPropertyPathEQValue("body.item.baudRate", baudRate.ToString()));
                Assert.IsTrue(BaseSettingsGetApi.ResponseBody.IsPropertyPathEQValue("body.item.controllerID", controllerID.ToString()));
                Assert.IsTrue(BaseSettingsGetApi.ResponseBody.IsPropertyPathEQValue("body.item.wifiPassword", wifiPassword));

            });
        }



    }




}


