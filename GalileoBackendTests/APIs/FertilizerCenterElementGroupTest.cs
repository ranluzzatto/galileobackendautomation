﻿using GalileoTestInfrastructure.GalileoApis.FertilizerCenterElementGroup;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Linq;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class FertilizerCenterElementGroupTest
    {
        [Test]
        [Category("Get FertilizerCenterElementGroup")]
        public void FertilizerCenterElementGroup()
        {
            var FertilizerCenterGetApi = new FertilizerCenterElementGroupGetApi();

            FertilizerCenterGetApi.Run();

            Assert.IsTrue(FertilizerCenterGetApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(FertilizerCenterGetApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");

            var FertilizerCenterElementGroupItems = FertilizerCenterGetApi.ResponseBody["body"]["fertilizerCenterItems"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(FertilizerCenterElementGroupItems, "response should include items");
                Assert.IsNotNull(FertilizerCenterGetApi.ResponseBody.GetStringValueFromPath("body.pipeLineList"), "response should include pipeLineList");
                Assert.IsNotNull(FertilizerCenterGetApi.ResponseBody.GetStringValueFromPath("body.sensorGroupNumbers"), "response should include sensorGroupNumbers");
                Assert.IsNotNull(FertilizerCenterGetApi.ResponseBody.GetStringValueFromPath("body.waterMeterGroupNumbers"), "response should include waterMeterGroupNumbers");
                Assert.IsTrue(FertilizerCenterElementGroupItems.Count() == 8, "there should be a 8 items in fertilizer center");
            });
        }






    }






}
