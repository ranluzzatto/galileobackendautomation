﻿using GalileoTestInfrastructure.GalileoApis.Alarm;
using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Net;

namespace GalileoBackendTests.APIs
{
    public class AlarmTests
    {
        [Test]
        [Category("Get Alarms")]
        public void GetAlarms()
        {
            var alarmsApi = new AlarmsGetApi();
            alarmsApi.Run();

            Assert.IsTrue(alarmsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(alarmsApi.ResponseBody.GetBoolValueFromPath("result"), "Result in the body must be true");

            var alarmItems = alarmsApi.ResponseBody["body"]["alarmItems"];
            var timeRanges = alarmsApi.ResponseBody["body"]["timeRanges"];
            var setup = alarmsApi.ResponseBody["body"]["setup"];
            var programs = alarmsApi.ResponseBody["body"]["programs"];
            var alarmsToSendItems = alarmsApi.ResponseBody["body"]["alarmsToSendItems"];

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(alarmItems, "alarmItems returned cannot be null");
                Assert.IsNotNull(timeRanges, "timeRanges returned cannot be null");
                Assert.IsNotNull(setup, "setup returned cannot be null");
                Assert.IsNotNull(programs, "programs returned cannot be null");
                Assert.IsNotNull(alarmsToSendItems, "alarmsToSendItems returned cannot be null");
                Assert.IsTrue(programs.GetChildrenCount() == 50, "Number of alarm types should be 50");
            });
        }

        //[Test] Impossible to test due to an online controller requirement!
        //[Category("Run Events")]
        //public void RunEvents()
        //{
        //    var AlarmsRunEventsApi = new AlarmsRunEventsApi();
        //    var body = @"{""numbers"":[1]}"; // request to run events for item# 3

        //    AlarmsRunEventsApi.RequestPayload = body;
        //    var eventToValidate = @"{
        //                              ""SN"": ""SerialNumberPlaceHolder"",
        //                              ""Type"": 1,
        //                              ""CommandCode"": 56,
        //                              ""ComponentTypeCode"": 1,
        //                              ""CNFCode"": 0,
        //                              ""Number"": 0,
        //                              ""Numbers"": [
        //                                1
        //                              ],
        //                              ""StatusCode"": 0,
        //                              ""RealTimeValue"": null,
        //                              ""FirmwareId"": 0,
        //                              ""LogType"": 0,
        //                              ""GHNumber"": 0,
        //                              ""InstanceNumber"": 0
        //                            }";
        //    eventToValidate = eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
        //    AlarmsRunEventsApi.AddRedisEventToValidate(eventToValidate);

        //    AlarmsRunEventsApi.Run();

        //    Assert.IsTrue(AlarmsRunEventsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
        //    Assert.IsTrue(AlarmsRunEventsApi.ResponseBody.IsPropertyPathEQValue("result", "true"), "result in response body should be true");
        //    Assert.IsTrue(AlarmsRunEventsApi.RedisValidationResult, "The expected Redis event was not found ");




        //}

        //public void RunEvents()
        //{
        //    var alarmsRunEventsApi = new AlarmsRunEventsApi();
        //    var body = @"{""numbers"":[1]}"; // request to run events for item# 3
        //    alarmsRunEventsApi.RequestPayload = body;
        //    var eventToValidate = @"{
        //                          ""SN"": ""SerialNumberPlaceHolder"",                               
        //                           ""Type"": 1,
        //                          ""CommandCode"": 56,
        //                          ""ComponentTypeCode"": 1,
        //                          ""CNFCode"": 0,
        //                          ""Number"": 0,
        //                          ""Numbers"": [
        //                            1
        //                          ],
        //                          ""StatusCode"": 0,
        //                          ""RealTimeValue"": null,
        //                          ""FirmwareId"": 0,
        //                          ""LogType"": 0,
        //                          ""GHNumber"": 0,
        //                          ""InstanceNumber"": 0
        //                            }";
        //    eventToValidate.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
        //    alarmsRunEventsApi.AddRedisEventToValidate(eventToValidate);

        //    alarmsRunEventsApi.Run();

        //    Assert.IsTrue(alarmsRunEventsApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
        //    Assert.IsTrue(bool.Parse(alarmsRunEventsApi.ResponseBody.GetStringValueFromPath("result")), "Result in the body must be true");
        //    Assert.IsTrue(alarmsRunEventsApi.RedisValidationResult, "Expected redis message was not found");
        //}

        [Test]
        [Category("Socket Field - Cancel Alarm")]
        public void SocketField_CancelAlarm()
        {
            var alarmsSocketFieldApi = new AlarmsSocketFieldApi();

            var eventToValidate = @"{
                                  ""SN"": ""SerialNumberPlaceHolder"",
                                  ""Type"": 4,
                                  ""CommandCode"": 56,
                                  ""ComponentTypeCode"": 0,
                                  ""CNFCode"": 0,
                                  ""Number"": 0,
                                  ""Numbers"": null,
                                  ""StatusCode"": 0,
                                  ""RealTimeValue"": {
                                    ""Numbers"": [
                                      1
                                    ],
                                    ""ParameterNumber"": 2,
                                    ""Value"": 1
                                  },
                                  ""FirmwareId"": 0,
                                  ""LogType"": 0
                                }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
            alarmsSocketFieldApi.AddRedisEventToValidate(eventToValidate);

            alarmsSocketFieldApi.RequestPayload = @"{ ""type"":5,""value"":true,""fieldName"":""CancelAlarm"",""numbers"":[1]}";
            alarmsSocketFieldApi.Run();

            Assert.IsTrue(alarmsSocketFieldApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(bool.Parse(alarmsSocketFieldApi.ResponseBody.GetStringValueFromPath("result")), "Result in the body must be true");
            Assert.IsTrue(alarmsSocketFieldApi.RedisValidationResult, "expected redis mseeage was not found");
        }

        [Test]
        [Category("Time Ranges")]
        public void TimeRanges()
        {
            var timeRangesApi = new AlarmsTimeRangesApi();

            Random rnd = new Random();
            var timeToChange = rnd.Next(1, 24);  // set from time for time range number 1 to be between 100 - 2300

            timeRangesApi.RequestPayload = @"{ ""timeRanges"":[{ ""number"":1,""from"":FromTimePlaceHolder,""to"":559,""isDayAndNight"":false},
                                           { ""number"":2,""from"":600,""to"":559,""isDayAndNight"":true},
                                           { ""number"":3,""from"":0,""to"":559,""isDayAndNight"":false}]}".Replace("FromTimePlaceHolder", timeToChange.ToString() + "00");

            //Todo: in case the timeToChange is the same as in the database the read cnf event will not be generated,
            //need to fix
            timeRangesApi.ValidateReadCnfRedisEvent = true;

            timeRangesApi.Run();

            Assert.IsTrue(timeRangesApi.ReadCnfRedisEventValidationResult);
            Assert.IsTrue(timeRangesApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");
            Assert.IsTrue(bool.Parse(timeRangesApi.ResponseBody.GetStringValueFromPath("result")), "Result in the body must be true");

            var alarmsApi = new AlarmsGetApi();
            alarmsApi.Run();

            var actualTimeRanges = alarmsApi.ResponseBody.GetStringValueFromPath("body.timeRanges[0].from");
            var timeToValidate = timeToChange + "00";

            Assert.IsTrue(actualTimeRanges == timeToValidate, string.Format("Expected 'from' time is:{0} but was:{1}", timeToValidate, actualTimeRanges));
        }

        [Test]
        [Category("Alarms To Send")]
        public void AlarmsToSend()
        {
            var alarmsToSendApi = new AlarmsAlarmsToSendApi();
            //start with setting data to default - all is false
            alarmsToSendApi.RequestPayload = AlarmsAlarmsToSendApi.DefaultPayload;
            alarmsToSendApi.Run();

            //make sure all data is cleared (false)
            var alarmsGetApi = new AlarmsGetApi();
            alarmsGetApi.Run();

            var actualAlarmsToSendclearedItems = alarmsGetApi.ResponseBody["body"]["alarmsToSendItems"] as JArray;

            Assert.Multiple(() =>
            {
                foreach (var actualAlarmToSend in actualAlarmsToSendclearedItems)
                {
                    Assert.IsFalse(actualAlarmToSend.GetBoolValueFromPath("sms"));
                    Assert.IsFalse(actualAlarmToSend.GetBoolValueFromPath("email"));
                    Assert.IsFalse(actualAlarmToSend.GetBoolValueFromPath("push"));
                }
            });

            //change 2 items in alarmsToSendItems
            var requestPayload = AlarmsAlarmsToSendApi.DefaultPayload.ToJObject();
            var alarmsToSendItems = requestPayload["alarmsToSendItems"] as JArray;

            foreach (var alarmToSend in alarmsToSendItems)
            {
                alarmToSend["sms"] = true;
                alarmToSend["email"] = true;
                alarmToSend["push"] = true;
            }

            alarmsToSendApi.RequestPayload = requestPayload.ToString();

            alarmsToSendApi.Run();

            //get alarmsToSendItems after change and validate it
            alarmsGetApi.Run();

            var actualAlarmsToSendItems = alarmsGetApi.ResponseBody["body"]["alarmsToSendItems"] as JArray;

            Assert.Multiple(() =>
            {
                foreach (var actualAlarmToSend in actualAlarmsToSendItems)
                {
                    Assert.IsTrue(actualAlarmToSend.GetBoolValueFromPath("sms"),"sms");
                    Assert.IsTrue(actualAlarmToSend.GetBoolValueFromPath("email"),"email");
                    Assert.IsTrue(actualAlarmToSend.GetBoolValueFromPath("push"),"push");
                }
            });
        }

        [Test]
        [Category("Put Alarms")]
        public void PutAlarms()
        {
            var alarmsPutApi = new AlarmsPutApi();
            var alarmsGetApi = new AlarmsGetApi();

            //set default alarm data 
            alarmsPutApi.RequestPayload = AlarmsPutApi.DefaultPayload;
            alarmsPutApi.Run();

            //validate default data was set
            alarmsGetApi.Run();
            var alarmPrograms = alarmsGetApi.ResponseBody["body"]["programs"];

            Assert.Multiple(() =>
            {
                foreach (var alarmProgram in alarmPrograms)
                {
                    Assert.IsTrue(alarmProgram.GetIntValueFromPath("elementNumber") == 1);
                    Assert.IsTrue(alarmProgram.GetIntValueFromPath("timeRangeNumber") == 1);
                }
            });

            //prepare payload data to send
            var payloadToSend = AlarmsPutApi.DefaultPayload.ToJObject();
            var programsToSend = payloadToSend["programs"];
            var setupToSend = payloadToSend["setup"];

            foreach (var program in programsToSend)
            {
                program["elementNumber"] = 2;//element number can be 1-8
                program["timeRangeNumber"] = 3;//time range number can be 1-3
            }

            setupToSend["alarmCancelCycle"] = 30;
            setupToSend["fertNotFinishedPercent"] = 90;

            alarmsPutApi.RequestPayload = payloadToSend.ToString();

            //validate the read cnf event was created correctlly
            //alarmsPutApi.ValidateReadCnfRedisEvent = true;

            alarmsPutApi.Run();

           // Assert.IsTrue(alarmsPutApi.ReadCnfRedisEventValidationResult, "read cnf event should be created");

            alarmsGetApi.Run();

            var aclualAlarmPrograms = alarmsGetApi.ResponseBody["body"]["programs"];
            var actualAlarmsSetup = alarmsGetApi.ResponseBody["body"]["setup"];

            foreach (var alarmProgram in aclualAlarmPrograms)
            {
                Assert.IsTrue(alarmProgram.GetIntValueFromPath("elementNumber") == 2);
                Assert.IsTrue(alarmProgram.GetIntValueFromPath("timeRangeNumber") == 3);
            }

            Assert.Multiple(() =>
            {
                Assert.IsTrue(actualAlarmsSetup.GetIntValueFromPath("alarmCancelCycle") == 30);
                Assert.IsTrue(actualAlarmsSetup.GetIntValueFromPath("fertNotFinishedPercent") == 90);
            });
        }
    }
}
