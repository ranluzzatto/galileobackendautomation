﻿using GalileoTestInfrastructure.GalileoApis.ImportExportEntities;
using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Threading.Tasks;

namespace GalileoTestInfrastructure.GalileoServices
{
    public class GalileoExportService
    {

        public string GetControlerExportAsString()
        {
            return GetControlerExportAsJObject().ToString();
        }
        public JObject GetControlerExportAsJObject()
        {
            var exportApi = new ExportControllerApi();
            exportApi.Run();

            var responseStream = exportApi.Response.Content.ReadAsStream();
            StreamReader streamReader = new StreamReader(responseStream);

            return streamReader.ReadToEnd().ToJObject();
        }
        //get the export data async
        public async Task<string> GetControlerExportAsJObjectAsync()
        {
            var exportApi = new ExportControllerApi();

            var apTask = Task.Run(() => exportApi.Run());
            await apTask;

            var responseStream = exportApi.Response.Content.ReadAsStreamAsync();
            StreamReader streamReader = new StreamReader(await responseStream);

            return await streamReader.ReadToEndAsync();
        }
    }
}
