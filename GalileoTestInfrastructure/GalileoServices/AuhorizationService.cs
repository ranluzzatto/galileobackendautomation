﻿using GalileoTestInfrastructure.Infrastructure;
using HttpCientInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;

namespace GalileoTestInfrastructure.Services
{
    class AuhorizationService
    {
        private AuhorizationService() { }

        private static AuhorizationService instance = null;
        public static AuhorizationService Instance
        {
            get
            {
                if (instance == null)
                    instance = new AuhorizationService();
                return instance;
            }
        }
        public string GetUserTokenFromAppConfigCredentials()
        {
            var httpService = new HttpClientServices();
            var baseUri = ConfigurationManagerWrapper.GetParameterValue("BaseUri");

            string body = @" {""UserName"":""UserPlaceHolder"",""Password"": ""PasswordPlaceHolder""}"
                         .Replace("UserPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("GalileoUser"))
                         .Replace("PasswordPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("GalileoPassword"));

            var response = httpService.RunPostRequest(baseUri + "/auth/login", body);
            var responseBody = JObject.Parse(response.Result);

            return responseBody["body"]["accountToken"].ToString();
        }
        public string GetUserToken(string email, string password)
        {
            var httpService = new HttpClientServices();
            var baseUri = ConfigurationManagerWrapper.GetParameterValue("BaseUri");

            string body = @" {""UserName"":""UserPlaceHolder"",""Password"": ""PasswordPlaceHolder"" ,""isMobile"": true}"
                         .Replace("UserPlaceHolder", email)
                         .Replace("PasswordPlaceHolder", password);

            var response = httpService.RunPostRequest(baseUri + "/auth/login", body);
            var responseBody = JObject.Parse(response.Result);

            if (!responseBody.GetBoolValueFromPath("result"))
                return "Login Failed";

            var token = responseBody["body"]["accountToken"].ToString();

            if (string.IsNullOrEmpty(token))
            {
                var tempSession = responseBody["body"]["tempSessionId"].ToString();
                return GetTokenFromSession(tempSession, email, password);
            }
            return token;
        }

        private string GetTokenFromSession(string tempSessionId, string email, string password)
        {
            var body = @"{
                          ""userName"": ""currUserName"",
                          ""password"": ""currPassword"",
                          ""tempSession"": ""sessionId"",
                          ""isMobile"": true
                        }".Replace("sessionId", tempSessionId)
                            .Replace("currUserName", email)
                            .Replace("currPassword", password);

            var httpService = new HttpClientServices();

            var baseUri = ConfigurationManagerWrapper.GetParameterValue("BaseUri");

            var response = httpService.RunPostRequest(baseUri + "/auth/session", body);
            var responseBody = JObject.Parse(response.Result);

            return responseBody["body"]["accountToken"].ToString();
        }
    }
}
