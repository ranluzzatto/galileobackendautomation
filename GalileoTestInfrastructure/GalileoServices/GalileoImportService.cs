﻿using GalileoTestInfrastructure.GalileoApis.ImportExportEntities;

namespace GalileoTestInfrastructure.GalileoServices
{
    public class GalileoImportService
    {
        public void Import(string fileName)
        {
            var importControllerApi = new ImportControllerApi();
            importControllerApi.FileNameToPost = fileName;
            importControllerApi.Run();

        }
    }
}
