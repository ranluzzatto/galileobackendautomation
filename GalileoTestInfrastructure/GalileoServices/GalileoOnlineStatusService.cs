﻿using GalileoTestInfrastructure.GalileoApis.Project;
using GalileoTestInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoServices
{
    public class GalileoOnlineStatusService
    {
        public bool IsControllerConnected()
        {
            var projectConfigOnlineStatusApi = new ProjectConfigOnlineStatusApi();

            projectConfigOnlineStatusApi.Run();

            var responseBody = projectConfigOnlineStatusApi.ResponseBody;

            return responseBody.GetBoolValueFromPath("body.connectionStatus");

        }
    }
}
