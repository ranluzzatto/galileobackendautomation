﻿using GalileoTestInfrastructure.GalileoApis.Component;
using GalileoTestInfrastructure.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace GalileoTestInfrastructure.GalileoServices
{
    public class GalileoComponentsService
    {
        public List<GalileoComponent> GetComponents(SetupElementType componentType)
        {
            var returnedComponents = new List<GalileoComponent>();

            var getComponentsApi = new ComponentsGetApi(new Dictionary<string, string>
            {
                {
                    "elementTypeId", ((int)componentType).ToString()
                }
            });

            getComponentsApi.Run();

            if (getComponentsApi.ResponseStatusCode != System.Net.HttpStatusCode.OK)
                return null;

            var elements = getComponentsApi.ResponseBody["body"]["tabs"][0]["tables"][0]["elements"];

            foreach (var element in elements)
            {
                var galileoComponent = new GalileoComponent();
                galileoComponent.ComponentType = (SetupElementType)getComponentsApi.ResponseBody.GetIntValueFromPath("body.elementTypeId");

                galileoComponent.Name = element["name"].ToString();
                galileoComponent.Number = (int)element["number"];

                var fields = element["fields"];

                foreach (var field in fields.Where(f => !f.GetBoolValueFromPath("isSocket")))
                {
                    galileoComponent.Properties.Add(field["title"].ToString(), field["value"].ToString());
                }

                returnedComponents.Add(galileoComponent);   
            }

            return returnedComponents;
        }
    }
}
