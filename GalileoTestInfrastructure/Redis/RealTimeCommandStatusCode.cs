﻿namespace GalileoTestInfrastructure.Redis
{
    public enum RealTimeCommandStatusCode : int
    {
        Login = 0,
        Logout = 1,
        Other = 0xFFFF
    }
}
