﻿namespace GalileoTestInfrastructure.Redis
{
    public enum RealTimeCommandType : int
    {
        StatusCommand = 0,
        ComponentCommand = 1,
        ReadCNF = 2,
        ReadClock = 3,
        RealTimeValue = 4,
        Firmware = 5,
        ResetLogs = 6
    }
}
