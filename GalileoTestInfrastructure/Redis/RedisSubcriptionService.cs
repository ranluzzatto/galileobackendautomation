﻿using GalileoTestInfrastructure.Infrastructure;
using StackExchange.Redis;
using System;

namespace GalileoTestInfrastructure.Redis
{
    public class RedisSubcriptionService
    {
        #region Singelton
        private static RedisSubcriptionService instance = null;
        public static RedisSubcriptionService Instance
        {
            get
            {
                if (instance == null)
                    instance = new RedisSubcriptionService();
                return instance;
            }
        }
        #endregion

        private ConnectionMultiplexer _muxer;
        private ISubscriber _subscriber;
        private readonly string _redisChannel = ConfigurationManagerWrapper.GetParameterValue("SocketRedisChannel");

        private RedisSubcriptionService() => Connect();

        public bool IsConnected() => _muxer != null;
        public void RegisterMethod(Action<RedisChannel, RedisValue> callback)
        {
            _subscriber.Subscribe(_redisChannel, callback);
        }
        public void UnRegisterMethod(Action<RedisChannel, RedisValue> callback)
        {
            _subscriber.Unsubscribe(_redisChannel, callback);
        }
        private bool Connect()
        {
            var redisServer = ConfigurationManagerWrapper.GetParameterValue("SocketHost");
            var redisServerPort = ConfigurationManagerWrapper.GetParameterValue("SocketPort");
            var connetionString = ConfigurationOptions.Parse($"{redisServer}:{redisServerPort}");

            try
            {
                _muxer = ConnectionMultiplexer.Connect(connetionString);
            }
            catch
            {
                throw new Exception("Connection to redis server failed");
            }
            _subscriber = _muxer.GetSubscriber();

            return _muxer != null;
        }
        public void Disconnect()
        {
            if (_muxer != null)
            {
                _muxer.Close();
                _muxer.Dispose();
                _muxer = null;
            }
            if (_subscriber != null)
            {
                _subscriber.UnsubscribeAll();
                _subscriber = null;
            }
        }
    }
}
