﻿using GalileoTestInfrastructure.Infrastructure;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.Redis
{
    public class RealTimeCommand
    {
        public string SN { get; set; }
        public RealTimeCommandType Type { get; set; }
        public RealTimeCommandCode CommandCode { get; set; }
        public RealTimeCommandTypeCode ComponentTypeCode { get; set; }
        public RealTimeCommandCNFCode CNFCode { get; set; }
        public int Number { get; set; }
        public IEnumerable<int> Numbers { get; set; }
        public RealTimeCommandStatusCode StatusCode { get; set; }
        public RealTimeCommandValue RealTimeValue { get; set; }
        public long FirmwareId { get; set; }
        public byte LogType { get; set; }

        public static RealTimeCommand FormJson(string json)
        {
            return JsonConvert.DeserializeObject<RealTimeCommand>(json);
        }
        public static string ToJson(RealTimeCommand obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }
        public override bool Equals(object obj)
        {
            var other = obj as RealTimeCommand;

            return SN == other.SN                                         &&
                Type == other.Type                                        &&
                CommandCode == other.CommandCode                          &&
                ComponentTypeCode == other.ComponentTypeCode              &&
                CNFCode == other.CNFCode                                  &&
                Number == other.Number                                    &&
                Numbers.NullRespectingSequenceEqual(other.Numbers)        &&
                StatusCode == other.StatusCode                            &&
                RealTimeValue == other.RealTimeValue                      &&
                FirmwareId == other.FirmwareId                            &&
                LogType == other.LogType;
        }
        public override int GetHashCode() => (SN, Type, CommandCode, ComponentTypeCode, CNFCode, 
            Number, Numbers, StatusCode, RealTimeValue, FirmwareId, LogType)
            .GetHashCode();

        public static bool operator == (RealTimeCommand lhs, RealTimeCommand rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }
        public static bool operator != (RealTimeCommand lhs, RealTimeCommand rhs) => !(lhs == rhs);
    }
}
