﻿using GalileoTestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.Redis
{
    public class RealTimeCommandValue
    {
        public List<int> Numbers { get; set; }
        public int ParameterNumber { get; set; }
        public long Value { get; set; }
        public override bool Equals(object obj)
        {
            var other = obj as RealTimeCommandValue;

            return ParameterNumber == other.ParameterNumber &&
                   Value == other.Value &&
                   Numbers.NullRespectingSequenceEqual(other.Numbers);
        }
        public override int GetHashCode() => (Numbers, ParameterNumber, Value).GetHashCode();
        public static bool operator ==(RealTimeCommandValue lhs, RealTimeCommandValue rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }
        public static bool operator !=(RealTimeCommandValue lhs, RealTimeCommandValue rhs) => !(lhs == rhs);
    }
}
