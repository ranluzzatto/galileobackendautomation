﻿namespace GalileoTestInfrastructure.Redis
{
    public enum RealTimeCommandCNFCode : int
    {
        Partial = 0,
        All = 1,
        Other = 0xFFFF
    }
}
