﻿namespace GalileoTestInfrastructure.Redis
{
    public enum RealTimeCommandTypeCode : int
    {
        Add = 1,
        Delete = 2,
        Other = 0xFFFF
    }
}
