﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PipeLineCenter
{
    public class PipeLinesGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/pipelines";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
