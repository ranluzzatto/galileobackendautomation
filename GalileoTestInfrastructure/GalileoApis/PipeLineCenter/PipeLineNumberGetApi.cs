﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PipeLineCenter
{
    public class PipeLineNumberGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/pipelines/{pipeLineNumber}";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
