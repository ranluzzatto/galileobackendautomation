﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PipeLineCenter
{
    public class AllPipeLineElementsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/all-pipeline-elements";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
