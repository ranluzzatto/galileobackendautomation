﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ManualOperation
{
    public class ManualOperationGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/manual-operation";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
