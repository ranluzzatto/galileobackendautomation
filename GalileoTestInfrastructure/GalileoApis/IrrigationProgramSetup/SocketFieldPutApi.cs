﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramSetup
{
    public class SocketFieldPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-setup/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaulyPayload = @"{""type"":3,""value"":1,""fieldName"":""TodayCancelledFert"",""numbers"":[1]}";
    }
}
