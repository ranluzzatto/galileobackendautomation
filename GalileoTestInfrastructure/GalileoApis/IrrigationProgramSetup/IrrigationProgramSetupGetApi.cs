﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramSetup
{
    public class IrrigationProgramSetupGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-setup";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
