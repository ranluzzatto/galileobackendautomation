﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramSetup
{
    public class ListGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-setup/list";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
