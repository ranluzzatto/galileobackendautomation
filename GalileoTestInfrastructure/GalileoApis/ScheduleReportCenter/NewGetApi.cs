﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class NewGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/schedule";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
