﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class ReportIdDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/schedule/{reportId}";
        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
