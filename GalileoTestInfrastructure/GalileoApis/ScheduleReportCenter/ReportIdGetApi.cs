﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class ReportIdGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/schedule/{reportId}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
