﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class SchedulePutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/schedule";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public static string DefaultPayload = @"{""reportId"":0,""name"":"""",""userId"":0,""userName"":"""",""timePeriodView"":{""type"":""Daily"",""time"":{""hour"":0,""minute"":0},""dayNumber"":0},""emailView"":{""email1"":"""",""email2"":"""",""email3"":""""},""mainFilter"":""Valve"",""groupFilter"":""Valve"",""waterDiff"":0,""selectedFilters"":[{}],""columns"":[""Area""]}";
    }
}
