﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Auth
{
    public class AuthTokenRefreshApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "auth/token/refresh";
    }
}
