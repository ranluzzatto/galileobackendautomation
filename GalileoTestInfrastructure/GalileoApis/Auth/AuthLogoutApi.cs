﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Auth
{
    public class AuthLogoutApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/auth/logout";
    }
}
