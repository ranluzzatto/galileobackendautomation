﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Auth
{
    public class AuthUserExistsApi : GalileoApiBase
    {
        public AuthUserExistsApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams,null) { }
        internal override HttpRequestType RequestType => HttpRequestType.GET;
        protected override string ApiRoutePart => "user/exists";
    }
}
