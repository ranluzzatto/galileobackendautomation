﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PipeLineElementGroup
{
    public class PipeLineRunEventsApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/config/{configId}/element-group/pipe-line/run-events";




    }
}
