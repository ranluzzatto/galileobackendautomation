﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PipeLineElementGroup
{
    public class PipeLineSocketFiledApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/pipe-line/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;

    }
}
