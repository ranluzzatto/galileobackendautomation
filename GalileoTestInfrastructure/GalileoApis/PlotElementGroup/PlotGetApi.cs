﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PlotElementGroup
{
    public class PlotGetApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.GET;
        protected override string ApiRoutePart => "/config/{configId}/element-group/plot";

    }
}
