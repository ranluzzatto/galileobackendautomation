﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PlotElementGroup
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/plot/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
