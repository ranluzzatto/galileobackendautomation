﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Component
{
    public class ComponentsGetApi : GalileoApiBase
    {
        public ComponentsGetApi(Dictionary<string, string> uriQueryParams)
            : base(null, uriQueryParams) { }

        protected override string ApiRoutePart => "/config/{configId}/element-types/{elementTypeId}/components";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
