﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PasswordRestoreTicket
{
    public class PasswordRestoreTicketPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/password-restore-ticket";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaulyPayload = @"{""userName"": ""NoName""}";
    }
}
