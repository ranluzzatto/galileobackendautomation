﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PasswordRestoreTicket
{
    public class ExistGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/password-restore-ticket/{ticketId}/exist";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
