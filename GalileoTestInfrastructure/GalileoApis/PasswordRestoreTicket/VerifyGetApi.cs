﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PasswordRestoreTicket
{
    public class VerifyGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/password-restore-ticket/{TicketId}/verify";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
