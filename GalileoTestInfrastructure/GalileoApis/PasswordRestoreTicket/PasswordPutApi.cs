﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PasswordRestoreTicket
{
    public class PasswordPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/password-restore-ticket/{ticketId}/password";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaulyPayload = @"{""ticketId"":""id"",""newPassword"":""userName""}";
    }
}
