﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PasswordRestoreTicket
{
    public class SendPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/password-restore-ticket/send";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaulyPayload = @"{""ticketId"":""id"",""userName"":""userName""}";
    }
}
