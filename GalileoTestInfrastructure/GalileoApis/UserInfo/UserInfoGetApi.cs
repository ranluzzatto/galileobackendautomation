﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.UserInfo
{
    public class UserInfoGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/user/current/user-info";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
