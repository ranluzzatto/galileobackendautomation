﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class MapImageIdGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-images/{mapImageId}";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
