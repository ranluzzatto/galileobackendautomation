﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class ActivePutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/map-images/active";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""mapImageID"":""""}";
    }
}
