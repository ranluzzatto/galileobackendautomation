﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class ActivePostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/map-images/active";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""mapImageID"":""""}";
    }
}
