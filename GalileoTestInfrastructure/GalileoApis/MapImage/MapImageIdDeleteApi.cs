﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class MapImageIdDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-images/{mapImageId}";
        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
