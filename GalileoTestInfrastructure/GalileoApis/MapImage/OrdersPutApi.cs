﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class OrdersPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-images/{mapImageId}/orders";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"[{""mapImageID"":""00000000-0000-0000-0000-000000000000"",""order"":0}]";
    }
}
