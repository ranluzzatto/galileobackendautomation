﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class BackgroundPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-images/{mapImageId}/background";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""mapImageID"":""""}";
    }
}
