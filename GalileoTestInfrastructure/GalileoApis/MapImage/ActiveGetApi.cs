﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class ActiveGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/map-images/active";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
