﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class MapImagesGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/map-images";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
