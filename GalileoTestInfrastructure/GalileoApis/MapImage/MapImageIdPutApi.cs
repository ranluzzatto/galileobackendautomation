﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class MapImageIdPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-images/{mapImageId}";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""name"":""RegularName""}";
    }
}
