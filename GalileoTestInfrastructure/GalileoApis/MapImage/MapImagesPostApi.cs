﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapImage
{
    public class MapImagesPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/map-images";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""backgroundMode"":""Image"",""name"":""string"",""imageData"":""string"",""backgroundColor"":""string"",""order"":0}";
    }
}
