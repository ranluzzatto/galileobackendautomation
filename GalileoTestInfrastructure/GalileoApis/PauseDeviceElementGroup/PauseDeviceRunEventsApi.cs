﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PauseDeviceElementGroup
{
    public class PauseDeviceRunEventsApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/config/{configId}/element-group/pause-device/run-events";
    }
}
