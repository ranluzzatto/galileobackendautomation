﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationSetupElementGroup
{
    public class IrrigationSetupGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{ghConfigId}/greenhouse/{greenHouseId}/element-group/irrigation-setup";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
