﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ProjectCenter
{
    public class ProjectCenterPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project-center";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""projectId"":0,""name"":""string"",""projectManagerUserId"":0,""distributorUserId"":0,""distributorUser"":{""userID"":0,""fullName"":""string""},""controllers"":[{""configId"":0,""commUnitId"":0,""controlUnitId"":0,""name"":""string"",""type"":""OpenField"",""paymentExpirationDate"":""2022-09-07T06:06:59.990Z"",""isSaleTableExist"":true,""relatedToOneProject"":true,""buyTableProjectId"":0,""serialNumber"":""string""}],""users"":[{""userID"":0,""fullName"":""string""}],""usersWithActiveProject"":[{""userID"":0,""fullName"":""string""}]}";
    }
}
