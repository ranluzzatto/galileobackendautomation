﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ProjectCenter
{
    public class GetAllGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project-center/all";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
