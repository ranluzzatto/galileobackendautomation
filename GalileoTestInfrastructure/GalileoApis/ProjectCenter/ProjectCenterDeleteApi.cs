﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ProjectCenter
{
    public class ProjectCenterDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project-center/{projectId}";
        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
