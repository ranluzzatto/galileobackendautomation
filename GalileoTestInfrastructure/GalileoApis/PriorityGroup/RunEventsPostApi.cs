﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PriorityGroup
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/priority-group/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
