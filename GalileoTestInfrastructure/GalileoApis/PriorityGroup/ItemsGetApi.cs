﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PriorityGroup
{
    public class ItemsGetApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.GET;
        protected override string ApiRoutePart => "/config/{configId}/program/priority-group/items";

    }
}
