﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PriorityGroup
{
    public class PriorityGroupPostApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/config/{configId}/program/priority-group";
        public string DefaultPayload = @"{""items"":[{""number"":0,""cutInfoAProgram"":true}],""assignProgramItems"":[{""programNumber"":0,""priorityGroupNumber"":0,""priority"":0}]}";

    }
}
