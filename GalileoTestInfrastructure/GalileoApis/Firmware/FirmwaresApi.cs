﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Firmware
{
    public class FirmwaresApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.GET;
        protected override string ApiRoutePart => "firmwares";
    }
}
