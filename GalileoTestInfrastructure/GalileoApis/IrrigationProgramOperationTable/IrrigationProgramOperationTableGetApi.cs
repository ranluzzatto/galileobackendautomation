﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramOperationTable
{
    public class IrrigationProgramOperationTableGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-operation-table";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
