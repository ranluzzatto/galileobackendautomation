﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramOperationTable
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-operation-table/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]";
    }
}
