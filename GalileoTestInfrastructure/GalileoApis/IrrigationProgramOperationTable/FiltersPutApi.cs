﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramOperationTable
{
    public class FiltersPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-operation-table/filters";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""programNumberFrom"":1,""programNumberTo"":200,""activity"":null,""localFertPumpNumber"":null,""fertCenterNumber"":null,""fertCenterProgNumber"":null,""statuses"":[{""status"":0,""selected"":false,""order"":0,""name"":""NotActive""},{""status"":1,""selected"":false,""order"":1,""name"":""DefinitionError""},{""status"":2,""selected"":false,""order"":2,""name"":""NoStartTime""},{""status"":3,""selected"":false,""order"":3,""name"":""Conditioned""},{""status"":4,""selected"":false,""order"":4,""name"":""Active""},{""status"":5,""selected"":false,""order"":5,""name"":""ActivePlusAlarm""},{""status"":6,""selected"":false,""order"":6,""name"":""Pause""},{""status"":7,""selected"":false,""order"":7,""name"":""Fault""},{""status"":8,""selected"":false,""order"":8,""name"":""Waiting""},{""status"":9,""selected"":false,""order"":9,""name"":""WaitingPlusAlarm""},{""status"":10,""selected"":false,""order"":10,""name"":""Irrigating""},{""status"":11,""selected"":false,""order"":11,""name"":""Fertigating""},{""status"":12,""selected"":false,""order"":12,""name"":""IrrigatingPlusAlarm""},{""status"":13,""selected"":false,""order"":13,""name"":""FertigatingPlusAlarm""}]}";
    }
}
