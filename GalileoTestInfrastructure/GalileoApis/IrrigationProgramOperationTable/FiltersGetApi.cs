﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramOperationTable
{
    public class FiltersGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-operation-table/filters";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
