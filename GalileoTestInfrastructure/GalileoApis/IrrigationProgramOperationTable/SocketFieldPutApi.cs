﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramOperationTable
{
    public class SocketFieldPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-operation-table/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""type"":6,""value"":0,""fieldName"":""ValveRemain3"",""numbers"":[1],""fractionalDigitsCount"":2}";
    }
}
