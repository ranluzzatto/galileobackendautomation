﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.TypeEntity
{
    public class UserInfoGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/type-entities/received-by-list";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
