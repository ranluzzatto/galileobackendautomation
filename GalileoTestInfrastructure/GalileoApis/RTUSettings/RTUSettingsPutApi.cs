﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RTUSettings
{
    public class RTUSettingsPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/rtu-settings";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""configID"":0,""rtuCheckSpeed"":""NormalCycle"",""lowBattery"":""Ignore"",""unitUnknown"":""Ignore"",""generalRTUFault"":""Ignore"",""noAnswerFromUnit"":""Ignore"",""messageDelay"":0,""alarmDelay"":0,""messagesTargetDef"":""AlarmsDiary""}";
    }
}
