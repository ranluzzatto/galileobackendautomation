﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RTUSettings
{
    public class RTUSettingsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/rtu-settings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
