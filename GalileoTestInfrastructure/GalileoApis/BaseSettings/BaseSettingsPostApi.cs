﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.BaseSettings
{
    public class BaseSettingsPostApi : GalileoApiBase
    {
      

        protected override string ApiRoutePart => "/config/{configId}/base-settings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string DefaultPayload = @"{""item"":{""tcpActive"":true,""serverAddress"":""test"",""port"":1,""keepAlive"":2,""serialActive"":true,""controllerID"":3,""baudRate"":1,""wifiActive"":true,""wifiNetwork"":""4"",""wifiPassword"":""5"",""modemActive"":true,""modemApn"":""6"",""modemUser"":""7"",""modemPassword"":""8"",""modemDelayConnect"":1}}";


    }
}


