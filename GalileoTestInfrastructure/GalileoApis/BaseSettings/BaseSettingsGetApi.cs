﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.BaseSettings
{
    public class BaseSettingsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/base-settings";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}

