﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PaymentPrices
{
    public class PaymentPricesPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payment-prices";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaulyPayload = @"[{""paymentPriceID"":0,""paymentPriceType"":""User"",""coin"":""ILS"",""price"":0}]";
    }
}
