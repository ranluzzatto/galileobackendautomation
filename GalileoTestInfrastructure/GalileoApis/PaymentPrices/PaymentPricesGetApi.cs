﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.PaymentPrices
{
    public class PaymentPricesGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payment-prices";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
