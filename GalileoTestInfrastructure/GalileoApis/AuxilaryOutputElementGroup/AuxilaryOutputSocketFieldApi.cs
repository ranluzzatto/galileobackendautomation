﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.AuxilaryOutputElementGroup
{
    public class AuxilaryOutputSocketFieldApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/auxilary-output/socket-field";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
