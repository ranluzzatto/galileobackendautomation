﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.AuxilaryOutputElementGroup
{
    public class AuxilaryOutputRunEventsApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/auxilary-output/run-events";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
