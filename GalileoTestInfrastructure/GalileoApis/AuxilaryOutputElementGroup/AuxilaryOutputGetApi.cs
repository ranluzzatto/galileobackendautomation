﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.AuxilaryOutputElementGroup
{
    public class AuxilaryOutputGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/auxilary-output";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
