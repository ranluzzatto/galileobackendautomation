﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.FertilizerPumpElementGroup
{
    public class FertilizerPumpElementGroupInfoGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/fertilizer-pump/info";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
