﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Alarm
{
    public class FertilizerPumpElementGroupSocketField : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/fertilizer-pump/socket-field";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
