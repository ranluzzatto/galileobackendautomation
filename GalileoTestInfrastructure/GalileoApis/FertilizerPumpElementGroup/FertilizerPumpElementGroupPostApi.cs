﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Alarm
{
    public class FertilizerPumpElementGroupPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/fertilizer-pump";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public static string DefaultPayload = @"{""fertilizerPumpItems"":[{""name"":""Fertilizer Pump 1"",""number"":1,""fertPumpType"":0,""fertilizerTypeID"":0,""mainFertilizerPumpElement"":true,""openFertilizerPumpElement"":true,""closeFertilizerPumpElement"":false,""flowElementType"":1,""sensorType"":0,""sensorElementID"":null,""sensorGroupNumber"":null,""sensorSaleTableID"":null,""fertilizerPumpElement"":true,""pulse"":4,""fertFlowRate"":4,""pulseDuration"":4,""minimumAutoPulse"":4,""recommendedCycle"":4,""electronicStroke"":0,""continueOperation"":true,""waterMeterType"":0,""waterMeter"":null,""pipeLineNumber"":null,""pipeFillDelay"":10,""noFertPulse"":4,""uncontrolFert"":4,""autoCancellAlarm"":true,""references"":[]}],""analogSetupItems"":[{""number"":1,""valveFullMoveTime"":0,""minimumStep"":0.2,""ignoreFertFlowBelow"":0}],""analogGeneralSetup"":{""analogValveStartOper"":1,""changeIrrigationProgramOffDelay"":10,""enableFertValveMoveAlarm"":false,""countFertQuantAccordMainFertOper"":false,""fertQuantMeasureCycle"":10},""analogTypeGeneralSetup"":{""maxAutoIncreaseEC"":50,""maxAutoDecreaseEC"":50,""targetReachedBandEC"":1,""maxAutoIncreasePH"":50,""maxAutoDecreasePH"":50,""targetReachedBandPH"":1,""maxAutoIncreaseGen"":50,""maxAutoDecreaseGen"":50,""targetReachedBandGen"":1},""autoGeneralSetup"":{""pid"":0,""fertFlowAutoCycle"":10,""maxValveMove"":5,""maxTrialsToReachFlowTarget"":3},""pidSetupItems"":[{""number"":1,""firstOpenStepOnStartCycle"":20,""proportionCoefficient"":0.15,""integralCoefficient"":0.05,""derivativeCoefficient"":1,""maxValveMoveSec"":5}],""generalDerivativeSetup"":{""significantFlowChangeEC"":0,""derivativeFlowDomainEC"":0,""significantFlowChangePH"":0,""derivativeFlowDomainPH"":0,""significantFlowChangeGen"":0,""derivativeFlowDomainGen"":0}}";
    }
}