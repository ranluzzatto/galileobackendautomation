﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.FertilizerPumpElementGroup
{
    public class FertilizerPumpElementGroupRunEvents : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/fertilizer-pump/run-events";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
