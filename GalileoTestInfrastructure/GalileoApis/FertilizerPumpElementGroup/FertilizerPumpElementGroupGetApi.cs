﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.FertilizerPumpElementGroup
{
    public class FertilizerPumpElementGroupGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/fertilizer-pump";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
