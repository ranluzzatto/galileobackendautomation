﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RealTime
{
    public class StatusTestModeSocketFieldPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/real-time/status-test-mode-socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""startStopValue"":""Stop""}";
    }
}
