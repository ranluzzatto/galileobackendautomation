﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RealTime
{
    public class ResetLogsPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/real-time/reset-logs/{logType}";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
