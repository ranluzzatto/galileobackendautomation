﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RealTime
{
    public class ProgramTypeRunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/real-time/programType/{programType}/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""getAll"":true,""page"":0,""step"":0,""skip"":0,""take"":0}";
    }
}
