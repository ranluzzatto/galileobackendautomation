﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RealTime
{
    public class RunTestModeEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/real-time/run-test-mode-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
