﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RealTime
{
    public class IsExistControllerSetupUpdateGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/is-exist-controller-setup-update";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
