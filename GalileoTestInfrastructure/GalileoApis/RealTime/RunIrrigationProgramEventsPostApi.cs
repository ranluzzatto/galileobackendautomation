﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RealTime
{
    public class RunIrrigationProgramEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/real-time/run-irrigation-program-status-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""getAll"":true,""page"":0,""step"":0,""skip"":0,""take"":0}";
    }
}
