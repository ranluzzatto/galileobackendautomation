﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RealTime
{
    public class RunControllerStatusEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/real-time/run-controller-status-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
