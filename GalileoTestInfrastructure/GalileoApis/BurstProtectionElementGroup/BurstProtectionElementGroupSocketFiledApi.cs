﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.BurstProtectionElementGroup
{
    public class BurstProtectionElementGroupSocketFiledApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/burst-protection/socket-field";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
