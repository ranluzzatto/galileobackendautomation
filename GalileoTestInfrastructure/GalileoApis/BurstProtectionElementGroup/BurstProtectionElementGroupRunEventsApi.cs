﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.BurstProtectionElementGroup
{
    public class BurstProtectionElementGroupRunEventsApi : GalileoApiBase

    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/config/{configId}/element-group/burst-protection/run-events";


    }
}
