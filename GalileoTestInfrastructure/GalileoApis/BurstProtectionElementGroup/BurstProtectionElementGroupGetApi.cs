﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.BurstProtectionElementGroup
{
    public class BurstProtectionElementGroupGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/burst-protection";
        internal override HttpRequestType RequestType => HttpRequestType.GET;

    }
}
