﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.FilterFlushElementGroup
{
   public class FilterFlushSocketFieldApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.PUT;

        protected override string ApiRoutePart => "/config/{configId}/element-group/filter-flush/socket-field";
    }
}
