﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.FilterFlushElementGroup
{
   public class FilterFlushRunEventsApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;

        protected override string ApiRoutePart => "/config/{configId}/element-group/filter-flush/run-events";
    }
}
