﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterPumpElementGroup
{
    public class ListGetApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.GET;
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-pump";

    }
}
