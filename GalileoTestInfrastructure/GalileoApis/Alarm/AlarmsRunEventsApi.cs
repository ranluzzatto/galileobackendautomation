﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Alarm
{
    public class AlarmsRunEventsApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configID}/alarms/run-events";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
