﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Alarm
{
    public class AlarmsSocketFieldApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/alarms/socket-field";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
