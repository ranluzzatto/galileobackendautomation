﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Alarm
{
    public class AlarmsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configID}/alarms";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
