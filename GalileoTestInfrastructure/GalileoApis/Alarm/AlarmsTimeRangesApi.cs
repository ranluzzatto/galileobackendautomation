﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Alarm
{
    public class AlarmsTimeRangesApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configID}/alarms/time-ranges";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
