﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ValveSetupElementGroup
{
    public class VersionGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/version";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
