﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class DownloadPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/messages/download";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""messageID"":0,""culture"":""EN""}";
    }
}
