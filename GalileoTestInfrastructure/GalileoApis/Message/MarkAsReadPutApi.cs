﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class MarkAsReadPutApi : GalileoApiBase
    {
        public MarkAsReadPutApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/user-messages/make-as-read";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
