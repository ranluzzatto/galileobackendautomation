﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class MessageIdDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/user-messages/{messageId}";
        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
