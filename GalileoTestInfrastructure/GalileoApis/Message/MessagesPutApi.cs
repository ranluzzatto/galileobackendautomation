﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class MessagesPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/messages";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""messageID"":0,""startDate"":""2022-08-02T10:13:02.612Z"",""endDate"":""2022-08-02T10:13:02.612Z""}";
    }
}
