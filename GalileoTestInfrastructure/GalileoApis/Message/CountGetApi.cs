﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class CountGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/messages/count";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
