﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class UserMessagesGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/user-messages";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
