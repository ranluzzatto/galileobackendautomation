﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class MessagesGetApi : GalileoApiBase
    {
        public MessagesGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/messages";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
