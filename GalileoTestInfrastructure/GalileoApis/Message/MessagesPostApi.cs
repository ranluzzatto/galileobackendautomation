﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Message
{
    public class MessagesPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/messages";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
