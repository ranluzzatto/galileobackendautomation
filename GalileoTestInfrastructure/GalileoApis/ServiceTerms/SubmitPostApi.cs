﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class SubmitPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/submit";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
