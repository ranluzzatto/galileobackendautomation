﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class InfoGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/info";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
