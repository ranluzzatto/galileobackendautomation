﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class EnInfoGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/en/info";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
