﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class HebServiceTermsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/heb";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
