﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class HebServiceTermsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/heb";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
