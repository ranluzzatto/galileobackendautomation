﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class ServiceTermsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
