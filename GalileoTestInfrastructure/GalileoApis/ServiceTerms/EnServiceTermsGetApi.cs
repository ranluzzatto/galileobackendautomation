﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class EnServiceTermsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/en";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
