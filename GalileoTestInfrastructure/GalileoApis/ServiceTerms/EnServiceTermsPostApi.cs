﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class EnServiceTermsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/en";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
