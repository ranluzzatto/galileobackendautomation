﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ServiceTerms
{
    public class HebInfoGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/service-terms/heb/info";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
