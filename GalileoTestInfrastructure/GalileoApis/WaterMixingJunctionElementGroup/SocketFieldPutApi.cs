﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterMixingJunctionElementGroup
{
    public class SocketFieldPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-mixing-junction/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""type"":""Numeric"",""value"":{},""unitId"":0,""fieldName"":""string"",""numbers"":[0],""fractionalDigitsCount"":0}";
    }
}
