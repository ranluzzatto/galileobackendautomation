﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterMixingJunctionElementGroup
{
    public class WaterMixingJunctionElementGroupPostApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-mixing-junction";
        public static string DefaultPayload = @"{""waterMixingJunctionItems"":[{""number"":0,""name"":"""",""isActive"":true,""pipeLineNumber"":0,""valveControl"":0,""ecmjSensorType"":""None"",""ecmjSensorElementID"":0,""ecmjSensorGroupNumber"":0,""ecmjSensorSaleTableID"":0,""precision"":0,""autoCycleTime"":0,""ecDevisionAlarm"":0,""ecAlarmDelay"":0,""defaultMixingProgram"":""Off"",""programNumber"":0,""byPass"":true}],""waterSourceItems"":[{""number"":0,""name"":"""",""freshPipeLineNumber"":0,""freshActionFullWayTime"":0,""freshActionNumberOfStages"":0,""freshMaximumWaterFlow"":0,""freshWaterSourcePulseVolume"":0,""freshWaterSourcePulseFailDelay"":0,""freshWaterSourceUncontrolledPulses"":0,""freshActionAtSourceFail"":0,""freshWaterSourceStageWhileMixOff"":0,""freshWaterSourceStageWhileIrrigOff"":0,""openFreshWaterElement"":true,""closeFreshWaterElement"":true,""mainFreshWaterElement"":true,""freshWaterCounterElement"":true,""freshWaterFaultElement"":true,""salinePipeLineNumber"":0,""salineActionFullWayTime"":0,""salineActionNumberOfStages"":0,""salineMaximumWaterFlow"":0,""salineWaterSourcePulseVolume"":0,""salineWaterSourcePulseFailDelay"":0,""salineWaterSourceUncontrolledPulses"":0,""salineActionAtSourceFail"":0,""salineWaterSourceStageWhileMixOff"":0,""salineWaterSourceStageWhileIrrigOff"":0,""openSalineWaterElement"":true,""closeSalineWaterElement"":true,""mainSalineWaterElement"":true,""salineWaterCounterElement"":true,""salineWaterFaultElement"":true,""mixingBypassElement"":true}],""waterSourceSetup"":[{""waterSourceNumber"":0,""proportionalCoefficient"":0,""integralCoefficient"":0,""derivateiveCoefficient"":0}]}";
    }
}
