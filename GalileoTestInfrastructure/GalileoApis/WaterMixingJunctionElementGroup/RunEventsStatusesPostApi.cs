﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterMixingJunctionElementGroup
{
    public class RunEventsStatusesPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-mixing-junction/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""numbers"":[0]}";
    }
}
