﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Project
{
    public class ActiveGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/active";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
