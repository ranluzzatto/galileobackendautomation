﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Project
{
    public class ResetGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/config/{configId}/reset";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
