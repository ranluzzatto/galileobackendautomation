﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Project
{
    public class UpdateStatusPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/config/{configId}/update-status";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""status"":""Updated""}";
    }
}
