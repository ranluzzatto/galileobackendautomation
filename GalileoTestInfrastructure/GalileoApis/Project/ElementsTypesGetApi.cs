﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Project
{
    public class ElementsTypesGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/config/{configId}/element-types";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
