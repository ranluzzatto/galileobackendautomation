﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Project
{
    public class ProjectGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
