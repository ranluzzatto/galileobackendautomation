﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Project
{
    public class ActivePostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/active";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""projectID"":0,""controllerID"":0,""greenHouseID"":0}";
    }
}
