﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Project
{
    public class ProjectConfigOnlineStatusApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/config/{configId}/online-status";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
