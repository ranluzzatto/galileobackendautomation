﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterPumpHouseElementGroup
{
    public class WaterQuotaPostApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-pump-house";
        public static string DefaultPayload = @"{""items"":[{""number"":0,""name"":"""",""methodOfOperation"":0,""pipeLineNumber"":0,""waterMeterType"":""None"",""waterMeter"":0,""sensorType"":""None"",""sensorElementID"":0,""sensorGroupNumber"":0,""sensorSaleTableID"":0,""lowPresureStepUp"":0,""highPressureStepDown"":0,""pauseTime"":{""hour"":0,""minute"":0},""resumeTime"":{""hour"":0,""minute"":0},""pauseLogicCondition"":0,""inPause"":0,""saleTableItems"":[{""number"":0,""groupNumber"":0,""saleTableId"":0}]}],""waterPumpCombinationItems"":[{""number"":0,""waterPump1"":0,""waterPump2"":0,""waterPump3"":0,""waterPump4"":0,""waterPump5"":0}],""waterPumpHouseStepItems"":[{""number"":0,""stepNumber"":0,""waterPumpValue1"":true,""waterPumpValue2"":true,""waterPumpValue3"":true,""waterPumpValue4"":true,""waterPumpValue5"":true,""stepUpDelay"":0,""downDelay"":0,""flowRate"":0,""logicConditionNumber"":0}]}";

    }
}
