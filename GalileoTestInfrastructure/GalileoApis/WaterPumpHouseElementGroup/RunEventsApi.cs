﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterPumpHouseElementGroup
{
    public class RunEventsApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-pump-house/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""numbers"":[0]}";
    }
}
