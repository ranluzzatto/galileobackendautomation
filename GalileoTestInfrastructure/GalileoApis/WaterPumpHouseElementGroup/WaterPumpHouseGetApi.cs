﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterPumpHouseElementGroup
{
    public class WaterPumpHouseGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-pump-house";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
