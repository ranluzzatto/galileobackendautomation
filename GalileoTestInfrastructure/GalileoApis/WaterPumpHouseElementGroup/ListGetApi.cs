﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterPumpHouseElementGroup
{
    public class ListGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/water-pump-house/list";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
