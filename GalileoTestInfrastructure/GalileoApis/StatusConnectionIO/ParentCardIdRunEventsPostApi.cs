﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.StatusConnectionIO
{
    public class ParentCardIdRunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/parent-cards/{parentCardId}/status-connection-io/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
