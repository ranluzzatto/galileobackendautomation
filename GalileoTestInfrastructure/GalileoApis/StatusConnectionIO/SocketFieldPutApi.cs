﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.StatusConnectionIO
{
    public class SocketFieldPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/cards/{cardId}/status-connection-io/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
