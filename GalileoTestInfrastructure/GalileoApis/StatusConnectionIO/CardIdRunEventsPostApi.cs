﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.StatusConnectionIO
{
    public class CardIdRunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/cards/{cardId}/status-connection-io/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
