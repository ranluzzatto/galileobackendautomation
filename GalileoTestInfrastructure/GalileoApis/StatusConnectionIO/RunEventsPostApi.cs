﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.StatusConnectionIO
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/status-connection-io/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
