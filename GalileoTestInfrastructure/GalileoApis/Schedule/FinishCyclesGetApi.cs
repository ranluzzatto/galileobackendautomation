﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Schedule
{
    public class FinishCyclesGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/schedule/finish-cycles";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
