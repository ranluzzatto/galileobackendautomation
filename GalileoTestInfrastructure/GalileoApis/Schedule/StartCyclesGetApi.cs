﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Schedule
{
    public class StartCyclesGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/schedule/start-cycles";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
