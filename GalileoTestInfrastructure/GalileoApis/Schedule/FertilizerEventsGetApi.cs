﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Schedule
{
    public class FertilizerEventsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/schedule/fertilizer-events";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
