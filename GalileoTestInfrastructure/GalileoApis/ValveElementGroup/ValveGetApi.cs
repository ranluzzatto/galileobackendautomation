﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ValveElementGroup
{
    public class ValveSetupGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/valve";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
