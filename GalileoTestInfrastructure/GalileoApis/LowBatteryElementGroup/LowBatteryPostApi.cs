﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.LowBatteryElementGroup
{
    public class LowBatteryPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/low-battery";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaulyPayload = @"{""name"":""string"",""conditionInputForAlarmElementID"":0,""conditionInputForPauseElementID"":0}";
    }
}
