﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.LowBatteryElementGroup
{
    public class LowBatteryGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/low-battery";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
