﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MeteorologyGHElementGroup
{
    public class MeteorologyPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/meteorology";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""name"":""string"",""windSpeedLevel1"":0,""windSpeedLevel2"":0,""windSpeedLevel3"":0,""windSpeedLevel4"":0,""windSpeedLevel5"":0,""windStageCheckTime"":0,""windStageDecrDelayTime"":0,""windDirectionCheckTime"":0,""rainCondDefinitionBy"":""AccordingRainCounter"",""minimumTimeAtRainyStatus"":{""minute"":0,""second"":0},""snowCondition"":""RainCondition"",""lowExtTempForSnowDef"":0,""minimumTimeAtSnowStatus"":0,""externalTempSnsType"":""None"",""externalTempSnsElementID"":0,""externalTempSnsGroupNumber"":0,""externalHumSnsType"":""None"",""externalHumSnsElementID"":0,""externalHumSnsGroupNumber"":0,""externalRadiationGenSnsType"":""None"",""externalRadiationGenSnsElementID"":0,""externalRadiationGenSnsGroupNumber"":0,""windSpeedGenSnsType"":""None"",""windSpeedGenSnsElementID"":0,""windSpeedGenSnsGroupNumber"":0,""windDirectionGenSnsType"":""None"",""windDirectionGenSnsElementID"":0,""windDirectionGenSnsGroupNumber"":0,""rainCounterInputConnectionElementID"":0,""rainCounterPulseSizeMil"":0,""precipitationDetectorInputConnectionElementID"":0,""precipitationDetectorONOFF"":""OFF"",""setLastMinutesAverageRadiationCycle"":0}";
    }
}
