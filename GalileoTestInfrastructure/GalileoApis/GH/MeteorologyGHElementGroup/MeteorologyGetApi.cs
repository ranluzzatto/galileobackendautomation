﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MeteorologyGHElementGroup
{
    public class MeteorologyGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/meteorology";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
