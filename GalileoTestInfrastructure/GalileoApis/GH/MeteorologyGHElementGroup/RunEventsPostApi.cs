﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MeteorologyGHElementGroup
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/meteorology/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
