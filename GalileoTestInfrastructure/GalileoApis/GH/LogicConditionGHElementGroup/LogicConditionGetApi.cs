﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.LogicConditionGHElementGroup
{
    public class LogicConditionGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/logic-condition";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
