﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationValve
{
    public class InfoGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/irrigation-valve/info";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
