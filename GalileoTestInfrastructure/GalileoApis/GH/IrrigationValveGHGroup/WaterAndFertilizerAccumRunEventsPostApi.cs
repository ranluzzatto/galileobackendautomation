﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationValve
{
    public class WaterAndFertilizerAccumRunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/irrigation-valve/water-and-fertilizer-accum/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""numbers"":[0]}";
    }
}
