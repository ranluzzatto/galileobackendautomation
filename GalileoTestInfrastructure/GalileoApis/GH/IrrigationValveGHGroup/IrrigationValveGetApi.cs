﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationValve
{
    public class IrrigationValveGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/irrigation-valve";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
