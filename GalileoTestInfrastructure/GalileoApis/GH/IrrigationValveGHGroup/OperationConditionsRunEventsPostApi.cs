﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationValve
{
    public class OperationConditionsRunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/irrigation-valve/operation-conditions/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""numbers"":[0]}";
    }
}
