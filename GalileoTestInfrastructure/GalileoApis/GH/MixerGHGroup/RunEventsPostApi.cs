﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MixerGHGroup
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/mixer/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""numbers"":[0]}";
    }
}
