﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MixerGHGroup
{
    public class MixerPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/mixer";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""view"":{""waterFillingCounterPulseSizeLitre"":0,""mixerTankVolumeLitre"":0,""fillingTankMaxTimeSec"":0,""emptyingTankMaxTimeSec"":0,""waterCirculationMaxTimeSec"":0,""useLevelMeterIfPropErrorDontUse"":0,""sensorAType"":""None"",""sensorAElementID"":0,""sensorAGroupNumber"":0,""valueToStopFillingValveAbove"":0,""fillingDelayAfterFullTankIndicationSec"":0,""pauseFillingValveByConditionInputElementID"":0,""valueToOpenMainValveAbove"":0,""valueToCloseMainValveBelow"":0,""actualMainValveCloseTimeSec"":0,""valueToStopMixerPumpBelow"":0,""pumpStopDelayAfterEmptyTankSignSec"":0,""pauseMixerPumpByConditionInputElementID"":0,""maxTimeToFinishFertPulsesSec"":0,""maxTimeToForFertCirculationSec"":0,""ecDifferFromRequiredToStartIrrig"":0,""phDifferFromRequiredToStartIrrig"":0,""maxAttemptsToReachOpenLevel"":0,""enableContinouseIrrigation"":0,""maxFertDifferenceForSimilarProgDefinition"":0,""useEmptingValveForOverQuantity"":0,""ignoreEcPhAutoLimitDuringFertAddition"":0,""pauseMixerIfEcPhOutOfRangeAndPassedMaxFertAddAttempt"":0}}";
    }
}
