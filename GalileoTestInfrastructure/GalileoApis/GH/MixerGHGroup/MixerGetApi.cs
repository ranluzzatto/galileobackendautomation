﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MixerGHGroup
{
    public class MixerGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/mixer";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
