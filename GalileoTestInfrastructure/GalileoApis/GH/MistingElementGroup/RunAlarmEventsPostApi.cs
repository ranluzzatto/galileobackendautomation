﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MistingElementGroup
{
    public class RunAlarmEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/misting/run-alarms-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
