﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MistingElementGroup
{
    public class MistingPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/misting";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"[{""number"":0,""name"":""string"",""startTimeByConditions"":{""minute"":0,""second"":0},""endTimeByConditions"":{""minute"":0,""second"":0},""tempToOperAbove"":0,""tempDifferenceToStop"":0,""humToOperBelow"":0,""humDifferenceToStop"":0,""operConditionSetup"":""ByTime"",""tempToDecWaitAbove"":0,""minimumWaitTime"":0,""tempToStopBelow"":0,""humToStopAbove"":0,""workTime"":0,""waitTime"":0,""tempSensorType"":""None"",""tempSensorElementID"":0,""tempSensorGroupNumber"":0,""humSensorType"":""None"",""humSensorElementID"":0,""humSensorGroupNumber"":0,""startTimeA"":{""minute"":0,""second"":0},""endTimeA"":{""minute"":0,""second"":0},""workTimeA"":0,""waitTimeA"":0,""startTimeB"":{""minute"":0,""second"":0},""endTimeB"":{""minute"":0,""second"":0},""workTimeB"":0,""waitTimeB"":0,""mistingGroup"":0,""references"":[{""id"":{},""typeName"":""string"",""number"":0,""groupNumber"":0,""name"":""string""}]}]";
    }
}
