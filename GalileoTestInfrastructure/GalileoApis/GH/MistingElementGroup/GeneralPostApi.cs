﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MistingElementGroup
{
    public class GeneralPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/misting/general";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""genSensorNoToStopMistingsType"":""None"",""genSensorNoToStopMistingsElementID"":0,""genSensorNoToStopMistingsGroupNumber"":0,""lowValueGenSensorToStop"":0,""highValueGenSensorToStop"":0,""mistingCounterExist"":0,""uncontrolledMistPulses"":0,""mistingNotExecuted"":0,""mainMistValveOperTime"":""MistingRequired"",""operMainMistBeforeMistValve"":0,""stopMainMistAfterMistValve"":0,""stopIrrigationDuringMisting"":""No"",""operMainIrrigValveDuringMisting"":""No"",""operateMainWaterPumpDuringMisting"":""No"",""isMistingMeterExist"":true,""decreaseWaitTimeAbove"":0}";
    }
}
