﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MistingElementGroup
{
    public class RunManualOverridingEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/misting/run-manual-overriding-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
