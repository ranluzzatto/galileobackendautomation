﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MistingElementGroup
{
    public class MistingGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/element-group/misting";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
