﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramMultiPrograms
{
    public class FiltersGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-multi-programs/filters";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
