﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramMultiPrograms
{
    public class IrrigationProgramMultiProgramsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-multi-programs";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
