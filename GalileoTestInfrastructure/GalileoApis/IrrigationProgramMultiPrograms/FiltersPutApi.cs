﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramMultiPrograms
{
    public class FiltersPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-multi-programs/filters";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;

        public string DefaultPayload = @"{""programNumberFrom"":1,""programNumberTo"":20,""status"":null,""activity"":null,""localFertPumpNumber"":0,""fertCenterNumber"":0,""fertCenterProgNumber"":0}";
    }
}
