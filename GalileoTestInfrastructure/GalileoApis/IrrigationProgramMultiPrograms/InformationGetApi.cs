﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramMultiPrograms
{
    public class InformationGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program-multi-programs/information";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
