﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrrigationProgramMultiPrograms
{
    public class SocketFieldPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/program/irrigation-program/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;

        public string DefaultPayload = @"{""type"":3,""value"":1,""fieldName"":""Restart"",""numbers"":[1]}";
    }
}
