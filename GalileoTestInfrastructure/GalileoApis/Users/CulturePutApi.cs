﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class CulturePutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users/culture";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public static string DefaultPayload = @"{""culture"": ""EN""}";
    }
}