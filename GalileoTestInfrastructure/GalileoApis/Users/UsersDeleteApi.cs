﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class UsersDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users/{userId}";

        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
