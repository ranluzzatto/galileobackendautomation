﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class UsersGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
