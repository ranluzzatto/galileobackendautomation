﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class ResetPasswordPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users/reset-password";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public static string DefaultPayload = @"{""userID"":0,""password"":""""}";
    }
}
