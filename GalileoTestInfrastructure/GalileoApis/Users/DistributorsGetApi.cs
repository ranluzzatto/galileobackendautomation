﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class DistributorsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users/distributors";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
