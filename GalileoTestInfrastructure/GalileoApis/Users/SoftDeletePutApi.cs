﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class SoftDeletePutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users/{userId}/soft-delete";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
