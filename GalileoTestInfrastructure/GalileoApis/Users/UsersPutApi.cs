﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class UsersPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public static string DefaultPayload = @"{""userID"":0,""fullName"":""Check"",""email"":""stam@galcon.co.il"",""role"":""God"",""status"":true,""username"":""string"",""password"":""string"",""timeZoneID"":0,""mobileNumber"":""string"",""createdDate"":""2023-01-17T10:00:17.973Z"",""paymentExpirationDate"":""2027-01-17T10:00:17.973Z"",""isDeleted"":false,""isFree"":true,""distributorCountryID"":0,""distributorRegionID"":0,""distributorSalesmanID"":0,""isSubmitLastServiceTerms"":true,""culture"":""EN"",""projects"":[{""name"":"""",""id"":0}]}";
    }
}
