﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Users
{
    public class CurrentGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/users/current";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
