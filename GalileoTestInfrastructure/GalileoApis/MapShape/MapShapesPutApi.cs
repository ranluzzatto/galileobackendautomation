﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapShape
{
    public class MapShapesPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-image/{mapImageId}/map-shapes";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"[{""mapShapeID"":""00000000-0000-0000-0000-000000000000"",""mapImageID"":""00000000-0000-0000-0000-000000000000"",""mapShapeType"":""Rectangle"",""coordinateX"":0,""coordinateY"":0,""width"":0,""height"":0,""angle"":0,""background"":""string"",""borderWidth"":0,""borderColor"":""string"",""points"":[{""x"":0,""y"":0}],""text"":""string"",""fontSettings"":{""fontSize"":0,""fontFamily"":""string"",""color"":""string"",""fontStyle"":""Normal"",""fontWeight"":""Thin""},""groupNumber"":0,""relatedMapImageID"":""00000000-0000-0000-0000-000000000000"",""descriptionPosition"":""Top""}]";
    }
}
