﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapShape
{
    public class DefaultMapShapesSettingsPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/default-map-shapes-settings";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""borderWidth"":0,""borderColor"":""string"",""background"":""string"",""fontSettings"":{""fontSize"":0,""fontFamily"":""string"",""color"":""string"",""fontStyle"":""Normal"",""fontWeight"":""Thin""}}";
    }
}
