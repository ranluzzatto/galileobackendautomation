﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapShape
{
    public class DefaultMapShapesSettingsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/default-map-shapes-settings";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
