﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapShape
{
    public class CountGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-image/{mapImageId}/map-shapes";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
