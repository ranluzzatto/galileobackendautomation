﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportResult
{
    public class ValveResultJsonGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/single/result/{reportId}/valve/{valveNumber}/json";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
