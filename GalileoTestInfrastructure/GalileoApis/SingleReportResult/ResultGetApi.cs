﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportResult
{
    public class ResultGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/single/result/{reportId}";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
