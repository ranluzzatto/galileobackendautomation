﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportResult
{
    public class ResultJsonGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/single/result/{reportId}/json";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
