﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportResult
{
    public class ValveResultExcelGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/single/result/{reportId}/valve/{valveNumber}/excel";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
