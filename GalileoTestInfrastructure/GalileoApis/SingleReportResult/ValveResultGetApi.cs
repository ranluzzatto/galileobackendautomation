﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportResult
{
    public class ValveResultGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/single/result/{reportId}/valve/{valveNumber}";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
