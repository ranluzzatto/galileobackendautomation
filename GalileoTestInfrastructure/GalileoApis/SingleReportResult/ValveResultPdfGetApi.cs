﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportResult
{
    public class ValveResultPdfGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/single/result/{reportId}/valve/{valveNumber}/pdf";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
