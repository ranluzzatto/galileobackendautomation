﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SensorGraphs
{
    public class DataPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/sensor-graphs/data";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""startDate"":""2022-09-13T08:57:05.321Z"",""endDate"":""2022-09-13T08:57:05.321Z"",""sensorNumbers"":[0]}";
    }
}
