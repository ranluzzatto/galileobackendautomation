﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class UsersPaymentReportGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/users-payment-report";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}