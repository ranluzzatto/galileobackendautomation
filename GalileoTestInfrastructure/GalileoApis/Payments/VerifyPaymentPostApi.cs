﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class VerifyPaymentPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/verify-payment";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""id"":0,""cCode"":0,""amount"":0,""aCode"":"""",""order"":0,""fild1"":"""",""fild2"":"""",""fild3"":"""",""sign"":"""",""bank"":0,""payments"":0,""userId"":0,""brand"":0,""issuer"":0,""l4digit"":"""",""street"":"""",""city"":"""",""zip"":"""",""cell"":"""",""coin"":""ILS"",""tmonth"":"""",""tyear"":"""",""hesh"":""""}";
    }
}