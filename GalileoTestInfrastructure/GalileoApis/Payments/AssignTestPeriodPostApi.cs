﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class AssignTestPeriodPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/assign-test-period";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""testPeriod"":""TwoWeeks"",""userIds"":[0],""configIds"":[0]}";
    }
}