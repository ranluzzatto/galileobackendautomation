﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class ReportGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/report";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}