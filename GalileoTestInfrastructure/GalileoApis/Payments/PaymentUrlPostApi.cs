﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class PaymentUrlPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/get-payment-url";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""paymentActionType"":""Manual"",""paymentType"":""ControllerPayment"",""coin"":""ILS"",""yearCount"":0,""totalPrice"":0,""couponID"":0,""controllerPayments"":[{""previousControllerPaymentID"":0,""configID"":0,""currentExpirationDate"":""2022-09-04T10:01:36.496Z"",""price"":0,""fullPrice"":0}],""userPayments"":[{""previousUserPaymentID"":0,""userID"":0,""currentExpirationDate"":""2022-09-04T10:01:36.496Z"",""price"":0,""fullPrice"":0}]}";
    }
}