﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class UserPaymentsExcelGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/report/user-payments-excel";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}