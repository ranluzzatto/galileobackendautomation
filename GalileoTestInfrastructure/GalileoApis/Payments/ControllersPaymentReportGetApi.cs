﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class ControllersPaymentReportGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/controllers-payment-report";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}