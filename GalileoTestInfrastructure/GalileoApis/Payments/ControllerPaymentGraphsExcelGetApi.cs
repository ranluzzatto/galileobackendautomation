﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class ControllerPaymentGraphsExcelGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/report/controller-payment-graphs-excel";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}