﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class ControllerPaymentsExcelGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/report/controller-payments-excel";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}