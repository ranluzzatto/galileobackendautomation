﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class ManualPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/manual";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""paymentType"":""ControllerPayment"",""manualPaymentType"":""Check"",""coin"":""ILS"",""yearCount"":0,""totalAlreadyPaidSum"":0,""totalPrice"":0,""paymentCodeOfErp"":""string"",""couponID"":0,""controllerPayments"":[{""previousControllerPaymentID"":0,""configID"":0,""currentExpirationDate"":""2022-09-04T10:01:36.499Z"",""price"":0,""fullPrice"":0}],""userPayments"":[{""previousUserPaymentID"":0,""userID"":0,""currentExpirationDate"":""2022-09-04T10:01:36.499Z"",""price"":0,""fullPrice"":0}]}";
    }
}