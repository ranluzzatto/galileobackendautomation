﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class UserPaymentGraphsExcelGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments/report/user-payment-graphs-excel";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}