﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Payments
{
    public class PaymentsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/payments";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}