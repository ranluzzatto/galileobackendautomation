﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrriMax
{
    public class IrrimaxKeyGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/irrimax-api-key";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
