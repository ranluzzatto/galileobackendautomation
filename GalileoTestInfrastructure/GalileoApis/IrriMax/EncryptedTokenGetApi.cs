﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrriMax
{
    public class EncryptedTokenGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/irrimax-api-key/encrypted-token";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
