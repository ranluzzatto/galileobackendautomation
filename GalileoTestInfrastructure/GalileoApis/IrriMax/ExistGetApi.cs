﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.IrriMax
{
    public class ExistGetApi : GalileoApiBase
    {
        public ExistGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/config/{configId}/irrimax-api-key";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
