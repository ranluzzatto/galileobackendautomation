﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.IrriMax
{
    public class IrrimaxKeyPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/irrimax-api-key";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""ApiKey"": ""Check""}";
    }
}
