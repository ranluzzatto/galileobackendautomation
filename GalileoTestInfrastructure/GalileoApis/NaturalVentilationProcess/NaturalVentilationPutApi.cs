﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.NaturalVentilationProcess
{
    public class NaturalVentilationPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/process/natural-ventilation";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaulyPayload = @"{""naturalVentilationStartTime"":{""hour"":0,""minute"":0},""naturalVentilationEndTime"":{""hour"":0,""minute"":0},""windDirectionFrom"":0,""windDirectionTo"":0,""windSpeedSetupAbove"":0,""windSpeedDifferToStop"":0,""startProcessDelay"":0,""endProcessDelay"":0,""operateByTimeVentilationStartTime"":{""hour"":0,""minute"":0},""operateByTimeVentilationEndTime"":{""hour"":0,""minute"":0}}";
    }
}
