﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.NaturalVentilationProcess
{
    public class NaturalVentilationGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/process/natural-ventilation";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
