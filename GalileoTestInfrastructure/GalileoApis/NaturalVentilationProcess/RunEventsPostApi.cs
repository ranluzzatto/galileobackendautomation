﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.NaturalVentilationProcess
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/greenhouse/{greenHouseId}/process/natural-ventilation/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
