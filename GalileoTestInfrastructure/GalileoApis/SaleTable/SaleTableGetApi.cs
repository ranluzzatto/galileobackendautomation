﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SaleTable
{
    public class SaleTableGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/{projectId}/sale-table";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
