﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SaleTable
{
    public class ElementsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/{projectId}/sale-table/config/{configId}/element-type/{elementType}/elements";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
