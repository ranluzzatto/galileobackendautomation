﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SaleTable
{
    public class SaleTablePutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/project/{projectId}/sale-table";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""tableItems"":[{""saleTableId"":0,""index"":0,""projectId"":0,""configId"":0,""elementType"":""None"",""number"":0,""value"":0,""elementName"":""string"",""references"":[{""configId"":0,""controllerName"":""string"",""typeName"":""string"",""name"":""string"",""number"":0,""groupNumber"":0}]}]}";
    }
}
