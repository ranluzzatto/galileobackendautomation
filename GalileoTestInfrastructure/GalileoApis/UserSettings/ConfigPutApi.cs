﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.UserSettings
{
    public class ConfigPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/user-settings/config/{configId}";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public static string DefaultPayload = @"{""defaultPage"":0,""isIrrigationProgramTabsOpen"":true}";
    }
}
