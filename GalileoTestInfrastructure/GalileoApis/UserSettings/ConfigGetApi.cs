﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.UserSettings
{
    public class ConfigGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/user-settings/config/{configId}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
