﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Pause
{
    public class SocketFieldPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/pause-controller/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""type"":""Numeric"",""value"":{},""unitId"":0,""fieldName"":""string"",""numbers"":[0],""fractionalDigitsCount"":0}";
    }
}
