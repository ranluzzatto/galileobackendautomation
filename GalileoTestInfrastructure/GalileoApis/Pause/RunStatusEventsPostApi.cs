﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Pause
{
    public class RunStatusEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/pause-controller/run-status-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
