﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Pause
{
    public class PauseControllerPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/pause-controller";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""item"":{""permanentPauseController"":""No"",""startPause"":{""hour"":0,""minute"":0},""stopPause"":{""hour"":0,""minute"":0}}}";
    }
}
