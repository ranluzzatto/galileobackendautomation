﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Pause
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/pause-controller/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
