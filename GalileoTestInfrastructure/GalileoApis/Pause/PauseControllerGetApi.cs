﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Pause
{
    public class PauseControllerGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/pause-controller";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
