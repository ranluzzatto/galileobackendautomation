﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCardCell
{
    public class AvailableElementTypesPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/card/{cardId}/cell/{cellId}/available-element-types";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""filterType"":""IrrigationSystem"",""greenHouseID"":0}";
    }
}
