﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCardCell
{
    public class CellPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/card/{cardId}/cell/{cellId}";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""greenHouseID"":0,""element"":{""elementID"":0,""name"":""string"",""number"":0,""elementTypeID"":0,""elementGroupID"":0,""additionalFields"":[{""title"":""string"",""type"":""Numeric"",""objList"":[{""id"":0,""number"":0,""name"":""string""}],""value"":{},""unitId"":0,""validationSettings"":{""minValue"":{},""minValueField"":""string"",""maxValue"":{},""maxValueField"":""string"",""notEqualsField"":""string""},""fractionalDigitsCount"":0,""formatValueField"":""string"",""isDisabled"":true}],""references"":[{""name"":""string"",""references"":[{""id"":0,""number"":0,""name"":""string""}]}],""canDeleteReferences"":true}}";
    }
}
