﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCardCell
{
    public class CellGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/card/{cardId}/cell";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
