﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCardCell
{
    public class ElementTypeGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/card/{cardId}/cell/{cellId}/element-type/{elementType}";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
