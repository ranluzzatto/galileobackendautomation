﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MeteorologyElementGroup
{
    public class MeteorologyGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/meteorology";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
