﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MeteorologyElementGroup
{
    public class MeteorologyPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/meteorology";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""name"":""string"",""externalTempSensorType"":""None"",""externalTempSensorElementID"":0,""externalTempSensorGroupNumber"":0,""externalTempSensorSaleTableID"":0,""externalHumiditySensorType"":""None"",""externalHumiditySensorElementID"":0,""externalHumiditySensorGroupNumber"":0,""externalHumiditySensorSaleTableID"":0,""radiationSensorType"":""None"",""radiationSensorElementID"":0,""radiationSensorGroupNumber"":0,""radiationSensorSaleTableID"":0,""windSpeedSensorType"":""None"",""windSpeedSensorElementID"":0,""windSpeedSensorGroupNumber"":0,""windSpeedSensorSaleTableID"":0,""windDirectionSensorType"":""None"",""windDirectionSensorElementID"":0,""windDirectionSensorGroupNumber"":0,""windDirectionSensorSaleTableID"":0,""rainFactor"":0}";
    }
}
