﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportCenter
{
    public class SinglePostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/single";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""reportId"":0,""userId"":0,""userName"":""string"",""name"":""string"",""timePeriodView"":{""type"":""Period"",""startDate"":""2022-09-28T07:23:37.689Z"",""endDate"":""2022-09-28T07:23:37.689Z""},""mainFilter"":""Valve"",""groupFilter"":""Valve"",""waterDiff"":0,""selectedFilters"":[{}],""columns"":[""Area""]}";
    }
}
