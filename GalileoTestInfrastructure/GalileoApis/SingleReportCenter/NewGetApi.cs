﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportCenter
{
    public class NewGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/single/new";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
