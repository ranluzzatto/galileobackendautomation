﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportCenter
{
    public class FiltersPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/single/get-filters";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""timePeriodView"":{""type"":""Period"",""startDate"":""2022-10-06T06:31:43.352Z"",""endDate"":""2022-10-06T06:31:43.352Z""}}";
    }
}
