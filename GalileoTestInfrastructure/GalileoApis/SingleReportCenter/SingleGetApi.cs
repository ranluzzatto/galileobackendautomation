﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportCenter
{
    public class SingleGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/single";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
