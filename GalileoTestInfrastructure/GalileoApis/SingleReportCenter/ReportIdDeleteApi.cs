﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SingleReportCenter
{
    public class ReportIdDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report-center/{configId}/single/{reportId}";
        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
