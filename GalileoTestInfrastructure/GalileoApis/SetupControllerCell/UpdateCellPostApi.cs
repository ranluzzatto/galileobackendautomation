﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCell
{
    public class UpdateCellPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setupControllerCell/updateCell";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""cellID"":0,""selectedGreenHouseID"":0,""elements"":[{""elementID"":0,""name"":"""",""number"":0,""elementTypeID"":0,""elementGroupID"":0,""additionalFields"":[{""title"":"""",""type"":""Numeric"",""objList"":[{""id"":0,""number"":0,""name"":""""}],""value"":{},""unitId"":0,""validationSettings"":{""minValue"":{},""minValueField"":"""",""maxValue"":{},""maxValueField"":"""",""notEqualsField"":""""},""fractionalDigitsCount"":0,""formatValueField"":"""",""isDisabled"":true}],""references"":[{""name"":"""",""references"":[{""id"":0,""number"":0,""name"":""""}]}],""canDeleteReferences"":true}]}";
    }
}
