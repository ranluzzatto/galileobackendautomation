﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCell
{
    public class GetAllowGreenHousesGetApi : GalileoApiBase
    {
        public GetAllowGreenHousesGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setupControllerCell/getAllowGreenHouses";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
