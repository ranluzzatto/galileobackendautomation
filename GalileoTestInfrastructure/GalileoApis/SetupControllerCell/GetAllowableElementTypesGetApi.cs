﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCell
{
    public class GetAllowableElementTypesGetApi : GalileoApiBase
    {
        public GetAllowableElementTypesGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setupControllerCell/getAllowableElementTypes";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
