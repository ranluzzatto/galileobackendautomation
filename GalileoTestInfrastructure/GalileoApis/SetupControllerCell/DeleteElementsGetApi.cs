﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCell
{
    public class DeleteElementsGetApi : GalileoApiBase
    {
        public DeleteElementsGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setupControllerCell/deleteElements";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
