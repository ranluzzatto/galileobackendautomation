﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCell
{
    public class GetElementTypesGetApi : GalileoApiBase
    {
        public GetElementTypesGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setupControllerCell/getElementTypes";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
