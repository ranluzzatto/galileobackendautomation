﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCell
{
    public class GetDefaultElementGetApi : GalileoApiBase
    {
        public GetDefaultElementGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setupControllerCell/getDefaultElement";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
