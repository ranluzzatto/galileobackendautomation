﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SetupControllerCell
{
    public class GetEditSellViewGetApi : GalileoApiBase
    {
        public GetEditSellViewGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setupControllerCell/getEditCellView";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
