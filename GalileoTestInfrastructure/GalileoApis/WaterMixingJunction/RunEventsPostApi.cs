﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.WaterMixingJunction
{
    public class RunEventsPostApi : GalileoApiBase
    {
        public RunEventsPostApi(Dictionary<string, string> uriLocalParams)
           : base(null, uriLocalParams) { }

        protected override string ApiRoutePart => "/config/{configId}/program/water-mixing-junction/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""numbers"":[0]}";
    }
}
