﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SunriseSunset
{
    public class SunriseSunsetGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/sunrise-sunset";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
