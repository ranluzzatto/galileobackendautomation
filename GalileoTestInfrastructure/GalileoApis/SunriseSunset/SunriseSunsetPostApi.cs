﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.SunriseSunset
{
    public class SunriseSunsetPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/sunrise-sunset";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""sunriseSunsetSetupItem"":{""defineMethod"":""DefaultTiming"",""defaultSunrise"":{""hour"":0,""minute"":0},""defaultSunset"":{""hour"":0,""minute"":0},""startDayFromSunriseOffsets"":{""hour"":0,""minute"":0},""startNightFromSunsetOffsets"":{""hour"":0,""minute"":0}},""sunriseSunsetByDateItems"":[{""month"":0,""sunrise"":{""hour"":0,""minute"":0},""sunset"":{""hour"":0,""minute"":0}}]}";
    }
}
