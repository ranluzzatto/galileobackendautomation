﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.RTUSubscription
{
    public class RTUSubscriptionGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/rtu-subscription";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
