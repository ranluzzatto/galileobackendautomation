﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.LogicConditionElementGroup
{
    public class LogicConditionGetApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.GET;
        protected override string ApiRoutePart => "/config/{configId}/element-group/logic-condition";


    }
}
