﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.LogicConditionElementGroup
{
    public class RunEventsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/logic-condition/run-event";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""numbers"":[0]}";
    }
}
