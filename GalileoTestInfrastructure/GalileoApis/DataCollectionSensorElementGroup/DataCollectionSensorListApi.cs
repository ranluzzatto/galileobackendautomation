﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.DataCollectionSensorElementGroup
{
    public class DataCollectionSensorListApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configID}/element-group/data-collection-sensor/list";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
