﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.DataCollectionSensorElementGroup
{
    public class DataCollectionSensorRunEventsApi : GalileoApiBase
    {
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        protected override string ApiRoutePart => "/config/{configID}/element-group/data-collection-sensor/run-events";
    }
}
