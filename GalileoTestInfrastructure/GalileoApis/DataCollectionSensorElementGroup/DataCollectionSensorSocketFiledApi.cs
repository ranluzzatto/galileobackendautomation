﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.DataCollectionSensorElementGroup
{
    public class DataCollectionSensorSocketFiledApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configID}/element-group/data-collection-sensor/socket-field";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
