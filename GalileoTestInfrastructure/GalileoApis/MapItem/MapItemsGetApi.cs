﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapItem
{
    public class MapItemsGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-image/{mapImageId}/map-items";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
