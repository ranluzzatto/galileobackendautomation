﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapItem
{
    public class MapItemIdDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-image/{mapImageId}/map-items/{mapItemId}";
        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
