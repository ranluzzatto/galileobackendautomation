﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapItem
{
    public class MapItemIdPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-image/{mapImageId}/map-items/{mapItemId}";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""coordinateX"":0,""coordinateY"":0,""width"":0,""height"":0,""descriptionPosition"":""Top""}";
    }
}
