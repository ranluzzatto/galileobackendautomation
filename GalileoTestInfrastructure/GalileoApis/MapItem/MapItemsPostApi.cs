﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.MapItem
{
    public class MapItemsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/map-image/{mapImageId}/map-items";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""mapItemID"":""00000000-0000-0000-0000-000000000000"",""mapImageID"":""00000000-0000-0000-0000-000000000000"",""elementID"":0,""cardID"":0,""greenHouseID"":0,""entityType"":""Master"",""number"":0,""name"":""string"",""coordinateX"":0,""coordinateY"":0,""width"":0,""height"":0,""descriptionPosition"":""Top""}";
    }
}
