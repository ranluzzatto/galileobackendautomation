﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ValveSetupElementGroup
{
    public class ValveSetupPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/valve-setup";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""valveSetupComponentItems"":[{""number"":0,""name"":"""",""covalve"":true,""pipeLineNumber"":0,""plotNumber"":0,""nominalFlow"":0,""irrigArea"":0,""cropType"":0,""waterMeterType"":""None"",""waterMeter"":0,""useValveFeedback"":true,""isOpenFieldValveExist"":true,""isValveFeedbackInputExist"":true,""relatedProgramNumbers"":[0]}],""valveSeasonSetupView"":{""overWaterLimitReaction"":""Idle"",""alarmAbovePercOfLimit"":0},""valveSeasonValueViewItems"":[{""number"":0,""value"":0}]}";
    }
}