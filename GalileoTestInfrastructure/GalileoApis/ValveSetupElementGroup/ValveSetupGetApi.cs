﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ValveSetupElementGroup
{
    public class ValveSetupGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/valve-setup";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
