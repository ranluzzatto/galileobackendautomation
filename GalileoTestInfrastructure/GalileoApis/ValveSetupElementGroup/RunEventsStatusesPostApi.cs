﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ValveSetupElementGroup
{
    public class RunEventsStatusesPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/valve-setup/statuses/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""numbers"":[0]}";
    }
}
