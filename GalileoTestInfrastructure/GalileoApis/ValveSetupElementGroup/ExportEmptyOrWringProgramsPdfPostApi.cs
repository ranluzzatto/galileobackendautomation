﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ValveSetupElementGroup
{
    public class ExportEmptyOrWringProgramsPdfPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/element-group/valve-setup/export-empty-or-with-wrong-programs";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"[{""number"":0,""status"":""NotActive""}]";
    }
}