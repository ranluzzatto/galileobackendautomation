﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.License
{
    public class ReceivedBySettingsPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/licenses/{licenseId}/received-by-settings";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
        public string DefaultPayload = @"{""payer"":"""",""receivedByID"":null}";
    }
}
