﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.License
{
    public class ByLicenseCodeGetApi : GalileoApiBase
    {
        public ByLicenseCodeGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/licenses/by-license-code";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
