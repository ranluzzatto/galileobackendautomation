﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.License
{
    public class CountGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/licenses/count";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
