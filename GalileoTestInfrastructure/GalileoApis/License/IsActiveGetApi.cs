﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.License
{
    public class IsActiveGetApi : GalileoApiBase
    {
        public IsActiveGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/license/is-active";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
