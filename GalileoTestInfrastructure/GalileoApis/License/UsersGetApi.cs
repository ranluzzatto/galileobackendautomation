﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.License
{
    public class UsersGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/licenses/users";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
