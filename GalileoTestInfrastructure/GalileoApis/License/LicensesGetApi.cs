﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.License
{
    public class LicensesGetApi : GalileoApiBase
    {
        public LicensesGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/licenses";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
