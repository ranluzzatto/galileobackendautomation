﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class GetCardsSetupViewGetApi : GalileoApiBase
    {
        public GetCardsSetupViewGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/getCardsSetupView";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
