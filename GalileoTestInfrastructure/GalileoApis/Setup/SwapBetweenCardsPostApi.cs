﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class SwapBetweenCardsPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setup/controller/cell/{cellId}/swap-between-cards";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""newCardID"":0,""newLocationNumber"":0}";
    }
}
