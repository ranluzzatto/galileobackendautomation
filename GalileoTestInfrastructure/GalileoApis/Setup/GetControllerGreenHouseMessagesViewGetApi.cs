﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class GetControllerGreenHouseMessagesViewGetApi : GalileoApiBase
    {
        public GetControllerGreenHouseMessagesViewGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/getControllerGreenHouseMessagesView";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
