﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class IsAllocatingPendingSNGetApi : GalileoApiBase
    {
        public IsAllocatingPendingSNGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/isAllocatingPendingSN";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
