﻿using GalileoTestInfrastructure.Infrastructure;
using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class UpdateControllerSetupPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setup/controller/updateControllerSetup";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""commUnitID"":CommUnitPlaceHolder,""projectID"":18,""controllerNumber"":75,""sn"":""SNPlaceHolder"",""controllerName"":""QA "",""licenseCode"":null,""powerType"":2,""controllerType"":1,""controllerStatus"":true,""timeZoneID"":154,""latitude"":0,""longitude"":0,""openFieldSettingsView"":{""cycleQuantityLimit"":0,""cycleTimeLimit"":0,""systemUnits"":2,""enableIrrigationDuringOtherProgFertType"":0},""greenHouseSettingsView"":{""isIrrigationSystemExist"":false,""dataCollectionCycle"":10,""systemUnits"":0},""greenHouses"":[],""configID"":ConfigIdPlaceHolder,""isLogsExist"":true}"
                                         .Replace("CommUnitPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("CommUnitID"))
                                         .Replace("SNPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"))
                                         .Replace("ConfigIdPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("configId"));
    }
}
