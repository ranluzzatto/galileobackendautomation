﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class UpdateControllerCardViewPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setup/controller/updateControllerCardView";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"[{""cardID"":0,""cardNumber"":0,""cardTypeID"":0,""isDeleted"":true,""isEmpty"":true,""adapterNumber"":0,""connectorType"":""None"",""description"":""string"",""cards"":[{""cardID"":0,""cardTypeID"":0,""parentCardID"":0,""cardNumber"":0,""isEmpty"":true,""controlPanelSubCard"":{""id"":0,""controlPanelID"":0,""cardID"":0,""cardNumber"":0,""cpuCardID"":0,""subType"":""Device"",""lowBatteryThreshold"":0,""stickerID"":""string"",""description"":""string"",""reportingPeriods"":0,""digitalSamplingPeriod"":0,""analogSamplingPeriod"":0,""stabillizationPeriod"":0,""pulseCountTresholdInput1"":0,""pulseCountTresholdInput2"":0,""pulseCountTresholdInput3"":0,""pulseCountTresholdInput4"":0,""pulseCountTresholdInput5"":0,""pulseCountTresholdInput6"":0,""pulseWidth"":0,""order"":0}}]}]";
    }
}
