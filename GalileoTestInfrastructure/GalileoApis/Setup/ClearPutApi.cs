﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class ClearPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setup/controller/{configIdNotUsed}/clear";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
