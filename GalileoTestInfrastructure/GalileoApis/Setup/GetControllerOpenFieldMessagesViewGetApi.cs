﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class GetControllerOpenFieldMessagesViewGetApi : GalileoApiBase
    {
        public GetControllerOpenFieldMessagesViewGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/getControllerOpenFieldMessagesView";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
