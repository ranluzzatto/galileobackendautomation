﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class DownloadAllSetupToControllerPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setup/controller/{configIdNotUsed}/download-all-setup-to-controller";
        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
