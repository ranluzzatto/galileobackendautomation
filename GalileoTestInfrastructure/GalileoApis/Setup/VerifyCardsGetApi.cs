﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class VerifyCardsGetApi : GalileoApiBase
    {
        public VerifyCardsGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/verifyCards";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
