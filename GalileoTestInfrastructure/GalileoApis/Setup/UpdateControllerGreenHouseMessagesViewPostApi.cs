﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class UpdateControllerGreenHouseMessagesViewPostApi : GalileoApiBase
    {
        public UpdateControllerGreenHouseMessagesViewPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/updateControllerGreenHouseMessagesView";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""startIrrigationMessage"":true,""endIrrigationMessage"":true,""irrigationEc_pHMessage"":true,""startDrainageMessage"":true,""endDrainageMessage"":true,""failIrrigationEndMessage"":true,""uncontrollerWaterMessage"":true,""waterCounterFailMessage"":true,""lowWaterFlowMessage"":true,""highWaterFlowMessage"":true,""lowWaterPressureMessage"":true,""uncontrollerFartilizationMessage"":true,""fertCounterFailMessage"":true,""lowFertPumpFlowMessage"":true,""ec_pHIrrigationFailMessage"":true,""ec_pHDrainageFailMessage"":true,""mixerFillingFailMessage"":true,""mixerEmptyingFailMessage"":true,""generalSensorFailMessage"":true}";
    }
}
