﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class IsSNUniqueGetApi : GalileoApiBase
    {
        public IsSNUniqueGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/isSNUnique";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
