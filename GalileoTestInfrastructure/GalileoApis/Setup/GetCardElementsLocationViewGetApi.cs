﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class GetCardElementsLocationViewGetApi : GalileoApiBase
    {
        public GetCardElementsLocationViewGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/getCardElementsLocationView";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
