﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class ControllerDeleteApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setup/controller/{configId}";
        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
