﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class UpdateControllerOpenFieldMessagesViewPostApi : GalileoApiBase
    {
        public UpdateControllerOpenFieldMessagesViewPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/updateControllerOpenFieldMessagesView";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""pauseDeviceMessages"":true,""sensorsFaultMessages"":true,""burstProtectionMessages"":""No"",""rtuCommunicationFault"":""No"",""waterFaultMessages"":true,""flowRateMessages"":true,""fertilizerFaultMessages"":true,""eCpHFaultMessages"":true,""startIrrigationMessages"":true,""endIrrigationMessages"":true,""fertilizerMessages"":true,""filterFlushMessages"":true,""logicConditionMessages"":true}";
    }
}
