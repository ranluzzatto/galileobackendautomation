﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class SNPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/setup/controller/{configId}/sn";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string DefaultPayload = @"{""sn"":""""}";
    }
}
