﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.Setup
{
    public class GetControllerSetupViewGetApi : GalileoApiBase
    {
        public GetControllerSetupViewGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "/setup/controller/getControllerSetupView";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
