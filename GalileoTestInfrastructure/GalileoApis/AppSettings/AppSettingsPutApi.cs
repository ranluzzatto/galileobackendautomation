﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.AppSettings
{
    public class AppSettingsPutApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/app-settings";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
