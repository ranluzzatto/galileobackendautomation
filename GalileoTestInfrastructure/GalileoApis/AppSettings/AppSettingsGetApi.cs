﻿using HttpCientInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GalileoTestInfrastructure.GalileoApis.AppSettings
{
    public class AppSettingsGetApi : GalileoApiBase
    {
        public AppSettingsGetApi(Dictionary<string, string> uriLocalParams)
           : base(null, uriLocalParams) { }
        protected override string ApiRoutePart => "/app-settings/{key}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
