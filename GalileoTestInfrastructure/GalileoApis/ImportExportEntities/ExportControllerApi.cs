﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ImportExportEntities
{
    public class ExportControllerApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/export/controller/{configId}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
