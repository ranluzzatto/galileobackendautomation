﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ImportExportEntities
{
    public class ImportControllerApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/import/controller/{configId}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
