﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterQuota
{
    public class WaterQuotaPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/water-quota";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""waterQuotaItems"":[{""month"":0,""quota"":0}],""waterQuotaSetupItem"":{""waterMeterType"":""None"",""waterMeter"":0,""overQuotaLimitDeff"":0,""systemOverQuotaLimitReaction"":""Idle""}}";

    }
}
