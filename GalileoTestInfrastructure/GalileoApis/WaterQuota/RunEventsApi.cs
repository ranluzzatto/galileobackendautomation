﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterQuota
{
    public class RunEventsApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/water-quota/run-events";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public static string DefaultPayload = @"{""numbers"":[0]}";
    }
}
