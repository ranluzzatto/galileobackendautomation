﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.WaterQuota
{
    public class WaterQuotaGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/config/{configId}/water-quota";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
