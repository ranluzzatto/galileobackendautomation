﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.OneSignal
{
    public class PasswordRestoreTicketPostApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/one-signal";
        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
