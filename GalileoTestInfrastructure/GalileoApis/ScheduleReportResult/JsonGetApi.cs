﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class JsonGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/schedule/result/{reportId}/json";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
