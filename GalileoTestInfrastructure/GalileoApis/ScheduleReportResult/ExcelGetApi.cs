﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class ExcelGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/schedule/result/{reportId}/excel";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
