﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class PdfGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/schedule/result/{reportId}/pdf";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
