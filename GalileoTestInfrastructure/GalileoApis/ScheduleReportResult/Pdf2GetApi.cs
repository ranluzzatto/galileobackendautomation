﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportCenter
{
    public class Pdf2GetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/schedule/result/{reportId}/pdf2";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
