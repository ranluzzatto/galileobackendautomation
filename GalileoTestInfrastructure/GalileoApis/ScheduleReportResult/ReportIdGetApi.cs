﻿using HttpCientInfrastructure.Infrastructure;

namespace GalileoTestInfrastructure.GalileoApis.ScheduleReportResult
{
    public class ReportIdGetApi : GalileoApiBase
    {
        protected override string ApiRoutePart => "/report/{configId}/schedule/result/{reportId}";
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
