﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HttpCientInfrastructure.Infrastructure
{
    public class HttpClientServices
    {
        private HttpClient _restClient = new HttpClient();
        internal HttpResponseMessage RunRequest(GalileoApiBase galileoApi, string otherToken)
        {
            _restClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                string.IsNullOrEmpty(otherToken) ? GalileoApiBase.Token : otherToken);

            switch (galileoApi.RequestType)
            {
                case HttpRequestType.GET:
                    return RunGetRequest(galileoApi);

                case HttpRequestType.POST:
                    return RunPostRequest(galileoApi);

                case HttpRequestType.PUT:
                    return RunPutRequest(galileoApi);

                case HttpRequestType.DETELE:
                    return RunDeleteRequest(galileoApi);

                default:
                    return null;
            }
        }

        private HttpResponseMessage RunDeleteRequest(GalileoApiBase galileoApi)
        {
            var response = _restClient.DeleteAsync(galileoApi.ActualUri);
            return response.Result;
        }

        private HttpResponseMessage RunPutRequest(GalileoApiBase galileoApi)
        {
            if (string.IsNullOrEmpty(galileoApi.RequestPayload))
                galileoApi.RequestPayload = "";

            HttpContent c = new StringContent(galileoApi.RequestPayload, Encoding.UTF8, "application/json");
            var response = _restClient.PutAsync(galileoApi.ActualUri, c);

            return response.Result;
        }

        private  HttpResponseMessage RunGetRequest(GalileoApiBase galileoApi)
        {
            var response =  _restClient.GetAsync(galileoApi.ActualUri);

            return response.Result;
        }
        private HttpResponseMessage RunPostRequest(GalileoApiBase galileoApi)
        {
            if(string.IsNullOrEmpty(galileoApi.FileNameToPost))
            {
                var content = !string.IsNullOrEmpty(galileoApi.RequestPayload)
               ? new StringContent(galileoApi.RequestPayload, Encoding.UTF8, "application/json")
               : new StringContent(string.Empty);

                var response = _restClient.PostAsync(galileoApi.ActualUri, content);
                return response.Result;

            }
            else
            {
                var fileName = AppDomain.CurrentDomain.BaseDirectory + galileoApi.FileNameToPost;
                var stream = System.IO.File.OpenRead(fileName);

                using var request = new HttpRequestMessage(HttpMethod.Post, "file");
                using var content = new MultipartFormDataContent
                {
                    {
                        new StreamContent(stream), "FileToUpload1", "Test.txt"
                    },

                };
                request.Content = content;

                var response = _restClient.PostAsync(galileoApi.ActualUri, content);
                return response.Result;
            }
        }
        public async Task<string> RunPostRequest(string uri, string content)
        {
            if (string.IsNullOrEmpty(content))
                throw new Exception("Content cannot be empty for POST request");

            HttpContent c = new StringContent(content, Encoding.UTF8, "application/json");
            var response = await _restClient.PostAsync(uri, c);
            var context = await response.Content.ReadAsStringAsync();

            return context;
        }
    }
}
