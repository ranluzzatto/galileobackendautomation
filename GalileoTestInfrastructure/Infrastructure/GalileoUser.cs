﻿namespace GalileoTestInfrastructure.Infrastructure
{
    public class GalileoUser
    {
        public UserRoles Role { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
