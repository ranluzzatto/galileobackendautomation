﻿namespace GalileoTestInfrastructure.Infrastructure
{
    public enum UserRoles
    {
        God = 0,
        SuperAdmin = 1,
        SuperUser = 2,
        SiteManager = 3,
        Admin = 4,
        User = 5,
        Payment = 6,
        PaymentManager = 7,
        Distributor = 8
    }
}
