﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GalileoTestInfrastructure.Infrastructure
{
    public static class Extentions
    {
        public static bool IsPropertyPathEQValue(this JObject jObject, string path, string value)
        {
            var token = jObject.SelectToken(path).ToString();
            return token.ToString().Equals(value, StringComparison.CurrentCultureIgnoreCase);
        }
        public static string GetStringValueFromPath(this JObject jObject, string path)
        {
            return jObject.SelectToken(path).ToString();
        }
        public static bool GetBoolValueFromPath(this JToken jObject, string path)
        {
            return jObject.SelectToken(path).Value<bool>();
        }

        public static object GetObjectValueFromPath(this JToken jObject, string path)
        {
            return jObject.SelectToken(path).Value<object>();
        }
        
        public static string GetStringValueFromPath(this JToken jObject, string path)
        {
            return jObject.SelectToken(path).Value<string>();
        }
        public static int GetIntValueFromPath(this JToken jObject, string path)
        {
            return jObject.SelectToken(path).Value<int>();
        }
        public static double GetDoubleValueFromPath(this JToken jObject, string path)
        {
            return jObject.SelectToken(path).Value<double>();
        }
        public static void SetValueToPath(this JObject jObject, string path, object value)
        {
            JToken token = jObject.SelectToken(path);
            var jp = token.Parent as JProperty;

            if (jp == null) throw new Exception("Error while setting value to path");

            switch (Type.GetTypeCode(value.GetType()))
            {
                case TypeCode.Int32:
                    jp.Value = (int)value;
                    break;
                case TypeCode.String:
                    jp.Value = (string)value;
                    break;
                case TypeCode.Boolean:
                    jp.Value = (bool)value;
                    break;
                case TypeCode.Double:
                    jp.Value = (double)value;
                    break;
                case TypeCode.Decimal:
                    jp.Value = (decimal)value;
                    break;

                default:
                    throw new Exception("This type was not handled in extention method 'SetValueToPath': " + Type.GetTypeCode(value.GetType()));
            }
        }
        public static bool NullRespectingSequenceEqual<T>(this IEnumerable<T> first, IEnumerable<T> second)
        {
            if (first == null && second == null)
                return true;

            if (first == null || second == null)
                return false;

            return first.SequenceEqual(second);
        }
        public static int GetChildrenCount(this JToken jObject)
        {
            if (!(jObject is JArray))
                throw new Exception("GetChildrenCount: the referanced object has to be of type 'JArray'");

            return ((JArray)jObject).Count;
        }
        public static JObject ToJObject(this string str)
        {
            return JObject.Parse(str);
        }

    }
}
