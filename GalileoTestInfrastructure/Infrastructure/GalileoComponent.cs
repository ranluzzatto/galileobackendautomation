﻿using System.Collections.Generic;

namespace GalileoTestInfrastructure.Infrastructure
{
    public class GalileoComponent
    {
        public GalileoComponent()
        {
            Properties = new Dictionary<string, string>();
        }
        public SetupElementType ComponentType { get; set; }

        public Dictionary<string,string> Properties { get; set; }

        public string Name { get; set; }
        public int Number { get; set; }
    }
}
